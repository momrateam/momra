//
//  AppealsVotersViewController.h
//  MOMRA
//
//  Created by aya on 8/9/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface AppealsVotersViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>
- (IBAction)close:(id)sender;

- (IBAction)ckCityChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *ckCity;
- (IBAction)ShowInMap:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtRegion;
@property (weak, nonatomic) IBOutlet UITextField *txtAppellantType;
@property (weak, nonatomic) IBOutlet UITextField *AppealStatus;

@end
