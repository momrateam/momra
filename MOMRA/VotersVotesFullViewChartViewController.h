//
//  VotersVotesFullViewChartViewController.h
//  MOMRA
//
//  Created by aya on 7/13/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOMRA-Swift.h"
#import "Helper.h"

@interface VotersVotesFullViewChartViewController : UIViewController

@property (weak, nonatomic) IBOutlet BarChartView *chart;
- (IBAction)close:(id)sender;
@property (weak, nonatomic) NSDictionary *reg;
@property (weak, nonatomic) NSDictionary *parameters;
@property (weak, nonatomic) IBOutlet UILabel *reportTitle;
@property (weak, nonatomic) NSMutableDictionary * dictAges;
@property (weak, nonatomic) IBOutlet UILabel *criteria;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *header;

@end
