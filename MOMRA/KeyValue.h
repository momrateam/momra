//
//  KeyValue.h
//  MOMRA
//
//  Created by aya on 7/26/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyValue : NSObject
@property (weak, nonatomic) NSString * Key;
@property (weak, nonatomic)  NSString * Value;
@end
