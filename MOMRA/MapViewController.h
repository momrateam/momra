//
//  MapViewController.h
//  MOMRA
//
//  Created by aya on 6/14/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "Statistics.h"
#import "Region.h"
#import "AllStatisticsViewController.h"
#import "staticVariables.h"
#import <MapKit/MapKit.h>
#import "KMLParser.h"

@interface MapViewController : UIViewController<MKMapViewDelegate>
- (IBAction)btnIntensityTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewColor5;
@property (weak, nonatomic) IBOutlet UIView *viewColor4;
@property (weak, nonatomic) IBOutlet UIView *viewColor3;
@property (weak, nonatomic) IBOutlet UIView *viewColor2;
@property (weak, nonatomic) IBOutlet UIView *viewColor1;
@property (weak, nonatomic) IBOutlet UIButton *btnReload;
- (IBAction)btnReloadTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
- (IBAction)btnCloseKeysTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewMapKeys;
- (IBAction)btnMapKeysTapped:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (IBAction)goToStatistics:(id)sender;

@end
