//
//  VotersVotesFullViewChartViewController.m
//  MOMRA
//
//  Created by aya on 7/13/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "VotersVotesFullViewChartViewController.h"

@interface VotersVotesFullViewChartViewController ()

@end

@implementation VotersVotesFullViewChartViewController
@synthesize reg;
@synthesize parameters;
@synthesize chart;
@synthesize dictAges;
@synthesize reportTitle;

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
}

-(void)orientationChanged:(NSNotification *)notification
{
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.contentSize.height);
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@",@"إحصائيات",[reg objectForKey:@"Name"],@"حسب المعايير التالية"];
    NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc] initWithString:title];
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,title.length)];
    }else{
        [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:12.0] range:NSMakeRange(0,title.length)];
    }
    [attributedTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,title.length)];
    reportTitle.attributedText=attributedTitleString;
    self.header.text=title;
    //
    NSMutableString * criteria=[[NSMutableString alloc]init];
    //
    if (![[parameters valueForKey:@"Age"] isEqualToString:@"-1"]) {
        if ([[parameters valueForKey:@"Age"] isEqualToString:@"0"])
        {
            [criteria appendString:@"الفئة العمرية : الكل \n"];
        }
        else
        {
            [criteria appendString:[NSString stringWithFormat:@" الفئة العمرية : %@ \n",[dictAges objectForKey:[parameters valueForKey:@"Age"]]]];
        }
    }
    //
    if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"]) {
        
    }
    else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
    {
        [criteria appendString:@"النوع : ذكور "];
    }
    else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
    {
        [criteria appendString:@"النوع : إناث "];
    }
    else
    {
        [criteria appendString:@"النوع : الكل "];
    }
    self.criteria.text=criteria;
    //
    [Helper configureBarChart:chart inInfoWindow:NO];
    //
    float sizeOfContent = 0;
    UIView *lLast = self.chart;
    NSInteger wd = lLast.frame.origin.y;
    NSInteger ht = lLast.frame.size.height;
    sizeOfContent = wd+ht;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
    //
    [self loadChart];
    //
        [chart animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseInCirc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadChart {
    NSArray * arrofKeys = [[dictAges allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
    //male and female
    if ([[parameters valueForKey:@"Sex"] isEqualToString:@"3"]) {
        if ([[parameters valueForKey:@"Age"] isEqualToString:@"0"]) {
            NSMutableArray *xVals = [[NSMutableArray alloc] init];
            for (int i = 0; i < 6; i++)
            {
                [xVals addObject:[dictAges objectForKey:arrofKeys[i]]];
            }
            
            NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
            NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
            for (int i = 0; i < 6; i++)
            {
                double val1 = [[reg objectForKey:[NSString stringWithFormat:@"MaleAge%dVotersCount",i+1]]doubleValue];
                double val2 = [[reg objectForKey:[NSString stringWithFormat:@"FemaleAge%dVotersCount",i+1]]doubleValue];
                [yVals1 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val1)] xIndex:i]];
                [yVals2 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val2)] xIndex:i]];
            }
            //
            NSMutableArray *dataSets = [[NSMutableArray alloc] init];
            BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@"ذكور"];
            BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:@"إناث@"];
            set1.colors=@[[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
            set2.colors=@[[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
            //
            [dataSets addObject:set1];
            [dataSets addObject:set2];;
            //
            BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
            data.groupSpace = 0.8;
            if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                [data setValueFont:[UIFont systemFontOfSize:12.0f]];
            }
            chart.data = data;
        } else {
            NSMutableArray *xVals = [[NSMutableArray alloc] init];
            //[parameters valueForKey:@"Age"]
            if ([[parameters valueForKey:@"Age"] isEqualToString:@"-1"]) {
                [xVals addObject:@"إجمالى"];
            }else{
                [xVals addObject:[dictAges objectForKey:[parameters valueForKey:@"Age"]]];
            }
            
            NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
            NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
            double val1 = [[reg objectForKey:@"MaleAgeVotersCount"]doubleValue];
            double val2 = [[reg objectForKey:@"FemaleAgeVotersCount"]doubleValue];
            [yVals1 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val1)] xIndex:0]];
            [yVals2 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val2)] xIndex:0]];
            //
            BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@"ذكور"];
            BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:@"إناث"];
            set1.colors=@[[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
            set2.colors=@[[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
            //
            NSMutableArray *dataSets = [[NSMutableArray alloc] init];
            [dataSets addObject:set1];
            [dataSets addObject:set2];
            //
            BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
            data.groupSpace = 0.8;
            if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                [data setValueFont:[UIFont systemFontOfSize:12.0f]];
            }
            chart.data = data;
        }
        
    }
    else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
    {
        
        if ([[parameters valueForKey:@"Age"] isEqualToString:@"0"]) {
            NSMutableArray *xVals = [[NSMutableArray alloc] init];
            
            for (int i = 0; i < 6; i++)
            {
                [xVals addObject:[dictAges objectForKey:arrofKeys[i]]];
            }
            
            NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
            for (int i = 0; i < 6; i++)
            {
                double val1 = [[reg objectForKey:[NSString stringWithFormat:@"Age%dVotersCount",i+1]]doubleValue];
                [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:i]];
            }
            BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@""];
            //
            if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"])
            {
                [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:136/255.0 blue:91/255.0 alpha:1.0 ]];
            }
            else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
            {
                [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
            }
            else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
            {
                [set1 setColor:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
            }
            //
            NSMutableArray *dataSets = [[NSMutableArray alloc] init];
            [dataSets addObject:set1];
            
            BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
            chart.legend.enabled = NO;
            if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                [data setValueFont:[UIFont systemFontOfSize:12.0f]];
            }
            chart.data = data;
        }
        else
        {
            NSMutableArray *xVals = [[NSMutableArray alloc] init];
            if ([[parameters valueForKey:@"Age"] isEqualToString:@"-1"]) {
                [xVals addObject:@"إجمالى"];
            }else{
                [xVals addObject:[dictAges objectForKey:[parameters valueForKey:@"Age"]]];
            }
            
            NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
            double val1 = [[reg objectForKey:@"AgeVotersCount"]doubleValue];
            [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:0]];
            BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@""];
            //
            if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"])
            {
                [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:136/255.0 blue:91/255.0 alpha:1.0 ]];
            }
            else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
            {
                [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
            }
            else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
            {
                [set1 setColor:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
            }
            //
            set1.barSpace=0.7;
            NSMutableArray *dataSets = [[NSMutableArray alloc] init];
            [dataSets addObject:set1];
            
            BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
            chart.legend.enabled = NO;
            if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                [data setValueFont:[UIFont systemFontOfSize:12.0f]];
            }
            chart.data = data;
        }
        
    }
    
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
