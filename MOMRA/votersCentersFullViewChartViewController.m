//
//  votersCentersFullViewChartViewController.m
//  MOMRA
//
//  Created by aya on 7/8/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "votersCentersFullViewChartViewController.h"

@interface votersCentersFullViewChartViewController ()

@end

@implementation votersCentersFullViewChartViewController
@synthesize regCurrent;
@synthesize regFirst;
@synthesize regSecond;
//
@synthesize parameters;
@synthesize chart;
@synthesize reportTitle;
@synthesize mainContainerView;

static bool isFirstChart;
static bool isSecondChart;
static bool isThirdChart;
static PieChartView *secondChart;
static PieChartView *thirdChart;
static DetailsTable * table;

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
}

-(void)orientationChanged:(NSNotification *)notification
{
//    if (!UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.contentSize.height);
        [self adjustViewsForOrientation:[[UIApplication sharedApplication]statusBarOrientation]];
//    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    CGRect  frame=table.frame;
    CGRect  containerFrame=mainContainerView.frame;
    NSInteger wd = (containerFrame.size.width-frame.size.width)/2;
    frame.origin.x=wd;
    [table setFrame:frame];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    @try {
        secondChart=Nil;
        thirdChart=Nil;
        isFirstChart=NO;
        isSecondChart=NO;
        isThirdChart=NO;
        table=Nil;
        NSDictionary * reg;
        //
        CGFloat firstChartY=0;
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            firstChartY=100;
        }else{
            firstChartY=90;
        }
        //
        int numberOfCharts=0;
        if (regCurrent != Nil) {
            reg =regCurrent;
            [Helper configurePieChart:chart];
            isFirstChart=YES;
            //
            [self loadChart:reg withPieChartView:chart title:@"(الدورة الثالثة)"];
            numberOfCharts++;
            float sizeOfContent = 0;
            UIView *lLast = self.chart;
            NSInteger wd = lLast.frame.origin.y;
            NSInteger ht = lLast.frame.size.height;
            sizeOfContent = wd+ht;
            self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
        }
        if (regSecond != Nil){
            reg =regSecond;
            if (numberOfCharts == 0) {
                [Helper configurePieChart:chart];
                isFirstChart=YES;
                //
                [self loadChart:reg withPieChartView:chart title:@"(الدورة الثانية)"];
                numberOfCharts++;
                float sizeOfContent = 0;
                UIView *lLast = self.chart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
            }
            else{
                secondChart=[[PieChartView alloc] initWithFrame:CGRectMake(0, firstChartY+(numberOfCharts*chart.frame.size.height), chart.frame.size.width, chart.frame.size.height)];
                secondChart.autoresizingMask=UIViewAutoresizingFlexibleWidth;
                [mainContainerView addSubview:secondChart];
                float sizeOfContent = 0;
                UIView *lLast = secondChart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
                
                [Helper configurePieChart:secondChart];
                isSecondChart=YES;
                //
                [self loadChart:reg withPieChartView:secondChart title:@"(الدورة الثانية)"];
                numberOfCharts++;
            }
        }
        if(regFirst != Nil){
            reg =regFirst;
            if (numberOfCharts == 0) {
                [Helper configurePieChart:chart];
                isFirstChart=YES;
                //
                [self loadChart:reg withPieChartView:chart  title:@"(الدورة الأولى)"];
                numberOfCharts++;
                float sizeOfContent = 0;
                UIView *lLast = self.chart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
            }
            else{
                thirdChart=[[PieChartView alloc] initWithFrame:CGRectMake(0, firstChartY+(numberOfCharts*chart.frame.size.height), chart.frame.size.width, chart.frame.size.height)];
                thirdChart.autoresizingMask=UIViewAutoresizingFlexibleWidth;
                [mainContainerView addSubview:thirdChart];
                float sizeOfContent = 0;
                UIView *lLast = thirdChart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
                //
                [Helper configurePieChart:thirdChart];
                isThirdChart=YES;
                //
                [self loadChart:reg withPieChartView:thirdChart title:@"(الدورة الأولى)"];
                numberOfCharts++;
            }
        }
        //
        NSString *title = [NSString stringWithFormat:@"%@ %@ %@",@"إحصائيات",[reg objectForKey:@"Name"],@"حسب المعايير التالية"];
        NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc] initWithString:title];
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,title.length)];
        }
        else
        {
            [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:12.0] range:NSMakeRange(0,title.length)];
        }
        [attributedTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,title.length)];
        reportTitle.attributedText=attributedTitleString;
        self.header.text=title;
        //
        NSMutableString * criteria=[[NSMutableString alloc]init];
        //
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"]) {
            
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
        {
            [criteria appendString:@"النوع : ذكور "];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
        {
            [criteria appendString:@"النوع : إناث "];
        }
        else
        {
            [criteria appendString:@"النوع : الكل "];
        }
        //
        self.criteria.text=criteria;
        //
        if (numberOfCharts >1) {
            NSMutableArray * values=[[NSMutableArray alloc]init];
            NSLog(@"barChart : %ld",(long)[chart yValueSum]);
//            if ([chart yValueSum] != 0) {
            if (isFirstChart) {
                KeyValue * first=[[KeyValue alloc]init];
                if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
                    first.Key=@"الثالثة";
                }
                else if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
                    first.Key=@"الثانية";
                }
                else if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
                    first.Key=@"الأولى";
                }
                first.Value=[NSString stringWithFormat:@"%ld",(long)[chart yValueSum]];
                [values addObject:first];
            }
            NSLog(@"secondChart : %ld",(long)[secondChart yValueSum]);
//            if ([secondChart yValueSum] != 0) {
            if (isSecondChart) {
                KeyValue * first=[[KeyValue alloc]init];
                first.Key=@"الثانية";
                first.Value=[NSString stringWithFormat:@"%ld",(long)[secondChart yValueSum]];
                [values addObject:first];
            }
            NSLog(@"thirdChart : %ld",(long)[thirdChart yValueSum]);
//            if ([thirdChart yValueSum] != 0) {
            if (isThirdChart) {
                KeyValue * first=[[KeyValue alloc]init];
                first.Key=@"الأولى";
                first.Value=[NSString stringWithFormat:@"%ld",(long)[thirdChart yValueSum]];
                [values addObject:first];
            }
            DetailsTable *DetailsTable =  [[[NSBundle mainBundle] loadNibNamed:@"DetailsTable" owner:self options:nil] objectAtIndex:0];
            table=DetailsTable;
            [DetailsTable LoadDataWithArrayOfKeyValue:values];
            //
            CGRect frame=DetailsTable.frame;
            frame.origin.y=frame.origin.y+firstChartY+(numberOfCharts*chart.frame.size.height);
            DetailsTable.frame=frame;
            //
            [self adjustViewsForOrientation:[[UIApplication sharedApplication]statusBarOrientation]];
            //
            float sizeOfContent = 0;
            UIView *lLast = DetailsTable;
            NSInteger wd = lLast.frame.origin.y;
            NSInteger ht = lLast.frame.size.height;
            sizeOfContent = wd+ht;
            self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
            //
            [mainContainerView addSubview:DetailsTable];
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadChart :(NSDictionary *)reg withPieChartView:(PieChartView *)pieChart title : (NSString *)title{
    //[self adjustViewsForOrientation:[[UIApplication sharedApplication]statusBarOrientation]];
    //male and female
    if ([[parameters valueForKey:@"Sex"] isEqualToString:@"3"]) {
        NSString * maleKey;
        NSString * femaleKey;
        if ([[parameters valueForKey:@"isVoters"] isEqualToString:@"1"]) {
            maleKey=@"VotersMaleCenterCount";
            femaleKey=@"VotersFemaleCenterCount";
        }
        else{
            maleKey=@"CanMaleCenterCount";
            femaleKey=@"CanFemaleCenterCount";
        }
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        double val1 = [[reg objectForKey:maleKey]integerValue];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:0]];
        double val2 = [[reg objectForKey:femaleKey]integerValue];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val2 xIndex:1]];
        //
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        [xVals addObject:[NSString stringWithFormat:@"ذكور : %@",[reg objectForKey:maleKey]]];
        [xVals addObject:[NSString stringWithFormat:@"إناث : %@",[reg objectForKey:femaleKey]]];
        PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:title];
        dataSet.sliceSpace = 3.0;
        
        NSMutableArray *colors = [[NSMutableArray alloc] init];
        [colors addObject:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
        [colors addObject:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
//        [colors addObject:ChartColorTemplates.vordiplom[2]];
//        [colors addObject:ChartColorTemplates.vordiplom[3]];
        
        dataSet.colors = colors;
        
        
        PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
        
        NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
        pFormatter.numberStyle = NSNumberFormatterPercentStyle;
        pFormatter.maximumFractionDigits = 1;
        pFormatter.multiplier = @1.f;
        pFormatter.percentSymbol = @" %";
        [data setValueFormatter:pFormatter];
        [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:11.f]];
        [data setValueTextColor:[UIColor blackColor]];
//        pieChart.legend.enabled = NO;
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:16.f]];
        }
        pieChart.data = data;
        [pieChart highlightValues:nil];
    }
    else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
    {
        NSString * centersKey;
        if ([[parameters valueForKey:@"isVoters"] isEqualToString:@"1"]) {
            centersKey=@"VotersCenterCount";
        }
        else{
            centersKey=@"CanCenterCount";
        }
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        double val1 = [[reg objectForKey:centersKey]doubleValue];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:0]];
        //
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"]) {
            [xVals addObject:[NSString stringWithFormat:@"ذكور : %@",[reg objectForKey:centersKey]]];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"]) {
            [xVals addObject:[NSString stringWithFormat:@"إناث : %@",[reg objectForKey:centersKey]]];
        }
        else{
            [xVals addObject:[NSString stringWithFormat:@"الكل : %@",[reg objectForKey:centersKey]]];
        }
//        [xVals addObject:[NSString stringWithFormat:@"%@",[reg objectForKey:centersKey]]];
        PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:title];
        dataSet.sliceSpace = 3.0;
        
        NSMutableArray *colors = [[NSMutableArray alloc] init];
//        [colors addObject:ChartColorTemplates.vordiplom[2]];
//        [colors addObject:ChartColorTemplates.vordiplom[3]];
        //
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"])
        {
            [colors addObject:[[UIColor alloc]initWithRed:0/255.0 green:136/255.0 blue:91/255.0 alpha:1.0 ]];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
        {
            [colors addObject:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
        {
            [colors addObject:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
        }
        //
        dataSet.colors = colors;
        
        PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
        
        NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
        pFormatter.numberStyle = NSNumberFormatterPercentStyle;
        pFormatter.maximumFractionDigits = 1;
        pFormatter.multiplier = @1.f;
        pFormatter.percentSymbol = @" %";
        [data setValueFormatter:pFormatter];
        [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:11.f]];
        [data setValueTextColor:[UIColor blackColor]];
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:16.f]];
        }
        pieChart.data = data;
        [pieChart highlightValues:nil];
    }
    [pieChart animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
