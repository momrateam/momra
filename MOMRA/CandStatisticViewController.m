//
//  CandStatisticViewController.m
//  MOMRA
//
//  Created by aya on 7/2/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "CandStatisticViewController.h"
#import "CandidatesReportViewController.h"
#import "DisCandidatesReportViewController.h"
#import "AcceptedCandidatesReportViewController.h"
#import "staticVariables.h"

@interface CandStatisticViewController ()

@end

@implementation CandStatisticViewController
NSString *Rec;
NSString *Acepted;
NSString *Excl;
static NSTimer * timer;
static int FinishedAPI;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    self.lblAcepted.text = Acepted;
    //    self.lblExcl.text = Excl;
    //    self.lblRec.text = Rec;
    //
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    [self getLastUpdate];
    [self getAllStatistics];
    //
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    
    
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width, self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height);
    }
    //
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    //
    if ((toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(320,  self.viewContent.frame.size.height);
    }
    
}

- (void) updateData:(NSTimer *)timer
{
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    [self getLastUpdate];
    [self getAllStatistics];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
    timer=nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self LoadCachedData];
    //  [self getLastUpdate];
    //  [self getAllStatistics];
    //
    timer=[NSTimer scheduledTimerWithTimeInterval:900.0f
                                           target:self selector:@selector(updateData:) userInfo:nil repeats:YES];
    //
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width, self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)LoadDataWithRec:(NSString *)rec andAcepted:(NSString *)acepted andExcl:(NSString *)excl{
    
    Acepted  =acepted;
    Rec =rec;
    Excl  =excl;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)btnAcceptedCandReport:(id)sender {
    AcceptedCandidatesReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AcceptedCandidatesReport"];
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)btnDisCandidatesReport:(id)sender {
    
    CandidatesReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DisCandidatesReport"];
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)btnRecCand:(id)sender {
    CandidatesReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CandidatesReport"];
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)getLastUpdate{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetLastUpdateTime"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data.length > 0 && connectionError == nil)
        {
            FinishedAPI ++;
            [self checkActivityToStop];

            NSError * err =nil;
            NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                         NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
            NSDictionary * data=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:NSJSONReadingAllowFragments
                                                                  error:&err];
            if (data!= nil) {
                [staticVariables singelton].dataDate = data;
                
                
                
                if ([data objectForKey:@"1"] != Nil) {
                    self.lbllastUpdate.text=[NSString stringWithFormat:@"أخر تحديث : %@",[data objectForKey:@"1"]];
                    
                }
            }
        }else
        {
            FinishedAPI ++;
            [self checkActivityToStop];

            UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
            [alertConErr show];
        }
    }];
}
//
-(void)checkActivityToStop{
    if (FinishedAPI >=2) {
        [self.activity stopAnimating];
       // self.btnUpdate.enabled=YES;
        //((UIButton * )sender).enabled =YES;
        NSLog(@"stop");
        
    }
}
-(void)getAllStatistics
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetAllStatistics"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data.length > 0 && connectionError == nil)
        {
            FinishedAPI ++;
            [self checkActivityToStop];
            NSError * err =nil;
            NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                         NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
            NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingAllowFragments
                                                           error:&err];
            NSDictionary *resultDic = [ff objectAtIndex:0];
            if (resultDic!= nil) {
                [staticVariables singelton].AllStaticisticDic = resultDic;
                
                self.lblRec.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"CandidatesCount"]];
                self.lblExcl.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"DisregardedCandidatesCount"]];
                self.lblAcepted.text =[NSString stringWithFormat:@"%d" ,([[resultDic objectForKey:@"CandidatesCount"] intValue]-[[resultDic objectForKey:@"DisregardedCandidatesCount"] intValue])];
                
                self.lblCandMaleCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"MaleCandidatesCount"]];
                self.lblCandFemaleCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"FemaleCandidatesCount"]];
                
            }
        }else
        {
            FinishedAPI ++;
            [self checkActivityToStop];
            UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
            [alertConErr show];
        }
    }];
}
//
-(void)LoadCachedData{
    NSDictionary *resultDic =  [staticVariables singelton].AllStaticisticDic;
     if (resultDic!= nil) {
    self.lblRec.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"CandidatesCount"]];
    self.lblExcl.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"DisregardedCandidatesCount"]];
    self.lblAcepted.text =[NSString stringWithFormat:@"%d" ,([[resultDic objectForKey:@"CandidatesCount"] intValue]-[[resultDic objectForKey:@"DisregardedCandidatesCount"] intValue])];
    
    self.lblCandMaleCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"MaleCandidatesCount"]];
    self.lblCandFemaleCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"FemaleCandidatesCount"]];
     }
    //
    NSDictionary *data =  [staticVariables singelton].dataDate ;
       if (data!= nil) {
    if ([data objectForKey:@"1"] != Nil) {
        self.lbllastUpdate.text=[NSString stringWithFormat:@"أخر تحديث : %@",[data objectForKey:@"1"]];
    }
       }
}
@end
