//
//  MapLocationView.h
//  MOMRA
//
//  Created by aya on 7/27/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapLocationView : NSObject<MKAnnotation>

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate;
//- (MKMapItem*)mapItem;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;
@property (nonatomic, assign) int typeID;
@property (nonatomic, assign) NSNumber * isCanidiateCenter;
@property (retain, nonatomic) NSNumber *MAX_VOTER_SERIAL;
@end
