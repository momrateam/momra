//
//  DisVotersReportViewController.m
//  MOMRA
//
//  Created by aya on 6/25/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "DisVotersReportViewController.h"
#import "Helper.h"
#import "RejectedVotersMapReportResultViewController.h"
#import "staticVariables.h"

@interface DisVotersReportViewController ()

@end

@implementation DisVotersReportViewController

static NSDictionary * dicAges;
static NSMutableDictionary * dicDays;
static NSMutableDictionary * dicWeeks;
static NSDictionary * dicRegions;
static NSDictionary * dicDivision;
static NSDictionary * dicCenter;
static NSDictionary * dicCity;
static NSDictionary * dicExReason;
//
static UIPickerView*  pickerRegion;
static UIPickerView*  pickerage;
static UIPickerView*  pickerWeek;
static UIPickerView*  pickerDay;
static UIPickerView*  pickerDivision;
static UIPickerView*  pickerCenter;
static UIPickerView*  pickerCity;
static UIPickerView*  pickerExReason;
//
static NSString * SelectedRegionID;
static NSString * SelectedCityID;
static NSString * SelectedDivisionID ;
static NSString * SelectedCenterID ;
static NSString * SelectedAge;
static NSString *SelectedDay ;
static NSString *SelectedWeek;
static NSString *SelectedGender ;
static NSString *SelectedExReason ;

- (void)viewDidLoad {
    [super viewDidLoad];
    SelectedRegionID = nil;
    SelectedCityID = nil;
    SelectedDivisionID  = nil;
    SelectedCenterID  = nil;
    SelectedAge = nil;
    SelectedDay  = nil;
    SelectedWeek = nil;
    SelectedGender  = nil;
    SelectedExReason = nil;

    // Do any additional setup after loading the view.
    [self loadPickers];
    //
    float sizeOfContent = 0;
    UIView *lLast = [self.scrollView.subviews lastObject];
    NSInteger wd = lLast.frame.origin.y;
    NSInteger ht = lLast.frame.size.height;
    
    sizeOfContent = wd+ht;
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
    //self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [self.scrollView addGestureRecognizer:tapGesture];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    ///
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"HacenSaudiArabia" size:13], NSFontAttributeName, nil];
    [self.genderSeg setTitleTextAttributes:attributes forState:UIControlStateNormal];
    //
    [self initPickers];
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if ((orientation == UIInterfaceOrientationPortraitUpsideDown) || (orientation == UIInterfaceOrientationPortrait)) {
        self.scrollView.contentSize = CGSizeMake(306,  458);
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if ((orientation == UIInterfaceOrientationPortraitUpsideDown) || (orientation == UIInterfaceOrientationPortrait)) {
        self.scrollView.contentSize = CGSizeMake(306,  458);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
static bool keyboardIsShown;
- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    //    viewFrame.size.height += (keyboardSize.height - kTabBarHeight);
    
    viewFrame.size.height += (keyboardSize.height - 140.0);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}
- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    //    viewFrame.size.height -= (keyboardSize.height - kTabBarHeight);
    viewFrame.size.height -= (keyboardSize.height - 140.0);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}
//
-(void)initPickers{
    
    
    pickerRegion = [[UIPickerView alloc]init];
    pickerRegion.tag=2;
    [pickerRegion setDataSource:self];
    [pickerRegion setDelegate:self];
    [pickerRegion setShowsSelectionIndicator:YES];
    self.txtRegion.inputView = pickerRegion;
    
    pickerage = [[UIPickerView alloc]init];
    pickerage.tag=3;
    [pickerage setDataSource:self];
    [pickerage setDelegate:self];
    [pickerage setShowsSelectionIndicator:YES];
    self.txtage.inputView = pickerage;
    

    

    
    pickerDivision = [[UIPickerView alloc]init];
    pickerDivision.tag=1;
    [pickerDivision setDataSource:self];
    [pickerDivision setDelegate:self];
    [pickerDivision setShowsSelectionIndicator:YES];
    self.circle.inputView = pickerDivision;
    
    pickerCenter = [[UIPickerView alloc]init];
    pickerCenter.tag=6;
    [pickerCenter setDataSource:self];
    [pickerCenter setDelegate:self];
    [pickerCenter setShowsSelectionIndicator:YES];
    self.txtCenter.inputView = pickerCenter;
    
    pickerCity = [[UIPickerView alloc]init];
    pickerCity.tag=7;
    [pickerCity setDataSource:self];
    [pickerCity setDelegate:self];
    [pickerCity setShowsSelectionIndicator:YES];
    self.txtCity.inputView = pickerCity;
    
    pickerExReason = [[UIPickerView alloc]init];
    pickerExReason.tag=8;
    [pickerExReason setDataSource:self];
    [pickerExReason setDelegate:self];
    [pickerExReason setShowsSelectionIndicator:YES];
    self.txtExReason.inputView = pickerExReason;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}



-(void)updateTextField:(id)sender
{
    @try {
        //
        UIDatePicker *picker = (UIDatePicker*)self.circle.inputView;
        picker.datePickerMode=UIDatePickerModeDate;
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        df.locale= locale;
        [df  setDateFormat:@"yyyy-MM-dd"];
        NSString *formattedDate = [df stringFromDate:picker.date];
        self.circle.text = formattedDate;
    }
    @catch (NSException *exception) {
        NSLog(@"View %@inFunction %@erroris %@",@"SignUpView",@"updateTextField",exception.description);
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
-(void)hideKeyboard
{
    [self.view endEditing:YES];
}
-(void)loadPickers
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"VotersSearchCriteria"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
                          NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
             NSDictionary * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:NSJSONReadingAllowFragments
                                                                 error:&err];
             
             dicAges =[NSJSONSerialization JSONObjectWithData:[[ff objectForKey:@"Ages"] dataUsingEncoding:NSUTF8StringEncoding]
                                                      options:NSJSONReadingAllowFragments
                                                        error:&err];
             if (dicAges && dicAges.count>0) {
                 NSArray * arrofKeys = [[dicAges allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
                 NSArray * arrofValues = [dicAges objectsForKeys: arrofKeys notFoundMarker: [NSNull null]];
                 
                 self.txtage.text = arrofValues[0];
                 [self.txtage resignFirstResponder];
                 NSString * ID = (NSString *)arrofKeys[0];
                 SelectedAge = ID;
             }
             //
             //
             dicRegions =[NSJSONSerialization JSONObjectWithData:[[ff objectForKey:@"Regions"] dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingAllowFragments
                                                           error:&err];
             
             if (![[staticVariables singelton].UserTypeID isEqualToString:@"0"]) {
                 self.txtRegion.text = [dicRegions objectForKey:[staticVariables singelton].UserTypeID];
                 self.txtRegion.enabled = NO;
                 
                 SelectedRegionID = [staticVariables singelton].UserTypeID;
                 self.txtCity.enabled = YES;
                 self.ckCity.enabled = YES;
                 self.ckCity.on = YES;
                 
                 [self loadCitybyRegionID:SelectedRegionID];
             }

             
             dicWeeks =[NSJSONSerialization JSONObjectWithData:[[ff objectForKey:@"VotersWeeks"] dataUsingEncoding:NSUTF8StringEncoding]
                                                       options:NSJSONReadingMutableContainers
                                                         error:&err];
             [dicWeeks removeObjectForKey:@"-2"];
             
             
             dicDays=[NSJSONSerialization JSONObjectWithData:[[ff objectForKey:@"VotersDays"] dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:NSJSONReadingMutableContainers
                                                       error:&err];
             [dicDays removeObjectForKey:@"-2"];
           
             
         }else
         {
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
    //
    [self loadExReason];
    
}
//
-(void)loadDivisionByCityID:(NSString *)CityID
{
    
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",CityID, @"CityID", nil];
    
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetDivisionByCityID"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
                          NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
             dicDivision =[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                          options:NSJSONReadingAllowFragments
                                                            error:&err];
             
             
             [pickerDivision reloadAllComponents];
             [pickerDivision selectRow:0 inComponent:0 animated:NO];
         }else
         {
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
    
}
//
-(void)loadExReason
{
    
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",@"1", @"IsVoter", nil];
    
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetDisregardedReason"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
                          NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
             dicExReason =[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                          options:NSJSONReadingAllowFragments
                                                            error:&err];
             
             
             [pickerExReason reloadAllComponents];
             [pickerExReason selectRow:0 inComponent:0 animated:NO];
         }else
         {
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
    
}
//


-(void)loadCitybyRegionID:(NSString *)regionID
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",regionID, @"RegionID", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetCityByRegionID"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
                          NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
             dicCity =[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                      options:NSJSONReadingAllowFragments
                                                        error:&err];
             
             [pickerCity reloadAllComponents];
             [pickerCity selectRow:0 inComponent:0 animated:NO];
             
         }else
         {
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
    
}

//
//
-(void)loadCenterByDivisionID:(NSString *)DivisionID
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",DivisionID, @"DivisionID",@"1",@"IsVoterCenter", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetCenterByDivisionID"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
                          NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
             dicCenter =[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                        options:NSJSONReadingAllowFragments
                                                          error:&err];
             [pickerCenter reloadAllComponents];
             [pickerCenter selectRow:0 inComponent:0 animated:NO];
         }else
         {
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
    
}
//


#pragma mark - Deleget event
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 1) {
        return (int)dicDivision.count;
    }
    else if (pickerView.tag == 2) {
        return (int)dicRegions.count;
        return (int)dicRegions.count;
    } else if (pickerView.tag == 3) {
        return (int)dicAges.count;
    }
    else if (pickerView.tag == 4) {
        return (int)dicWeeks.count;
    }
    else if (pickerView.tag == 5) {
        return (int)dicDays.count;
    } else if (pickerView.tag ==6) {
        return (int)dicCenter.count;
    }else if (pickerView.tag ==7) {
        return (int)dicCity.count;
    }else if (pickerView.tag ==8) {
        return (int)dicExReason.count;
    }
    
    else
    {
        return 0;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView.tag == 1) {
        NSArray * sortedKeys = [[dicDivision allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * arrofValues = [dicDivision objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        
        self.circle.text = arrofValues[row];
        [self.circle resignFirstResponder];
        NSString * ID = (NSString *)sortedKeys[row];
        [self loadCenterByDivisionID:ID];
        
        self.txtCenter.text = nil;
        SelectedCenterID = nil;
        
        if ([ID isEqualToString:@"0"] ) {
            self.ckCenter.enabled = false;
            self.ckCenter.on = false;
            
        }else
        {
            self.ckCenter.enabled = true;
        }
        SelectedDivisionID = ID;
    } else if (pickerView.tag == 2) {
        NSArray * arrofKeys = [[dicRegions allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * arrofValues = [dicRegions objectsForKeys: arrofKeys notFoundMarker: [NSNull null]];
        
        self.txtRegion.text = arrofValues[row];
        [self.txtRegion resignFirstResponder];
        NSString * ID = (NSString *)arrofKeys[row];
        [self loadCitybyRegionID:ID];
        
        self.txtCity.text = nil;
        SelectedCityID = nil;
        self.circle.text = nil;
        SelectedDivisionID = nil;
        self.txtCenter.text = nil;
        SelectedCenterID = nil;
        
        if ([ID isEqualToString:@"0"] ) {
            self.txtCity.enabled = false;
            self.txtCenter.enabled = false;
            self.circle.enabled = false;
            self.ckCity.enabled = false;
            self.ckCircle.enabled = false;
            self.ckCenter.enabled = false;
            
            self.ckCity.on = false;
            self.ckCircle.on = false;
            self.ckCenter.on = false;
            
        }else
        {
            //  self.txtCity.enabled = true;
            self.ckCity.enabled = YES;
            //              self.txtCenter.enabled = true;
            //              self.circle.enabled = true;
        }
        SelectedRegionID = ID;
        
    } else if (pickerView.tag == 3) {
        NSArray * arrofKeys = [[dicAges allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * arrofValues = [dicAges objectsForKeys: arrofKeys notFoundMarker: [NSNull null]];
        
        self.txtage.text = arrofValues[row];
        [self.txtage resignFirstResponder];
        NSString * ID = (NSString *)arrofKeys[row];
        SelectedAge = ID;
    }else if (pickerView.tag == 6) {
        NSArray * arrofKeys = [[dicCenter allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * arrofValues = [dicCenter objectsForKeys: arrofKeys notFoundMarker: [NSNull null]];
        
        self.txtCenter.text = arrofValues[row];
        [self.txtCenter resignFirstResponder];
        NSString * ID = (NSString *)arrofKeys[row];
        SelectedCenterID =ID;
        
    }else if (pickerView.tag == 7) {
        NSArray * arrofKeys = [[dicCity allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * arrofValues = [dicCity objectsForKeys: arrofKeys notFoundMarker: [NSNull null]];
        
        self.txtCity.text = arrofValues[row];
        NSString * ID = (NSString *)arrofKeys[row];
        [self.txtCity resignFirstResponder];
        [self loadDivisionByCityID:ID];
        self.circle.text = nil;
        SelectedDivisionID = nil;
        self.txtCenter.text = nil;
        SelectedCenterID = nil;
        
        if ([ID isEqualToString:@"0"] ) {
            
            
            
            self.txtCenter.enabled = false;
            self.circle.enabled = false;
            self.ckCircle.enabled = false;
            self.ckCenter.enabled = false;
            
            self.ckCircle.on = false;
            self.ckCenter.on = false;
            
            
        } else
        {
            self.ckCircle.enabled = true;
        }
        SelectedCityID =ID;
    }else if (pickerView.tag == 8) {
        NSArray * arrofKeys = [[dicExReason allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * arrofValues = [dicExReason objectsForKeys: arrofKeys notFoundMarker: [NSNull null]];
        
        self.txtExReason.text = arrofValues[row];
        NSString * ID = (NSString *)arrofKeys[row];
        [self.txtExReason resignFirstResponder];
        SelectedExReason =ID;
    }

    
}
//
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* tView = (UILabel*)view;
    
    if (!tView){
        tView = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        tView.font = [UIFont fontWithName:@"Arial Black" size:11.0];
        tView.textAlignment = NSTextAlignmentCenter;
        tView.textColor = [UIColor colorWithRed:0.0392 green:0.4470 blue:0.6156 alpha:1.0];
    }
    if (pickerView.tag == 1) {
        NSArray * sortedKeys = [[dicDivision allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * objects = [dicDivision objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        tView.text= objects[row];
        return tView;
    }
    if (pickerView.tag == 2) {
        NSArray * sortedKeys = [[dicRegions allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * objects = [dicRegions objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        tView.text= objects[row];
        return tView;
    } else if (pickerView.tag == 3) {
        NSArray * sortedKeys = [[dicAges allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * objects = [dicAges objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        tView.text= objects[row];
        return tView;
    } else if (pickerView.tag == 6) {
        NSArray * sortedKeys = [[dicCenter allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * objects = [dicCenter objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        tView.text= objects[row];
        return tView;
    }else if (pickerView.tag == 7) {
        NSArray * sortedKeys = [[dicCity allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * objects = [dicCity objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        tView.text= objects[row];
        return tView;
    }else if (pickerView.tag == 8) {
        NSArray * sortedKeys = [[dicExReason allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * objects = [dicExReason objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        tView.text= objects[row];
        return tView;
    }

    
    else
    {
        return NULL;
    }
}
- (IBAction)ShowInMap:(id)sender {
    
    NSString * ValidMessage =  [self Validation];
    
    if (![ValidMessage isEqualToString:@""]) {
        UIAlertView * alertValid = [[UIAlertView alloc]initWithTitle:@"تنبية" message:ValidMessage delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
        [alertValid show];
        return;
    }
    NSString * regionID;
    NSString * cityID;
    NSString * DivisionID = @"-1";
    NSString * CenterID = @"-1";
    NSString * Age;
    NSString *Gender = @"-1";
    NSString *Reason;
    NSString *ReasonText;
    //
    if (self.ckCircle.on && self.ckCircle.enabled) {
        DivisionID = SelectedDivisionID;
    }
    else
    {
        DivisionID = @"-1";
    }
    //
    if (self.ckCenter.on && self.ckCenter.enabled) {
        CenterID = SelectedCenterID;
    }
    else
    {
        CenterID = @"-1";
    }
    //
    if (self.ckCity.on && self.ckCity.enabled) {
        cityID = SelectedCityID;
    }
    else
    {
        cityID = @"-1";
    }
    //
    
    
    //if (!self.ckGender.on) {
    if (self.ckGender.on) {
        if (self.genderSeg.selectedSegmentIndex == 0) {
            Gender = @"3";
        }else if (self.genderSeg.selectedSegmentIndex == 1) {
            Gender = @"2";
        } else{
            Gender =@"1";
        }
    }
    //
    regionID = SelectedRegionID;
    if (self.ckAge.on) {
       Age = SelectedAge;
    } else
    {
         Age = @"-1";
    }

    
    RejectedVotersMapReportResultViewController *MapResult = [self.storyboard instantiateViewControllerWithIdentifier:@"RejectedVotersMapReportResult"];
    
    NSString *currentYear ;
    NSString *secondYear ;
    NSString *firstYear ;
    
    if (self.ckCurrentYear.on) {
        currentYear = @"1";
    }
    else
    {
        currentYear = @"0";
    }
    
    if (self.ckSecYear.on) {
        secondYear = @"1";
    }
    else
    {
        secondYear = @"0";
    }
    
    if (self.ckFirstYear.on) {
        firstYear = @"1";
    }
    else
    {
        firstYear = @"0";
    }
    
    if (self.ckExReason.on) {
        Reason = SelectedExReason;
        ReasonText=[dicExReason valueForKey:Reason];
    }
    else
    {
        Reason = @"-1";
        ReasonText=@"";
    }
    
     [MapResult setSearchValuesWithRegionID:regionID CityID:cityID DivisionID:DivisionID CenterID:CenterID Age:Age Sex:Gender CurrentYear:currentYear SecondYear:secondYear  FirstYear:firstYear AgesDiction:dicAges Reason:Reason disReason:ReasonText isVoters:@"1"];
    [self presentViewController:MapResult animated:YES completion:nil];
    
}
//
-(NSString *)Validation
{
    NSString * ValidationMessage =@"";
    
    if (SelectedRegionID == nil) {
        ValidationMessage = [NSString stringWithFormat:@"%@-%@",ValidationMessage,@"من فضلك أختر لجنة انتخابية"];
        
    }else
        if (SelectedCityID == nil && self.ckCity.on && ![SelectedRegionID isEqualToString:@"0"]) {
            ValidationMessage = [NSString stringWithFormat:@"%@\n-%@",ValidationMessage,@"من فضلك أختر بلدية"];
        }else
            if (SelectedDivisionID == nil && self.ckCircle.on && ![SelectedCityID isEqualToString:@"0"] && SelectedCityID) {
                ValidationMessage = [NSString stringWithFormat:@"%@\n-%@",ValidationMessage,@"من فضلك أختر الدائرة الانتخابية"];
            }else
                if (SelectedCenterID == nil && self.ckCenter.on && ![SelectedDivisionID isEqualToString:@"0"] && SelectedDivisionID) {
                    ValidationMessage = [NSString stringWithFormat:@"%@\n-%@",ValidationMessage,@"من فضلك أختر المركز الانتخابى"];
                }
    if (SelectedAge == nil&& self.ckAge.on) {
        ValidationMessage = [NSString stringWithFormat:@"%@\n-%@",ValidationMessage,@"من فضلك أختر الفئة العمرية"];
        
    }
  
    if (SelectedExReason == nil && self.ckExReason.on ) {
        ValidationMessage = [NSString stringWithFormat:@"%@\n-%@",ValidationMessage,@"من فضلك أختر سبب الاستبعاد"];
    }
    //
    
    if (!self.ckCurrentYear.on && !self.ckSecYear.on && !self.ckFirstYear.on) {
        //
          ValidationMessage = [NSString stringWithFormat:@"%@\n-%@",ValidationMessage,@"من فضلك أختر مرحلة انتخابية واحدة على الاقل"];
    }
    return ValidationMessage;
}
//
- (IBAction)ckAgeChanged:(id)sender {
    self.txtage.enabled = self.ckAge.on;
}
- (IBAction)ckGenderChanged:(id)sender {
    
    if (self.ckGender.on) {
        self.genderSeg.enabled = true;
    }else
    {
        self.genderSeg.enabled = false;
    }
}


- (IBAction)ckCenterChanged:(id)sender {
    
    if (self.ckCenter.on && SelectedDivisionID && ![SelectedDivisionID isEqualToString: @"0"]) {
        self.txtCenter.enabled = true;
    }else
    {
        self.txtCenter.enabled = false;
    }
    
}


- (IBAction)ckDivitionChanged:(id)sender {
    if (self.ckCircle.on && SelectedCityID && ![SelectedCityID isEqualToString: @"0"]) {
        self.circle.enabled = true;
        
        if ( SelectedDivisionID &&  ![SelectedDivisionID isEqualToString: @"0"]) {
            self.ckCenter.enabled = true;
        }
    }else
    {
        self.circle.enabled = false;
        self.ckCenter.on = false;
        self.ckCenter.enabled = false;
        
        self.txtCenter.enabled = false;
    }
}

- (IBAction)ckCityChanged:(id)sender {
    if (self.ckCity.on && SelectedRegionID &&  ![SelectedRegionID isEqualToString: @"0"]) {
        self.txtCity.enabled = true;
        
        if ( SelectedCityID &&  ![SelectedCityID isEqualToString: @"0"]) {
            self.ckCircle.enabled = true;
        }
        
    }else
    {
        self.txtCity.enabled = false;
        self.ckCircle.on = false;
        self.ckCenter.on = false;
        self.ckCircle.enabled = false;
        self.ckCenter.enabled = false;
        
        self.txtCenter.enabled = false;
        self.circle.enabled = false;
    }
    
    
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)ckExReasonChanged:(id)sender {
    if (self.ckExReason.on) {
        self.txtExReason.enabled = true;
    }else
    {
        self.txtExReason.enabled = false;
    }
}


@end
