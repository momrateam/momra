//
//  GateEntryMapReportViewController.m
//  MOMRA
//
//  Created by aya on 7/9/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "GateEntryMapReportViewController.h"
#import "GateEntryInfoWindow.h"
#import "staticVariables.h"

@interface GateEntryMapReportViewController ()
@property (nonatomic ,strong ) NSMutableArray * regions;
@property (nonatomic ,strong ) NSMutableDictionary * parameters;
@end

@implementation GateEntryMapReportViewController
@synthesize regions;
@synthesize parameters;
static MKAnnotationView *openedAnnotationView;
static KMLParser *kmlParser;
static KMLParser *kmlParserCover;
static bool isOverlayAdded;
static NSArray *overlays ;
static NSArray *KMLAnnonations ;
static int SearchTypeID ; //1:Region ,2: city , 3:Divition , 4: Center

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
    self.mapView.delegate = self;
  [self setZoomLevel];
    [self loadData];
    //
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:singleTap];
    openedAnnotationView = nil;
    //
    // Locate the path to the route.kml file in the application's bundle
    // and parse it with the KMLParser.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AllDevisions" ofType:@"kml"];
    NSURL *url = [NSURL fileURLWithPath:path];
    kmlParser = [[KMLParser alloc] initWithURL:url];
     [kmlParser parseKML];
    //
NSURL *urlCover = [NSURL URLWithString:@"http://services.intekhab.gov.sa/GeoDashWebService/covers.kml"];
    kmlParserCover = [[KMLParser alloc] initWithURL:urlCover];
    [kmlParserCover parseKML];
    [self.mapView addOverlays:[kmlParserCover overlays]];
    [self.mapView addAnnotations:[kmlParserCover points]];

    
    // Add all of the MKOverlay objects parsed from the KML file to the map.
    overlays = [kmlParser overlays];
    
    
    // Add all of the MKAnnotation objects parsed from the KML file to the map.
    KMLAnnonations = [kmlParser points];
 

}
- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    if (!openedAnnotationView)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:openedAnnotationView];
    DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
    
    BOOL isCallout = (CGRectContainsPoint(DXview.calloutView.frame, touchPoint));
    BOOL isPin = (CGRectContainsPoint(DXview.pinView.frame, touchPoint));
    if (isCallout|| isPin) {
        return;
    }
    if ([openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];

    }
}
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setSearchValuesWithRegionID:(NSString *)RegionID
{
    parameters=[[NSMutableDictionary alloc]init];
    [parameters setObject:RegionID forKey:@"RegionID"];

        SearchTypeID = 1;
    
}

-(void)loadData
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",[parameters valueForKey:@"RegionID"],@"RegionID", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"SearchAdminUsers"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [self.activity startAnimating];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             [self.activity stopAnimating];
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
             NSString * results =[receivedData objectForKey:@"d"] ;
             regions=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:NSJSONReadingAllowFragments
                                                       error:&err];
             [self addMarkers];
         }else
         {
             [self.activity stopAnimating];
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
}
-(void)setZoomLevel{
    float zoomLevel = 20.0 ;
    switch (SearchTypeID) {
        case 1:
            zoomLevel = [[staticVariables singelton].RegionZoomLevel floatValue];
            break;
        case 2:
            zoomLevel = [[staticVariables singelton].CityZoomLevel floatValue];
            break;
        case 3:
            zoomLevel = [[staticVariables singelton].DivistionZoomLevel floatValue];
            break;
        case 4:
            zoomLevel = [[staticVariables singelton].CenterZoomLevel floatValue];
            break;
    }
    
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = zoomLevel;
    zoomIn.span.latitudeDelta  = zoomLevel;
    [self.mapView setRegion:zoomIn animated:NO];
    
}
-(void)addMarkers
{
    for (int i=0; i<regions.count; i++) {
        NSDictionary * reg =[regions objectAtIndex:i];
        CLLocationCoordinate2D coordinate =  CLLocationCoordinate2DMake([[reg objectForKey:@"lat"]doubleValue],[[reg objectForKey:@"long"]doubleValue]);
        MapLocationView *annotation = [[MapLocationView alloc] initWithName:[NSString stringWithFormat:@"%d",i] address:nil  coordinate:coordinate] ;
        [_mapView addAnnotation:annotation];

    }
    //
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in [_mapView annotations]) {
        if ([annotation isKindOfClass:[MapLocationView class]]) {
            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
            if (MKMapRectIsNull(flyTo)) {
                flyTo = pointRect;
            } else {
                flyTo = MKMapRectUnion(flyTo, pointRect);
            }
        }
    }

    
    // Position the map so that all overlays and annotations are visible on screen.
    if (flyTo.origin.x != INFINITY && flyTo.origin.y != INFINITY) {
        _mapView.visibleMapRect = flyTo;
    }
    //
    if (flyTo.size.width ==0 || flyTo.size.height ==0) {
        [self  setZoomLevel];
    }
}
#pragma mark - map delegate
//
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    if (mapView.region.span.latitudeDelta <= 3.0f && isOverlayAdded == NO) {
        [mapView addOverlays:overlays];
        [mapView addAnnotations:KMLAnnonations];
        isOverlayAdded = YES;
    } else  if (mapView.region.span.latitudeDelta > 3.0f && isOverlayAdded == YES){
        [mapView removeOverlays:overlays];
        [mapView removeAnnotations:KMLAnnonations];
        isOverlayAdded = NO;
        
    }
}
//
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKOverlayView * view = [[MKOverlayView alloc]init];
    if ([kmlParserCover viewForOverlay:overlay]) {
        view =[kmlParserCover viewForOverlay:overlay];
    }
    else
    {
        view =[kmlParser viewForOverlay:overlay];
    }
    return view;
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MapLocationView class]])
    {
        UIView *pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GreenPin.png"]];
        
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
        if (!annotationView) {
            
            DXAnnotationSettings *newSettings = [DXAnnotationSettings defaultSettings];
            newSettings.animationType = DXCalloutAnimationNone ;
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:nil
                                                                 settings:newSettings];
        }
        return annotationView;
    }
    else
    {
        return [kmlParser viewForAnnotationWithTitle:annotation forMapView:mapView];
    }
    
}
//
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
        return;
    }
    
    DXAnnotationView * DXview = (DXAnnotationView *)view;
        if (openedAnnotationView &&openedAnnotationView == view ) {
        DXview.layer.zPosition = 1;
        [self.mapView selectAnnotation:[view annotation] animated:NO];
    }
    else
    {
        DXview.layer.zPosition = -2;
    }
}
//

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
        return;
    }
    
        if (openedAnnotationView != nil&&openedAnnotationView != view) {
         view.layer.zPosition = -2;
        return;
    }else if(openedAnnotationView != nil&&openedAnnotationView == view)
    {
         return;
    }else
    {
        openedAnnotationView = view;
    }
    
    id annotation = [view annotation];
    UIView *infoWindow = [[UIView alloc]init];
    DXAnnotationView * DXview = (DXAnnotationView *)view;
    [DXview addNewCallOutView:infoWindow];
    view = (MKAnnotationView *)DXview;
    
    if ([view isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)view)showCalloutView];
        
        view.layer.zPosition = 0;
    }
    
    NSInteger index=[[annotation name] integerValue];
    NSDictionary * reg =[self.regions objectAtIndex:index];
    //
    //UIView * infoWindow=[[UIView alloc]init];
    [infoWindow setFrame:CGRectMake(0, 0, 280, 65)];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [infoWindow setFrame:CGRectMake(0, 0, 370, 100)];
    }
    [infoWindow setBackgroundColor:[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0]];
    //
    UILabel * titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(5, 0, 270, 44)];
    [titleLabel setTextColor:[UIColor colorWithRed:0/255.0 green:126/255.0 blue:89/255.0 alpha:1.0]];
    titleLabel.textAlignment=NSTextAlignmentRight;
    NSString *title = [NSString stringWithFormat:@"%@ %@",@"إحصائيات",[reg objectForKey:@"Name"]];
    NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc] initWithString:title];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:17.0] range:NSMakeRange(0,title.length)];
        [titleLabel setFrame:CGRectMake(10, 0, 352, 55)];
    }
    else{
            [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,title.length)];
    }
    [attributedTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,title.length)];
    titleLabel.attributedText=attributedTitleString;
    titleLabel.numberOfLines=0;
    [infoWindow addSubview:titleLabel];
    //
    UILabel *SubTitleLabel =[[UILabel alloc]initWithFrame:CGRectMake(170, 45, 105, 15)];
    //[SubTitleLabel setBackgroundColor:[UIColor yellowColor]];
    [SubTitleLabel setTextColor:[UIColor colorWithRed:61/255.0 green:35/255.0 blue:9/255.0 alpha:1.0]];
    [SubTitleLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:13.0]];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [SubTitleLabel setFrame:CGRectMake(193, 60, 169, 19)];
        [SubTitleLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:17.0]];
    }
    SubTitleLabel.textAlignment=NSTextAlignmentRight;
    [SubTitleLabel setText:@"مستخدمي النظام "];
    [infoWindow addSubview:SubTitleLabel];
    //
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formatter setGroupingSeparator:groupingSeparator];
    [formatter setGroupingSize:3];
    [formatter setAlwaysShowsDecimalSeparator:NO];
    [formatter setUsesGroupingSeparator:YES];
    //
    UILabel *numberLabel =[[UILabel alloc]initWithFrame:CGRectMake(80, 45, 85, 15)];
    //[numberLabel setBackgroundColor:[UIColor cyanColor]];
    [numberLabel setTextColor:[UIColor colorWithRed:11/255.0 green:93/255.0 blue:60/255.0 alpha:1.0]];
    [numberLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:11.0]];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [numberLabel setFrame:CGRectMake(50, 60, 169, 19)];
        [numberLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:15.0]];
    }
    numberLabel.textAlignment=NSTextAlignmentRight;
    [numberLabel setText:[formatter stringFromNumber:[reg objectForKey:@"UsersCount"]]];
    [infoWindow addSubview:numberLabel];
    //
    CGRect BarChartFrame = infoWindow.frame;
    BarChartFrame.origin.y =20;
    BarChartFrame.origin.x = -((BarChartFrame.size.width-30)/2);
    infoWindow.frame =BarChartFrame;
    [mapView setCenterCoordinate:[annotation coordinate] animated:YES];
    

}
//
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (openedAnnotationView && [openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        
        
        DXAnnotationView * newView = (DXAnnotationView *)openedAnnotationView;
        if (newView) {
             UIView *  viewStatistics =   ( UIView *)newView.calloutView;
            id<MKAnnotation> annotation = [newView annotation];
            //
            MKMapRect r = [self.mapView visibleMapRect];
            MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
            CGRect BarChartFrame = viewStatistics.frame;
            r.origin.x = pt.x - r.size.width * 0.5;
            r.origin.y = pt.y ;
            BarChartFrame.origin.y =20;
            BarChartFrame.origin.x = -((BarChartFrame.size.width-30)/2);

            viewStatistics.frame =BarChartFrame;
            [self.mapView setVisibleMapRect:r animated:YES];
            
        }
    }
    
}
//
@end
