//
//  VotersCentersMapReportViewController.m
//  MOMRA
//
//  Created by aya on 7/8/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "VotersCentersMapReportViewController.h"
#import "MOMRA-Swift.h"
#import "staticVariables.h"

@interface VotersCentersMapReportViewController ()
@property (nonatomic ,strong ) NSMutableArray * regionsCurrentYear;
@property (nonatomic ,strong ) NSMutableArray * regionsFirstYear;
@property (nonatomic ,strong ) NSMutableArray * regionsSecondYear;
@property (nonatomic ,strong ) NSMutableDictionary * parameters;
@end

@implementation VotersCentersMapReportViewController
@synthesize regionsCurrentYear;
@synthesize regionsFirstYear;
@synthesize regionsSecondYear;
@synthesize parameters;

static bool isFirstChart;
static bool isSecondChart;
static bool isThirdChart;
static PieChartView *secondChart;
static PieChartView *thirdChart;
static NSMutableDictionary * dictAges;
static MKAnnotationView *openedAnnotationView;
static KMLParser *kmlParser;
static KMLParser *kmlParserCover;
static bool isOverlayAdded;
static NSArray *overlays ;
static NSArray *KMLAnnonations ;
static int SearchTypeID ; //1:Region ,2: city , 3:Divition , 4: Center

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    @try {
        self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
        self.mapView.delegate = self;
  [self setZoomLevel];
        [self loadData];
        //
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [self.mapView addGestureRecognizer:singleTap];
        openedAnnotationView = nil;
        //
        // Locate the path to the route.kml file in the application's bundle
        // and parse it with the KMLParser.
        NSString *path = [[NSBundle mainBundle] pathForResource:@"AllDevisions" ofType:@"kml"];
        NSURL *url = [NSURL fileURLWithPath:path];
        kmlParser = [[KMLParser alloc] initWithURL:url];
         [kmlParser parseKML];
    //
NSURL *urlCover = [NSURL URLWithString:@"http://services.intekhab.gov.sa/GeoDashWebService/covers.kml"];
    kmlParserCover = [[KMLParser alloc] initWithURL:urlCover];
    [kmlParserCover parseKML];
    [self.mapView addOverlays:[kmlParserCover overlays]];
    [self.mapView addAnnotations:[kmlParserCover points]];

        
        // Add all of the MKOverlay objects parsed from the KML file to the map.
        overlays = [kmlParser overlays];
        
        // Add all of the MKAnnotation objects parsed from the KML file to the map.
        KMLAnnonations = [kmlParser points];
     }
    @catch (NSException *exception) {
        
    }
}
//
- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    if (!openedAnnotationView)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:openedAnnotationView];
    DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
    
    BOOL isCallout = (CGRectContainsPoint(DXview.calloutView.frame, touchPoint));
    BOOL isPin = (CGRectContainsPoint(DXview.pinView.frame, touchPoint));
    if (isCallout|| isPin) {
        return;
    }
    if ([openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];

    }
}
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    @try {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)setSearchValuesWithRegionID:(NSString *)RegionID CityID:(NSString *)CityID DivisionID:(NSString *)DivisionID Sex:(NSString *)Sex CurrentYear:(NSString *)CurrentYear SecondYear:(NSString * )SecondYear FirstYear:(NSString *)FirstYear isVoters:(NSString *)isVoters
{
    parameters=[[NSMutableDictionary alloc]init];
    [parameters setObject:RegionID forKey:@"RegionID"];
    [parameters setObject:CityID forKey:@"CityID"];
    [parameters setObject:DivisionID forKey:@"DivisionID"];
    [parameters setObject:Sex forKey:@"Sex"];
    [parameters setObject:Sex forKey:@"Duration"];
    [parameters setObject:CurrentYear forKey:@"CurrentYear"];
    [parameters setObject:SecondYear forKey:@"SecondYear"];
    [parameters setObject:FirstYear forKey:@"FirstYear"];
    [parameters setObject:isVoters forKey:@"isVoters"];
    //
    if (![DivisionID isEqualToString:@"-1"])
    {
        SearchTypeID = 3;
    }
    else if (![CityID isEqualToString:@"-1"])
    {
        SearchTypeID = 2;
    }
    else
    {
        SearchTypeID = 1;
    }
}

-(void)loadData
{
    [self.activity startAnimating];
    if ([[parameters valueForKey:@"isVoters"] isEqualToString:@"1"]) {
        NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",[parameters valueForKey:@"RegionID"],@"RegionID",[parameters valueForKey:@"CityID"],@"CityID",[parameters valueForKey:@"DivisionID"],@"DivisionID",[parameters valueForKey:@"Sex"],@"Sex",[parameters valueForKey:@"CurrentYear"],@"CurrentYear",[parameters valueForKey:@"SecondYear"],@"SecondYear",[parameters valueForKey:@"FirstYear"],@"FirstYear", nil];
        NSError *error = nil;
        NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
        NSURL *url = [Helper getWebserviceURL:@"VotersCenterSearch"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 [self.activity stopAnimating];
                 NSError * err =nil;
                 NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                               options:NSJSONReadingAllowFragments
                                                                                 error:&err];
                 
                 NSString * results =[receivedData objectForKey:@"d"] ;
                 NSDictionary * allData=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                                        options:NSJSONReadingAllowFragments
                                                                          error:&err];
                 if (allData != Nil) {
                     if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
                         NSString * CurrentYear=[allData objectForKey:@"CurrentYear"];
                         regionsCurrentYear=[NSJSONSerialization JSONObjectWithData:[CurrentYear dataUsingEncoding:NSUTF8StringEncoding]
                                                                            options:NSJSONReadingAllowFragments
                                                                              error:&err];
                     }
                     //
                     if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
                         NSString * CurrentYear=[allData objectForKey:@"FirstYear"];
                         regionsFirstYear=[NSJSONSerialization JSONObjectWithData:[CurrentYear dataUsingEncoding:NSUTF8StringEncoding]
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                     }
                     //
                     if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
                         NSString * CurrentYear=[allData objectForKey:@"SecondYear"];
                         regionsSecondYear=[NSJSONSerialization JSONObjectWithData:[CurrentYear dataUsingEncoding:NSUTF8StringEncoding]
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
                     }
                     //
                     [self addMarkers];
                 }
             }else
             {
                 [self.activity stopAnimating];
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
             }
         }];
    }
    else
    {
        NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",[parameters valueForKey:@"RegionID"],@"RegionID",[parameters valueForKey:@"CityID"],@"CityID",[parameters valueForKey:@"DivisionID"],@"DivisionID",[parameters valueForKey:@"Sex"],@"Sex",[parameters valueForKey:@"CurrentYear"],@"CurrentYear",[parameters valueForKey:@"SecondYear"],@"SecondYear",[parameters valueForKey:@"FirstYear"],@"FirstYear", nil];
        NSError *error = nil;
        NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
        NSURL *url = [Helper getWebserviceURL:@"CanCenterSearch"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 [self.activity stopAnimating];
                 NSError * err =nil;
                 NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                               options:NSJSONReadingAllowFragments
                                                                                 error:&err];
                 
                 NSString * results =[receivedData objectForKey:@"d"] ;
                 NSDictionary * allData=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                                        options:NSJSONReadingAllowFragments
                                                                          error:&err];
                 if (allData != Nil) {
                     if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
                         NSString * CurrentYear=[allData objectForKey:@"CurrentYear"];
                         regionsCurrentYear=[NSJSONSerialization JSONObjectWithData:[CurrentYear dataUsingEncoding:NSUTF8StringEncoding]
                                                                            options:NSJSONReadingAllowFragments
                                                                              error:&err];
                     }
                     //
                     if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
                         NSString * CurrentYear=[allData objectForKey:@"FirstYear"];
                         regionsFirstYear=[NSJSONSerialization JSONObjectWithData:[CurrentYear dataUsingEncoding:NSUTF8StringEncoding]
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                     }
                     //
                     if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
                         NSString * CurrentYear=[allData objectForKey:@"SecondYear"];
                         regionsSecondYear=[NSJSONSerialization JSONObjectWithData:[CurrentYear dataUsingEncoding:NSUTF8StringEncoding]
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
                     }
                     //
                     [self addMarkers];
                 }
             }else
             {
                 [self.activity stopAnimating];
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
             }
         }];
    }
}
-(void)setZoomLevel{
    float zoomLevel = 20.0 ;
    switch (SearchTypeID) {
        case 1:
            zoomLevel = [[staticVariables singelton].RegionZoomLevel floatValue];
            break;
        case 2:
            zoomLevel = [[staticVariables singelton].CityZoomLevel floatValue];
            break;
        case 3:
            zoomLevel = [[staticVariables singelton].DivistionZoomLevel floatValue];
            break;
        case 4:
            zoomLevel = [[staticVariables singelton].CenterZoomLevel floatValue];
            break;
    }
    
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = zoomLevel;
    zoomIn.span.latitudeDelta  = zoomLevel;
    [self.mapView setRegion:zoomIn animated:NO];
    
}
-(void)addMarkers
{
    @try {
        NSMutableArray * pinsArr=[[NSMutableArray alloc] init];
        //
        if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
            for (int i=0; i<regionsCurrentYear.count; i++) {
                NSDictionary * reg =[regionsCurrentYear objectAtIndex:i];
                [pinsArr addObject:reg];
            }
        }
        else if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
            for (int i=0; i<regionsSecondYear.count; i++) {
                NSString * stringId=[[[regionsSecondYear objectAtIndex:i]valueForKey:@"ID"] stringValue];
                Boolean found=false;
                for (int j=0; j<pinsArr.count; j++) {
                    NSString * stringId2=[[[pinsArr objectAtIndex:j]valueForKey:@"ID"] stringValue];
                    if ([stringId isEqual:stringId2]) {
                        found=true;
                        break;
                    }
                }
                if (!found) {
                    NSDictionary * reg =[regionsSecondYear objectAtIndex:i];
                    [pinsArr addObject:reg];
                }
            }
        }
        else if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
            for (int i=0; i<regionsFirstYear.count; i++) {
                NSString * stringId=[[[regionsFirstYear objectAtIndex:i]valueForKey:@"ID"] stringValue];
                Boolean found=false;
                for (int j=0; j<pinsArr.count; j++) {
                    NSString * stringId2=[[[pinsArr objectAtIndex:j]valueForKey:@"ID"] stringValue];
                    if ([stringId isEqual:stringId2]) {
                        found=true;
                        break;
                    }
                }
                if (!found) {
                    NSDictionary * reg =[regionsFirstYear objectAtIndex:i];
                    [pinsArr addObject:reg];
                }
            }
        }
        //
        for (int i=0; i<pinsArr.count; i++) {
            NSDictionary * reg =[pinsArr objectAtIndex:i];
            CLLocationCoordinate2D coordinate =  CLLocationCoordinate2DMake([[reg objectForKey:@"lat"]doubleValue],[[reg objectForKey:@"long"]doubleValue]);
            MapLocationView *annotation = [[MapLocationView alloc] initWithName:[[reg objectForKey:@"ID"] stringValue ] address:nil  coordinate:coordinate] ;
            [_mapView addAnnotation:annotation];
        }
        //
        MKMapRect flyTo = MKMapRectNull;
        for (id <MKAnnotation> annotation in [_mapView annotations]) {
            if ([annotation isKindOfClass:[MapLocationView class]]) {
                MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
                if (MKMapRectIsNull(flyTo)) {
                    flyTo = pointRect;
                } else {
                    flyTo = MKMapRectUnion(flyTo, pointRect);
                }
            }
        }
        
        //
        
        
        // Position the map so that all overlays and annotations are visible on screen.
        if (flyTo.origin.x != INFINITY && flyTo.origin.y != INFINITY) {
            _mapView.visibleMapRect = flyTo;
        }
        //
        if (flyTo.size.width ==0 || flyTo.size.height ==0) {
            [self  setZoomLevel];
        }

    }
    @catch (NSException *exception) {
        
    }
}
#pragma mark - map delegate
//
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    if (mapView.region.span.latitudeDelta <= 3.0f && isOverlayAdded == NO) {
        [mapView addOverlays:overlays];
        [mapView addAnnotations:KMLAnnonations];
        isOverlayAdded = YES;
    } else  if (mapView.region.span.latitudeDelta > 3.0f && isOverlayAdded == YES){
        [mapView removeOverlays:overlays];
        [mapView removeAnnotations:KMLAnnonations];
        isOverlayAdded = NO;
        
    }
}
//
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKOverlayView * view = [[MKOverlayView alloc]init];
    if ([kmlParserCover viewForOverlay:overlay]) {
        view =[kmlParserCover viewForOverlay:overlay];
    }
    else
    {
        view =[kmlParser viewForOverlay:overlay];
    }
    return view;
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MapLocationView class]])
    {
        UIView *pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GreenPin.png"]];
        
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
        if (!annotationView) {
            
            DXAnnotationSettings *newSettings = [DXAnnotationSettings defaultSettings];
            newSettings.animationType = DXCalloutAnimationNone ;
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:nil
                                                                 settings:newSettings];
        }
        return annotationView;
    }
    else
    {
        return [kmlParser viewForAnnotationWithTitle:annotation forMapView:mapView];
    }
    
}
//
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
        return;
    }
    
    DXAnnotationView * DXview = (DXAnnotationView *)view;
        if (openedAnnotationView &&openedAnnotationView == view ) {
        DXview.layer.zPosition = 1;
        [self.mapView selectAnnotation:[view annotation] animated:NO];
    }
    else
    {
        DXview.layer.zPosition = -2;
    }
}
//
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (openedAnnotationView && [openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        DXAnnotationView * newView = (DXAnnotationView *)openedAnnotationView;
        if (newView) {
            //
            PieChartInfoWindow *  PieChartInfoView =   (PieChartInfoWindow *)newView.calloutView;
            id<MKAnnotation> annotation = [newView annotation];
            MKMapRect r = [self.mapView visibleMapRect];
            MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
            CGRect BarChartFrame = PieChartInfoView.frame;
            r.origin.x = pt.x - r.size.width * 0.5;
            r.origin.y = pt.y ;
            BarChartFrame.origin.y =20;
            PieChartInfoView.frame =BarChartFrame;
            [self.mapView setVisibleMapRect:r animated:YES];
        }
    }
}
//
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
        return;
    }
    
        if (openedAnnotationView != nil&&openedAnnotationView != view) {
         view.layer.zPosition = -2;
        return;
    }else if(openedAnnotationView != nil&&openedAnnotationView == view)
    {
         return;
    }else
    {
        openedAnnotationView = view;
    }
    
    id annotation = [view annotation];
    UIView *calloutView = [[[NSBundle mainBundle] loadNibNamed:@"PieChartInfoWindow" owner:self options:nil] firstObject];
    DXAnnotationView * DXview = (DXAnnotationView *)view;
    [DXview addNewCallOutView:calloutView];
    view = (MKAnnotationView *)DXview;
    
    if ([view isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)view)showCalloutView];
        
        view.layer.zPosition = 0;
    }
      @try {
        secondChart=Nil;
        thirdChart=Nil;
          isFirstChart=NO;
          isSecondChart=NO;
          isThirdChart=NO;
        //
        DXAnnotationView * newView = (DXAnnotationView * )view;

        PieChartInfoWindow *PieChartInfoView =  (PieChartInfoWindow *)newView.calloutView;
        //
        
        NSString * ID=[annotation name];
        //
        UIButton *btnFullScreen = [UIButton buttonWithType:UIButtonTypeRoundedRect];
          if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
              btnFullScreen.frame = CGRectMake(230, 380, 270 , 30);
          }else{
              btnFullScreen.frame = CGRectMake(40, 282, 270 , 30);
          }
        //
        NSString *btnFullScreenTitle = @"عرض الإحصائيات على الشاشة الكاملة";
        NSMutableAttributedString *btnFullScreenTitleString = [[NSMutableAttributedString alloc] initWithString:btnFullScreenTitle];
          if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
              [btnFullScreenTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:16.0] range:NSMakeRange(0,btnFullScreenTitle.length)];
          }else{
              [btnFullScreenTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:13.0] range:NSMakeRange(0,btnFullScreenTitle.length)];
          }
        [btnFullScreenTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,btnFullScreenTitle.length)];
        //
        [btnFullScreen setAttributedTitle:btnFullScreenTitleString forState:(UIControlState)UIControlStateNormal];
        //
        [btnFullScreen addTarget:self
                          action:@selector(fullScreenPressed:)
                forControlEvents:UIControlEventTouchUpInside];
        btnFullScreen.tag = [ID integerValue];
        
        [PieChartInfoView addSubview:btnFullScreen];
        //
        

        //
        NSMutableString * criteria=[[NSMutableString alloc]init];
        //
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"]) {
            
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
        {
            [criteria appendString:@"النوع : ذكور "];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
        {
            [criteria appendString:@"النوع : إناث "];
        }
        else
        {
            [criteria appendString:@"النوع : الكل "];
        }
        //
        PieChartInfoView.criteria.text=criteria;
        //
        NSDictionary * reg;//NSDictionary * regSecondYear; NSDictionary * regFirstYear;
        int numberOfCharts=0;
        if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
            for (int i=0; i< regionsCurrentYear.count; i++) {
                NSString * stringId=[[[regionsCurrentYear objectAtIndex:i]valueForKey:@"ID"]stringValue];
                if ([stringId isEqual:ID]) {
                    reg =[self.regionsCurrentYear objectAtIndex:i];
                    [Helper configurePieChart:PieChartInfoView.PieChart];
                    isFirstChart=YES;
                    PieChartInfoView.PieChart.userInteractionEnabled = NO;

                    //
                    [self loadChart:reg withPieChartView:PieChartInfoView.PieChart  title:@"(الدورة الثالثة)"];
                    numberOfCharts++;
                    break;
                }
            }
        }
        if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
            for (int i=0; i< regionsSecondYear.count; i++) {
                NSString * stringId=[[[regionsSecondYear objectAtIndex:i]valueForKey:@"ID"]stringValue];
                if ([stringId isEqual:ID]) {
                    reg =[self.regionsSecondYear objectAtIndex:i];
                    if (numberOfCharts == 0) {
                        [Helper configurePieChart:PieChartInfoView.PieChart];
                        isFirstChart=YES;
                        [self loadChart:reg withPieChartView:PieChartInfoView.PieChart title:@"(الدورة الثانية)"];
                        numberOfCharts++;
                    }
                    else{
                        secondChart=[[PieChartView alloc] initWithFrame:CGRectMake(0, PieChartInfoView.PieChart.frame.origin.y+(numberOfCharts*PieChartInfoView.PieChart.frame.size.height), PieChartInfoView.PieChart.frame.size.width,PieChartInfoView.PieChart.frame.size.height)];
                        secondChart.userInteractionEnabled = NO;
                        [PieChartInfoView addSubview:secondChart];
                        //
           
                       CGRect  frame =PieChartInfoView.frame;
                        frame.size.height=frame.size.height+PieChartInfoView.PieChart.frame.size.height;
                        PieChartInfoView.frame=frame;
                        //
                        [Helper configurePieChart:secondChart];
                        isSecondChart=YES;
                        //
                        [self loadChart:reg withPieChartView:secondChart title:@"(الدورة الثانية)"];
                        numberOfCharts++;
                    }
                    break;
                }
            }
        }
        if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
            for (int i=0; i< regionsFirstYear.count; i++) {
                NSString * stringId=[[[regionsFirstYear objectAtIndex:i]valueForKey:@"ID"]stringValue];
                if ([stringId isEqual:ID]) {
                    reg =[self.regionsFirstYear objectAtIndex:i];
                    if (numberOfCharts == 0) {
                        [Helper configurePieChart:PieChartInfoView.PieChart];
                        isFirstChart=YES;
                        [self loadChart:reg withPieChartView:PieChartInfoView.PieChart title:@"(الدورة الأولى)"];
                        numberOfCharts++;
                    }
                    else
                    {
                        thirdChart=[[PieChartView alloc] initWithFrame:CGRectMake(0, PieChartInfoView.PieChart.frame.origin.y+(numberOfCharts*PieChartInfoView.PieChart.frame.size.height), PieChartInfoView.PieChart.frame.size.width, PieChartInfoView.PieChart.frame.size.height)];
                        thirdChart.userInteractionEnabled = NO;
                        [PieChartInfoView addSubview:thirdChart];
                        //
                        CGRect  frame =PieChartInfoView.frame;
                        frame.size.height=frame.size.height+PieChartInfoView.PieChart.frame.size.height;
                        PieChartInfoView.frame=frame;
                        //
                        [Helper configurePieChart:thirdChart];
                        isThirdChart=YES;
                        //
                        [self loadChart:reg withPieChartView:thirdChart title:@"(الدورة الأولى)"];
                        numberOfCharts++;
                    }
                    break;
                }
            }
        }
        //
        NSString *title = [NSString stringWithFormat:@"%@ %@ %@",@"إحصائيات",[reg objectForKey:@"Name"],@"حسب المعايير التالية"];
        NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc] initWithString:title];
          if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
              [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:15.0] range:NSMakeRange(0,title.length)];
          }else{
              [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:12.0] range:NSMakeRange(0,title.length)];
          }
        [attributedTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,title.length)];
        PieChartInfoView.title.attributedText=attributedTitleString;
        //
        if (numberOfCharts > 1) {
            NSMutableArray * values=[[NSMutableArray alloc]init];
            NSLog(@"barChart : %ld",(long)[PieChartInfoView.PieChart yValueSum]);
//            if ([PieChartInfoView.PieChart yValueSum] != 0) {
            if (isFirstChart) {
                KeyValue * first=[[KeyValue alloc]init];
                if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
                    first.Key=@"الثالثة";
                }
                else if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
                    first.Key=@"الثانية";
                }
                else if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
                    first.Key=@"الأولى";
                }
                first.Value=[NSString stringWithFormat:@"%ld",(long)[PieChartInfoView.PieChart yValueSum]];
                [values addObject:first];
            }
            NSLog(@"secondChart : %ld",(long)[secondChart yValueSum]);
//            if ([secondChart yValueSum] != 0) {
            if (isSecondChart) {
                KeyValue * first=[[KeyValue alloc]init];
                first.Key=@"الثانية";
                first.Value=[NSString stringWithFormat:@"%ld",(long)[secondChart yValueSum]];
                [values addObject:first];
            }
            NSLog(@"thirdChart : %ld",(long)[thirdChart yValueSum]);
//            if ([thirdChart yValueSum] != 0) {
            if (isThirdChart) {
                KeyValue * first=[[KeyValue alloc]init];
                first.Key=@"الأولى";
                first.Value=[NSString stringWithFormat:@"%ld",(long)[thirdChart yValueSum]];
                [values addObject:first];
            }
            DetailsTable *DetailsTable =  [[[NSBundle mainBundle] loadNibNamed:@"DetailsTable" owner:self options:nil] objectAtIndex:0];
            [DetailsTable LoadDataWithArrayOfKeyValue:values];
            CGRect  frame =PieChartInfoView.frame;
            frame.size.height=frame.size.height+130;
            PieChartInfoView.frame=frame;
            //
            frame=DetailsTable.frame;
            frame.origin.y=frame.origin.y+PieChartInfoView.PieChart.frame.origin.y+(numberOfCharts*PieChartInfoView.PieChart.frame.size.height);
            DetailsTable.frame=frame;
            //
            btnFullScreen.frame = CGRectMake(40, DetailsTable.frame.origin.y+DetailsTable.frame.size.height, 270 , 30);

            [PieChartInfoView addSubview:DetailsTable];
            //
            
        }
          //
          [mapView setCenterCoordinate:[annotation coordinate] animated:YES];
        MKMapRect r = [mapView visibleMapRect];
          MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
          CGRect BarChartFrame = PieChartInfoView.frame;
          r.origin.x = pt.x - r.size.width * 0.5;
          r.origin.y = pt.y ;
          BarChartFrame.origin.y =20;
          PieChartInfoView.frame =BarChartFrame;
          [mapView setVisibleMapRect:r animated:YES];

    }
    @catch (NSException *exception) {
        
    }

}
-(void)fullScreenPressed:(id)sender{

    @try {
        votersCentersFullViewChartViewController *fullChart = [self.storyboard instantiateViewControllerWithIdentifier:@"votersCentersFullViewChart"];
        fullChart.parameters=parameters;
//        NSInteger index=[marker.title integerValue];
        NSString *ID= [NSString stringWithFormat:@"%ld",(long)((UIButton *) sender).tag];
//        NSDictionary * reg =[self.regions objectAtIndex:index];
        NSDictionary * regCurrent;
        NSDictionary * regFirst;
        NSDictionary * regSecond;
        if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
            for (int i=0; i< regionsCurrentYear.count; i++) {
                NSString * stringId=[[[regionsCurrentYear objectAtIndex:i]valueForKey:@"ID"]stringValue];
                if ([stringId isEqual:ID]) {
                    regCurrent =[self.regionsCurrentYear objectAtIndex:i];
                    break;
                }
            }
            //reg =[self.regionsCurrentYear objectAtIndex:index];
        }
        if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
            for (int i=0; i< regionsSecondYear.count; i++) {
                NSString * stringId=[[[regionsSecondYear objectAtIndex:i]valueForKey:@"ID"]stringValue];
                if ([stringId isEqual:ID]) {
                    regSecond =[self.regionsSecondYear objectAtIndex:i];
                    break;
                }
            }
            //reg =[self.regionsSecondYear objectAtIndex:index];
        }
        if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
            for (int i=0; i< regionsFirstYear.count; i++) {
                NSString * stringId=[[[regionsFirstYear objectAtIndex:i]valueForKey:@"ID"]stringValue];
                if ([stringId isEqual:ID]) {
                    regFirst =[self.regionsFirstYear objectAtIndex:i];
                    break;
                }
            }
            //reg =[self.regionsFirstYear objectAtIndex:index];
        }
        fullChart.regCurrent=regCurrent;
        fullChart.regFirst=regFirst;
        fullChart.regSecond=regSecond;
        [self presentViewController:fullChart animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)loadChart :(NSDictionary *)reg withPieChartView:(PieChartView *)pieChart title : (NSString *)title
{
    //male and female
    if ([[parameters valueForKey:@"Sex"] isEqualToString:@"3"]) {
        NSString * maleKey;
        NSString * femaleKey;
        if ([[parameters valueForKey:@"isVoters"] isEqualToString:@"1"]) {
            maleKey=@"VotersMaleCenterCount";
            femaleKey=@"VotersFemaleCenterCount";
        }else{
            maleKey=@"CanMaleCenterCount";
            femaleKey=@"CanFemaleCenterCount";
        }
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        double val1 = [[reg objectForKey:maleKey]doubleValue];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:0]];
        double val2 = [[reg objectForKey:femaleKey]doubleValue];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val2 xIndex:1]];
        //
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        [xVals addObject:[NSString stringWithFormat:@"ذكور : %@",[reg objectForKey:maleKey]]];
        [xVals addObject:[NSString stringWithFormat:@"إناث : %@",[reg objectForKey:femaleKey]]];
        PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:title];
        dataSet.sliceSpace = 3.0;
        
        NSMutableArray *colors = [[NSMutableArray alloc] init];
        [colors addObject:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
        [colors addObject:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
        
        dataSet.colors = colors;
        
        PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
        
        NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
        pFormatter.numberStyle = NSNumberFormatterPercentStyle;
        pFormatter.maximumFractionDigits = 1;
        pFormatter.multiplier = @1.f;
        pFormatter.percentSymbol = @" %";
        [data setValueFormatter:pFormatter];
        [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:11.f]];
        [data setValueTextColor:[UIColor blackColor]];
        //pieChart.legend.enabled = NO;
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:14.f]];
        }
        pieChart.data = data;
        [pieChart highlightValues:nil];
    }
    else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
    {
        NSString * centersKey;
        if ([[parameters valueForKey:@"isVoters"] isEqualToString:@"1"]) {
            centersKey=@"VotersCenterCount";
        }
        else{
            centersKey=@"CanCenterCount";
        }
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        double val1 = [[reg objectForKey:centersKey]doubleValue];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:0]];
        //
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"]) {
            [xVals addObject:[NSString stringWithFormat:@"ذكور : %@",[reg objectForKey:centersKey]]];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"]) {
            [xVals addObject:[NSString stringWithFormat:@"إناث : %@",[reg objectForKey:centersKey]]];
        }
        else{
            [xVals addObject:[NSString stringWithFormat:@"الكل : %@",[reg objectForKey:centersKey]]];
        }
        //[xVals addObject:[NSString stringWithFormat:@"%@",[reg objectForKey:centersKey]]];
        PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:title];
        dataSet.sliceSpace = 3.0;
        
        NSMutableArray *colors = [[NSMutableArray alloc] init];
//        [colors addObject:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
//        [colors addObject:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
//        [colors addObject:ChartColorTemplates.vordiplom[2]];
//        [colors addObject:ChartColorTemplates.vordiplom[3]];
        //
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"])
        {
            [colors addObject:[[UIColor alloc]initWithRed:0/255.0 green:136/255.0 blue:91/255.0 alpha:1.0 ]];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
        {
            [colors addObject:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
        {
            [colors addObject:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
        }
        //
        dataSet.colors = colors;
        
        PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
        
        NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
        pFormatter.numberStyle = NSNumberFormatterPercentStyle;
        pFormatter.maximumFractionDigits = 1;
        pFormatter.multiplier = @1.f;
        pFormatter.percentSymbol = @" %";
        [data setValueFormatter:pFormatter];
        [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:11.f]];
        [data setValueTextColor:[UIColor blackColor]];
        //pieChart.legend.enabled = NO;
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            [data setValueFont:[UIFont fontWithName:@"HacenSaudiArabia" size:14.f]];
        }
        pieChart.data = data;
        [pieChart highlightValues:nil];
    }
        [pieChart animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
}
#pragma mark - lazy init
//-(NSMutableArray *)regions
//{
//    if (!regions) {
//        regions=[[NSMutableArray alloc]init];
//    }
//    return regions;
//}
-(NSMutableDictionary *)parameters
{
    if (!parameters) {
        parameters=[[NSMutableDictionary alloc]init];
    }
    return parameters;
}

@end
