//
//  VoterStatisticViewController.m
//  MOMRA
//
//  Created by aya on 7/2/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "VoterStatisticViewController.h"
#import "VotersReportViewController.h"
#import "DisVotersReportViewController.h"
#import "AcceptedVotersReportViewController.h"
#import "staticVariables.h"

@interface VoterStatisticViewController ()

@end

@implementation VoterStatisticViewController

NSString *RecVoter;
NSString *AceptedVoter;
NSString *ExclVoter;
static NSTimer * timer;
static int FinishedAPI;

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    [self getLastUpdate];
    [self getAllStatistics];
    //
    // Do any additional setup after loading the view.
//    self.lblAcepted.text = AceptedVoter;
//    self.lblExcl.text = ExclVoter;
//    self.lblRec.text = RecVoter;
    //
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    
    
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width, self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height);
    }
    //
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    //
    if ((toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(320,  self.viewContent.frame.size.height);
    }
    
}
-(void)checkActivityToStop{
    if (FinishedAPI >=2) {
        [self.activity stopAnimating];
       // self.btnUpdate.enabled=YES;
        //((UIButton * )sender).enabled =YES;
        NSLog(@"stop");
        
    }
}

- (void) updateData:(NSTimer *)timer
{
    FinishedAPI = 0;
    [self.activity startAnimating] ;

    [self getLastUpdate];
    [self getAllStatistics];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
    timer=nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self LoadCachedData];
 //   [self getLastUpdate];
  //  [self getAllStatistics];
    //
    timer=[NSTimer scheduledTimerWithTimeInterval:900.0f
                                           target:self selector:@selector(updateData:) userInfo:nil repeats:YES];
    //
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width, self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)LoadDataWithRec:(NSString *)rec andAcepted:(NSString *)acepted andExcl:(NSString *)excl{
    
    AceptedVoter  =acepted;
    RecVoter =rec;
    ExclVoter  =excl;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)btnAcceptedVotersReport:(id)sender {
    AcceptedVotersReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AcceptedVotersReport"];
    [self presentViewController:ViewController animated:YES completion:nil];

}

- (IBAction)btnExVoter:(id)sender {
    
    DisVotersReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DisVotersReport"];
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (IBAction)btnGoToReport:(id)sender {
    VotersReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VoterReport"];
       [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//
-(void)getLastUpdate{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetLastUpdateTime"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data.length > 0 && connectionError == nil)
        {
            FinishedAPI ++;
            [self checkActivityToStop];

            NSError * err =nil;
            NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                         NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
            NSDictionary * data=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:NSJSONReadingAllowFragments
                                                                  error:&err];
             if (data!= nil) {
            [staticVariables singelton].dataDate = data;
             
            if ([data objectForKey:@"1"] != Nil) {
                self.lbllastUpdate.text=[NSString stringWithFormat:@"أخر تحديث : %@",[data objectForKey:@"1"]];
            }
             }
        }else
        {
            FinishedAPI ++;
            [self checkActivityToStop];

            UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
            [alertConErr show];
        }
    }];
}
//
-(void)getAllStatistics
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetAllStatistics"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data.length > 0 && connectionError == nil)
        {
            FinishedAPI ++;
            [self checkActivityToStop];

            NSError * err =nil;
            NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                         NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
            NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingAllowFragments
                                                           error:&err];
            NSDictionary *resultDic = [ff objectAtIndex:0];
             if (resultDic!= nil) {
            [staticVariables singelton].AllStaticisticDic = resultDic;
           
            self.lblRec.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersCount"]];
            self.lblExcl.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"DisregardedVotersCount"]];
            self.lblAcepted.text =[NSString stringWithFormat:@"%d" ,([[resultDic objectForKey:@"VotersCount"] intValue]-[[resultDic objectForKey:@"DisregardedVotersCount"] intValue])];
            
            self.lblNewVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"NewVotersCount"]];
            self.lblPreviousVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"PreviousVotersCount"]];
            self.lblMaleNewVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"MaleNewVotersCount"]];
            self.lblFemaleNewVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"FemaleNewVotersCount"]];
             }

        }else
        {
            FinishedAPI ++;
            [self checkActivityToStop];

            UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
            [alertConErr show];
        }
    }];
}
//
-(void)LoadCachedData{
    NSDictionary *resultDic =  [staticVariables singelton].AllStaticisticDic;
     if (resultDic!= nil) {
    self.lblRec.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersCount"]];
    self.lblExcl.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"DisregardedVotersCount"]];
    self.lblAcepted.text =[NSString stringWithFormat:@"%d" ,([[resultDic objectForKey:@"VotersCount"] intValue]-[[resultDic objectForKey:@"DisregardedVotersCount"] intValue])];
    
    self.lblNewVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"NewVotersCount"]];
    self.lblPreviousVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"PreviousVotersCount"]];
    self.lblMaleNewVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"MaleNewVotersCount"]];
    self.lblFemaleNewVotersCount.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"FemaleNewVotersCount"]];
     }
    //
    NSDictionary *data =  [staticVariables singelton].dataDate ;

      if (data!= nil) {
    if ([data objectForKey:@"1"] != Nil) {
        self.lbllastUpdate.text=[NSString stringWithFormat:@"أخر تحديث : %@",[data objectForKey:@"1"]];
    }
      }
}

@end
