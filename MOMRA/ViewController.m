//
//  ViewController.m
//  MOMRA
//
//  Created by aya on 6/14/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "ViewController.h"
#import "MOMRA-Swift.h"
@interface ViewController ()<ChartViewDelegate>

@property (weak, nonatomic) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet PieChartView *pieChart;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    float sizeOfContent = 0;
//    UIView *lLast = self.pieChart ;
//    NSInteger wd = lLast.frame.origin.y;
//    NSInteger ht = lLast.frame.size.height;
    
//    sizeOfContent = wd+ht;
    
//    self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, sizeOfContent);
    //
    _chartView.delegate = self;
    
    _chartView.descriptionText = @"";
    _chartView.noDataTextDescription = @"You need to provide data for the chart.";
    
    _chartView.pinchZoomEnabled = NO;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.drawGridBackgroundEnabled = NO;
    //_chartView.drawValueAboveBarEnabled=NO;
    
    ChartLegend *legend = _chartView.legend;
    //legend.wordWrapEnabled=YES;
    //legend.position = ChartLegendPositionRightOfChartInside;
    legend.position = ChartLegendPositionBelowChartLeft;
    legend.wordWrapEnabled=true;
    legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
//    [legend setNeededHeight:100.0];
//    [legend setNeededWidth:100.0];
//    [legend setMaxSizePercent:20.0];
    //legend.yOffset=50.0;
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:8.f];
    //xAxis.wordWrapEnabled=true;
    //xAxis.labelPosition=XAxisLabelPositionTop;
    //xAxis.xOffset=0.0;
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
    leftAxis.valueFormatter.maximumFractionDigits = 1;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.spaceTop = 0.25;
    
    _chartView.rightAxis.enabled = NO;
    _chartView.valueFormatter = [[NSNumberFormatter alloc] init];
    _chartView.valueFormatter.maximumFractionDigits = 1;
    [self setDataCount:(5.0 + 1) range:100.0];
//    _sliderX.value = 9.0;
//    _sliderY.value = 100.0;
//    [self slidersValueChanged:nil];
}
- (void)setDataCount:(int)count range:(double)range
{
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < count; i++)
    {
//        [xVals addObject:[@(i + 1990) stringValue]];
        [xVals addObject:[NSString stringWithFormat:@"١١-١٤",i]];
    }
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
    
    double mult = range * 1000.f;
    
    for (int i = 0; i < count; i++)
    {
        //double val = (double) (arc4random_uniform(mult) + 3.0);
        //[yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val xIndex:i]];
        double val1 = (double) (arc4random_uniform(mult) + mult / 2);
        double val2 = (double) (arc4random_uniform(mult) + mult / 2);
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val1), @(val2)] xIndex:i]];
        
//        double val = (double) (arc4random_uniform(mult) + 3.0);
//        [yVals2 addObject:[[BarChartDataEntry alloc] initWithValue:val xIndex:i]];
//        
//        val = (double) (arc4random_uniform(mult) + 3.0);
//        [yVals3 addObject:[[BarChartDataEntry alloc] initWithValue:val xIndex:i]];
         val1 = (double) (arc4random_uniform(mult) + mult / 2);
         val2 = (double) (arc4random_uniform(mult) + mult / 2);
        [yVals2 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val1), @(val2)] xIndex:i]];
        
         val1 = (double) (arc4random_uniform(mult) + mult / 2);
         val2 = (double) (arc4random_uniform(mult) + mult / 2);
        [yVals3 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val1), @(val2)] xIndex:i]];
    }
    
//    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@"Company A Company A Company A Company A"];
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@""];
     set1.stackLabels = @[@"ذكور ذكور ذكور", @"إناث إناث إناث"];
    set1.colors = @[ChartColorTemplates.vordiplom[2], ChartColorTemplates.joyful[1]];
    set1.drawValuesEnabled=NO;
    //[set1 setColor:[UIColor colorWithRed:104/255.f green:241/255.f blue:175/255.f alpha:1.f]];
    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:@""];
    //[set2 setColor:[UIColor colorWithRed:164/255.f green:228/255.f blue:251/255.f alpha:1.f]];
    set2.stackLabels = @[@"ذكور ذكور ذكور", @"إناث إناث إناث"];
    set2.colors = @[ChartColorTemplates.vordiplom[3], ChartColorTemplates.joyful[4]];
    set2.drawValuesEnabled=NO;
    BarChartDataSet *set3 = [[BarChartDataSet alloc] initWithYVals:yVals3 label:@""];
    //[set3 setColor:[UIColor colorWithRed:242/255.f green:247/255.f blue:158/255.f alpha:1.f]];
    set3.stackLabels = @[@"ذكور ذكور ذكور", @"إناث إناث إناث"];
    set3.colors = @[ChartColorTemplates.vordiplom[4], ChartColorTemplates.joyful[0]];
    set3.drawValuesEnabled=NO;
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    [dataSets addObject:set2];
    [dataSets addObject:set3];
    
    BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
    data.groupSpace = 0.8;
    //[data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
    
    _chartView.data = data;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
