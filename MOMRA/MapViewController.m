//
//  MapViewController.m
//  MOMRA
//
//  Created by aya on 6/14/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "MapViewController.h"
#import "MapLocationView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "StatisticsCenter.h"
#import "StatisticsDivision.h"
#import "StatisticsMohafaza.h"
#import "IntensityMapViewController.h"

@interface MapViewController ()
@property (nonatomic ,strong ) NSMutableArray * regions;
@property (nonatomic ,strong ) NSMutableArray * centers;
@property (nonatomic ,strong ) NSMutableArray * divisions;
@end

@implementation MapViewController
static KMLParser *kmlParser;
static KMLParser *kmlParserCover;

static MKAnnotationView *openedAnnotationView;
static bool isOverlayAdded;
static bool isCenterAdded;
static bool isDivisionAdded;
static NSArray *overlays ;
static NSArray *KMLAnnonations ;
static NSMutableArray * annotationsCenter;
static NSMutableArray * annotationsDivision;
static NSMutableArray * annotationsRegion;
static int FinishedAPI;




- (void)viewDidLoad {
    [super viewDidLoad];
    //
    //set the user type for all screens
    [staticVariables singelton].UserTypeID=@"0";
    
    //
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
    self.mapView.delegate = self;
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = 20;
    zoomIn.span.latitudeDelta  = 20;
    [self.mapView setRegion:zoomIn animated:NO];
    //
    //
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:singleTap];
    openedAnnotationView = nil;
    //
    //    /http://services.intekhab.gov.sa/GeoDashWebService/amanat.kml
    //http://services.intekhab.gov.sa/GeoDashWebService/mohafazat.kml
    // Locate the path to the route.kml file in the application's bundle
    // and parse it with the KMLParser.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AllDevisions" ofType:@"kml"];
    //  NSString *path = @"http://services.intekhab.gov.sa/GeoDashWebService/amanat.kml";
    NSURL *url = [NSURL fileURLWithPath:path];
    kmlParser = [[KMLParser alloc] initWithURL:url];
    [kmlParser parseKML];
    //
NSURL *urlCover = [NSURL URLWithString:@"http://services.intekhab.gov.sa/GeoDashWebService/covers.kml"];
    kmlParserCover = [[KMLParser alloc] initWithURL:urlCover];
    [kmlParserCover parseKML];
    [self.mapView addOverlays:[kmlParserCover overlays]];
    [self.mapView addAnnotations:[kmlParserCover points]];
    //
    //
       //
    // Add all of the MKOverlay objects parsed from the KML file to the map.
    overlays = [kmlParser overlays];
    
    // Add all of the MKAnnotation objects parsed from the KML file to the map.
    KMLAnnonations = [kmlParser points];
    
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    [self httpPostRequest];
    [self httpPostRequestCenter];
    [self httpPostRequestDivision];
    
    
}


- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    if (!openedAnnotationView)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:openedAnnotationView];
    DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
    
    BOOL isCallout = (CGRectContainsPoint(DXview.calloutView.frame, touchPoint));
    BOOL isPin = (CGRectContainsPoint(DXview.pinView.frame, touchPoint));
    
    
    if (isCallout|| isPin) {
        return;
    }
    if ([openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
        
    }
}
//
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    
    
    MapLocationView *selectedAnn = nil;
    if ([mapView selectedAnnotations ] && [mapView selectedAnnotations ].count >0 ) {
        selectedAnn =  [mapView selectedAnnotations ][0];
    }
    //
    if (mapView.region.span.latitudeDelta <= [[staticVariables singelton].CenterZoomLevel floatValue] && isCenterAdded == NO) {
        [mapView addAnnotations:annotationsCenter];
        isCenterAdded = YES;
    } else if(mapView.region.span.latitudeDelta > [[staticVariables singelton].CenterZoomLevel floatValue] && isCenterAdded == YES){
        if (selectedAnn.typeID == 1 ||selectedAnn.typeID == 2 ) { // Close the Callout
            DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
            [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
            DXview.calloutView = nil;
            DXview.layer.zPosition = -1;
            openedAnnotationView = nil;
            [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
        }
        
        [mapView removeAnnotations:annotationsCenter];
        isCenterAdded = NO;
    }
    //
    if (mapView.region.span.latitudeDelta <= [[staticVariables singelton].DivistionZoomLevel floatValue] && isDivisionAdded == NO) {
        [mapView addAnnotations:annotationsDivision];
        isDivisionAdded = YES;
    } else if (mapView.region.span.latitudeDelta > [[staticVariables singelton].DivistionZoomLevel floatValue] && isDivisionAdded == YES){
        if (selectedAnn.typeID == 4) { // Close the Callout
            DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
            [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
            DXview.calloutView = nil;
            DXview.layer.zPosition = -1;
            openedAnnotationView = nil;
            [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
        }
        
        [mapView removeAnnotations:annotationsDivision];
        isDivisionAdded = NO;
    }
    //
    if (mapView.region.span.latitudeDelta <= [[staticVariables singelton].DivistionZoomLevel floatValue] && isOverlayAdded == NO) {
        [mapView addOverlays:overlays];
        [mapView addAnnotations:KMLAnnonations];
        isOverlayAdded = YES;
    } else  if (mapView.region.span.latitudeDelta > [[staticVariables singelton].DivistionZoomLevel floatValue] && isOverlayAdded == YES){
        [mapView removeOverlays:overlays];
        [mapView removeAnnotations:KMLAnnonations];
        isOverlayAdded = NO;
        
    }
    //
}
//
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKOverlayView * view = [[MKOverlayView alloc]init];
    if ([kmlParserCover viewForOverlay:overlay]) {
        view =[kmlParserCover viewForOverlay:overlay];
    }
    else
    {
        view =[kmlParser viewForOverlay:overlay];
    }
    return view;
    
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MapLocationView class]])
    {
        MapLocationView *ann = (MapLocationView *) annotation;
        
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"MapLocationView%d", ann.typeID]];
        if (!annotationView) {
            
            UIView *pinView ;
            
            switch (ann.typeID) {
                case 1: //Center male
                {
                    if ([ann.isCanidiateCenter isEqualToNumber:@1]) {
                        
                        if ([ann.MAX_VOTER_SERIAL intValue]>2900) {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CandidatesMenWarning.PNG"]];
                        }
                        else
                        {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menCand.png"]];
                        }
                        
                    }
                    else
                    {
                        if ([ann.MAX_VOTER_SERIAL intValue]>2900) {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"VotersMenWarning.png"]];
                        }
                        else
                        {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"men.png"]];
                            
                        }
                    }
                    
                }
                    break;
                case 2: // center female
                {
                    if ([ann.isCanidiateCenter isEqualToNumber:@1]) {
                        if ([ann.MAX_VOTER_SERIAL intValue]>2900) {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CandidatesWomenWarning.PNG"]];
                        }
                        else
                        {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"womenCand.png"]];
                        }
                    }
                    else
                    {
                        if ([ann.MAX_VOTER_SERIAL intValue]>2900) {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"VotersWomenWarning.png"]];
                        }
                        else
                        {
                            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"women.png"]];
                        }
                    }
                    
                }
                    break;
                case 3:// Region
                {
                    pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GreenPin.png"]];
                }
                    break;
                case 4:// Division
                {
                    pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DivisionPin.png"]];
                }
                    break;
                default: // other type
                    pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GreenPin.png"]];
                    break;
            }
            
            
            DXAnnotationSettings *newSettings = [DXAnnotationSettings defaultSettings];
            newSettings.animationType = DXCalloutAnimationNone ;
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:nil
                                                                 settings:newSettings];
            //
            switch (ann.typeID) {
                case 1: //Center male
                case 2: // center female
                case 4: //Division
                {
                    //    annotationView.hidden = true;
                    //    annotationView.enabled = false;
                }
                    break;
                case 3:// Region
                {
                    //   annotationView.hidden = false;
                    //   annotationView.enabled = true;
                }
                    break;
            }
            
            
        }
        return annotationView;
    }
    else
    {
        
            return [kmlParser viewForAnnotationWithTitle:annotation forMapView:mapView];
        
    }
    
}
//
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
        if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
            return;
        }
    
    DXAnnotationView * DXview = (DXAnnotationView *)view;
    if (openedAnnotationView &&openedAnnotationView == view ) {
        DXview.layer.zPosition = 1;
        [self.mapView selectAnnotation:[view annotation] animated:NO];
    }
    else
    {
        DXview.layer.zPosition = -2;
    }
}
//

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (openedAnnotationView && [openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        
        
        DXAnnotationView * newView = (DXAnnotationView *)openedAnnotationView;
        if (newView) {
            UIView *  viewStatistics =  newView.calloutView;
            id<MKAnnotation> annotation = [newView annotation];
            //
            MKMapRect r = [self.mapView visibleMapRect];
            MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
            CGRect BarChartFrame = viewStatistics.frame;
            r.origin.x = pt.x - r.size.width * 0.5;
            r.origin.y = pt.y ;
            BarChartFrame.origin.y =30;
            viewStatistics.frame =BarChartFrame;
            [self.mapView setVisibleMapRect:r animated:YES];
            
        }
    }
    
}
//
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    
    if (![[view annotation] isKindOfClass:[MapLocationView class]])
        return;
        
        
        if (openedAnnotationView != nil&&openedAnnotationView != view) {
            view.layer.zPosition = -2;
            return;
        }else if(openedAnnotationView != nil&&openedAnnotationView == view)
        {
            return;
        }else
        {
            openedAnnotationView = view;
        }
        
        
        
        id annotation = [view annotation];
        
        
        UIView *calloutView ;//= [[[NSBundle mainBundle] loadNibNamed:@"Statistics" owner:self options:nil] firstObject];
        
        NSInteger index=  [[annotation name]integerValue];
        
        
        if ([view isKindOfClass:[DXAnnotationView class]]) {
            [((DXAnnotationView *)view)showCalloutView];
            
            view.layer.zPosition = 0;
        }
        //
        
        
        Region * reg;// =[self.regions objectAtIndex:index];
        
        MapLocationView *ann = (MapLocationView *) annotation;
        switch (ann.typeID) {
            case 1: //Center male
            case 2:
            {
                calloutView = [[[NSBundle mainBundle] loadNibNamed:@"StatisticsCenter" owner:self options:nil] firstObject];
                DXAnnotationView * DXview = (DXAnnotationView *)view;
                [DXview addNewCallOutView:calloutView];
                view = (MKAnnotationView *)DXview;
                reg =[self.centers objectAtIndex:index];
                //
                DXAnnotationView * newView = (DXAnnotationView * )view;
                
                StatisticsCenter *viewStatistics =  (StatisticsCenter *)newView.calloutView;
                NSString *string = reg.RegionName;
                
                if (ann.typeID == 1) {
                    string =   [string stringByAppendingString:@" - رجال"];
                }else if(ann.typeID == 2)
                {
                    string =   [string stringByAppendingString:@" - نساء"];
                }
                
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,string.length)];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:17.0] range:NSMakeRange(0,string.length)];
                }
                [attributedString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,string.length)];
                viewStatistics.RegionName.attributedText=attributedString;
                
                
                //
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
                [formatter setGroupingSeparator:groupingSeparator];
                [formatter setGroupingSize:3];
                [formatter setAlwaysShowsDecimalSeparator:NO];
                [formatter setUsesGroupingSeparator:YES];
                //
                [viewStatistics.VotersCount setText:[formatter stringFromNumber:reg.VotersCount]];
                [viewStatistics.DisregardedVotersCount setText:[formatter stringFromNumber:reg.DisregardedVotersCount]];
                [viewStatistics.VotersVotesCount setText:[formatter stringFromNumber:reg.VotersVotesCount]];
                [viewStatistics.AcceptedVotersCount setText:[formatter stringFromNumber:@([reg.VotersCount  doubleValue]- [reg.DisregardedVotersCount  doubleValue])]];
                //
                [viewStatistics.RecNewCount setText:[formatter stringFromNumber:reg.RecNewCount]];
                [viewStatistics.lastCount setText:[formatter stringFromNumber:reg.lastCount]];
                //
                //
                [mapView setCenterCoordinate:[annotation coordinate] animated:YES];
        MKMapRect r = [mapView visibleMapRect];
                MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
                CGRect BarChartFrame = viewStatistics.frame;
                r.origin.x = pt.x - r.size.width * 0.5;
                r.origin.y = pt.y ;
                BarChartFrame.origin.y =30;
                viewStatistics.frame =BarChartFrame;
                [mapView setVisibleMapRect:r animated:YES];
            }
                break;
            case 3:// Region
            {
                calloutView = [[[NSBundle mainBundle] loadNibNamed:@"Statistics" owner:self options:nil] firstObject];
                DXAnnotationView * DXview = (DXAnnotationView *)view;
                [DXview addNewCallOutView:calloutView];
                view = (MKAnnotationView *)DXview;
                reg =[self.regions objectAtIndex:index];
                DXAnnotationView * newView = (DXAnnotationView * )view;
                
                Statistics *viewStatistics =  (Statistics *)newView.calloutView;
                NSString *string = reg.RegionName;
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,string.length)];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:17.0] range:NSMakeRange(0,string.length)];
                }
                [attributedString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,string.length)];
                viewStatistics.RegionName.attributedText=attributedString;
                
                
                //
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
                [formatter setGroupingSeparator:groupingSeparator];
                [formatter setGroupingSize:3];
                [formatter setAlwaysShowsDecimalSeparator:NO];
                [formatter setUsesGroupingSeparator:YES];
                //
                [viewStatistics.VotersCount setText:[formatter stringFromNumber:reg.VotersCount]];
                [viewStatistics.AcceptedVotersCount setText:[formatter stringFromNumber:@([reg.VotersCount  doubleValue]- [reg.DisregardedVotersCount  doubleValue])]];
                [viewStatistics.AcceptedCandidatesCount setText:[formatter stringFromNumber:@([reg.CandidatesCount  doubleValue]- [reg.DisregardedCandidatesCount  doubleValue])]];
                [viewStatistics.CandidatesCount setText:[formatter stringFromNumber:reg.CandidatesCount]];;
                [viewStatistics.DisregardedCandidatesCount setText:[formatter stringFromNumber:reg.DisregardedCandidatesCount]];
                [viewStatistics.DisregardedVotersCount setText:[formatter stringFromNumber:reg.DisregardedVotersCount]];
                [viewStatistics.VotersCentersCount setText:[formatter stringFromNumber:reg.VotersCentersCount]];
                [viewStatistics.CandidatesCentersCount setText:[formatter stringFromNumber:reg.CandidatesCentersCount]];
                [viewStatistics.AdminUsersCountCount setText:[formatter stringFromNumber:reg.AdminUsersCountCount]];
                [viewStatistics.VotersVotesCount setText:[formatter stringFromNumber:reg.VotersVotesCount]];
                [viewStatistics.WinnersCount setText:[formatter stringFromNumber:reg.WinnersCount]];
                //
                [viewStatistics.CandFemale setText:[formatter stringFromNumber:reg.CandFemale]];
                [viewStatistics.CandMale setText:[formatter stringFromNumber:reg.CandMale]];
                [viewStatistics.RecNewFemaleCount setText:[formatter stringFromNumber:reg.RecNewFemaleCount]];
                [viewStatistics.RecNewMaleCount setText:[formatter stringFromNumber:reg.RecNewMaleCount]];
                [viewStatistics.RecNewCount setText:[formatter stringFromNumber:reg.RecNewCount]];
                [viewStatistics.lastCount setText:[formatter stringFromNumber:reg.lastCount]];
                [viewStatistics.votersYoung setText:[formatter stringFromNumber:reg.VotersYoung]];
                //
                [mapView setCenterCoordinate:[annotation coordinate] animated:YES];
        MKMapRect r = [mapView visibleMapRect];
                MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
                CGRect BarChartFrame = viewStatistics.frame;
                r.origin.x = pt.x - r.size.width * 0.5;
                r.origin.y = pt.y ;
                BarChartFrame.origin.y =30;
                viewStatistics.frame =BarChartFrame;
                [mapView setVisibleMapRect:r animated:YES];
            }
                break;
            case 4:// Division
            {
                calloutView = [[[NSBundle mainBundle] loadNibNamed:@"StatisticsDivision" owner:self options:nil] firstObject];
                DXAnnotationView * DXview = (DXAnnotationView *)view;
                [DXview addNewCallOutView:calloutView];
                view = (MKAnnotationView *)DXview;
                reg =[self.divisions objectAtIndex:index];
                DXAnnotationView * newView = (DXAnnotationView * )view;
                
                StatisticsDivision *viewStatistics =  (StatisticsDivision *)newView.calloutView;
                NSString *string = reg.RegionName;
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
                [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,string.length)];
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
                    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:17.0] range:NSMakeRange(0,string.length)];
                }
                [attributedString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,string.length)];
                viewStatistics.RegionName.attributedText=attributedString;
                
                
                //
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
                [formatter setGroupingSeparator:groupingSeparator];
                [formatter setGroupingSize:3];
                [formatter setAlwaysShowsDecimalSeparator:NO];
                [formatter setUsesGroupingSeparator:YES];
                //
                [viewStatistics.VotersCount setText:[formatter stringFromNumber:reg.VotersCount]];
                [viewStatistics.AcceptedVotersCount setText:[formatter stringFromNumber:@([reg.VotersCount  doubleValue]- [reg.DisregardedVotersCount  doubleValue])]];
                [viewStatistics.AcceptedCandidatesCount setText:[formatter stringFromNumber:@([reg.CandidatesCount  doubleValue]- [reg.DisregardedCandidatesCount  doubleValue])]];
                [viewStatistics.CandidatesCount setText:[formatter stringFromNumber:reg.CandidatesCount]];;
                [viewStatistics.DisregardedCandidatesCount setText:[formatter stringFromNumber:reg.DisregardedCandidatesCount]];
                [viewStatistics.DisregardedVotersCount setText:[formatter stringFromNumber:reg.DisregardedVotersCount]];
                [viewStatistics.VotersCentersCount setText:[formatter stringFromNumber:reg.VotersCentersCount]];
                [viewStatistics.CandidatesCentersCount setText:[formatter stringFromNumber:reg.CandidatesCentersCount]];
                [viewStatistics.VotersVotesCount setText:[formatter stringFromNumber:reg.VotersVotesCount]];
                [viewStatistics.WinnersCount setText:[formatter stringFromNumber:reg.WinnersCount]];
                //
                [viewStatistics.CandFemale setText:[formatter stringFromNumber:reg.CandFemale]];
                [viewStatistics.CandMale setText:[formatter stringFromNumber:reg.CandMale]];
                [viewStatistics.RecNewFemaleCount setText:[formatter stringFromNumber:reg.RecNewFemaleCount]];
                [viewStatistics.RecNewMaleCount setText:[formatter stringFromNumber:reg.RecNewMaleCount]];
                [viewStatistics.RecNewCount setText:[formatter stringFromNumber:reg.RecNewCount]];
                [viewStatistics.lastCount setText:[formatter stringFromNumber:reg.lastCount]];
                //
                [mapView setCenterCoordinate:[annotation coordinate] animated:YES];
        MKMapRect r = [mapView visibleMapRect];
                MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
                CGRect BarChartFrame = viewStatistics.frame;
                r.origin.x = pt.x - r.size.width * 0.5;
                r.origin.y = pt.y ;
                BarChartFrame.origin.y =30;
                viewStatistics.frame =BarChartFrame;
                [mapView setVisibleMapRect:r animated:YES];
            }
                break;
                
        }
        //
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)checkActivityToStop{
    if (FinishedAPI >=3) {
        [self.activity stopAnimating];
        self.btnReload.hidden = NO;
    }
}

-(void)httpPostRequest
{
    @try {
        NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
        NSError *error = nil;
        NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
        NSURL *url = [Helper getWebserviceURL:@"GetAllRegions"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data.length > 0 && connectionError == nil)
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                
                NSError * err =nil;
                NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:NSJSONReadingAllowFragments
                                                                                error:&err];
                NSString * results =[receivedData objectForKey:@"d"] ;
                if (results == nil || [results isEqualToString:@""]) {
                    UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                    [alertConErr show];
                    return ;
                }
                NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                             options:NSJSONReadingAllowFragments
                                                               error:&err];
                for (int i=0 ;i < ff.count; i++) {
                    NSDictionary * resultsItems =[ff objectAtIndex:i];
                    Region * reg=[[Region alloc]init];
                    reg.RegionName=[resultsItems objectForKey:@"Name"];
                    reg.Lat=[resultsItems objectForKey:@"lat"];
                    reg.Long=[resultsItems objectForKey:@"long"];
                    reg.VotersCount=[resultsItems objectForKey:@"VotersCount"];
                    reg.CandidatesCount=[resultsItems objectForKey:@"CandidatesCount"];
                    reg.DisregardedVotersCount=[resultsItems objectForKey:@"DisregardedVotersCount"];
                    reg.DisregardedCandidatesCount=[resultsItems objectForKey:@"DisregardedCandidatesCount"];
                    reg.VotersCentersCount=[resultsItems objectForKey:@"VotersCentersCount"];
                    reg.CandidatesCentersCount=[resultsItems objectForKey:@"CandidatesCentersCount"];
                    reg.AdminUsersCountCount=[resultsItems objectForKey:@"AdminUsersCountCount"];
                    reg.VotersVotesCount=[resultsItems objectForKey:@"VotersVotesCount"];
                    reg.WinnersCount=[resultsItems objectForKey:@"WinnersCount"];
                    //
                    reg.CandFemale=[resultsItems objectForKey:@"FemaleCandidatesCount"];
                    reg.CandMale=[resultsItems objectForKey:@"MaleCandidatesCount"];
                    reg.RecNewFemaleCount=[resultsItems objectForKey:@"FemaleNewVotersCount"];
                    reg.RecNewMaleCount=[resultsItems objectForKey:@"MaleNewVotersCount"];
                    reg.RecNewCount=[resultsItems objectForKey:@"NewVotersCount"];
                    reg.lastCount=[resultsItems objectForKey:@"PreviousVotersCount"];
                    reg.ID=[resultsItems objectForKey:@"ID"];
                    //
                    reg.TypeID=3;
                    
                    [self.regions addObject:reg];
                    //
                }
                [self addMarkers];
            }else
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                [alertConErr show];
            }
        }];
    }
    @catch (NSException *exception) {
        FinishedAPI ++;
        [self checkActivityToStop];
        NSLog(@"%@",exception.description);
    }
}
//
-(void)httpPostRequestCenter
{
    @try {
        NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
        NSError *error = nil;
        NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
        NSURL *url = [Helper getWebserviceURL:@"GetAllStatisticsByCenters"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data.length > 0 && connectionError == nil)
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                NSError * err =nil;
                NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:NSJSONReadingAllowFragments
                                                                                error:&err];
                NSString * results =[receivedData objectForKey:@"d"] ;
                if (results == nil || [results isEqualToString:@""]) {
                    UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                    [alertConErr show];
                    return ;
                }
                NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                             options:NSJSONReadingAllowFragments
                                                               error:&err];
                for (int i=0 ;i < ff.count; i++) {
                    NSDictionary * resultsItems =[ff objectAtIndex:i];
                    Region * reg=[[Region alloc]init];
                    reg.RegionName=[resultsItems objectForKey:@"Name"];
                    reg.Lat=[resultsItems objectForKey:@"lat"];
                    reg.Long=[resultsItems objectForKey:@"long"];
                    reg.VotersCount=[resultsItems objectForKey:@"VotersCount"];
                    reg.DisregardedVotersCount=[resultsItems objectForKey:@"DisregardedVotersCount"];
                    reg.VotersVotesCount=[resultsItems objectForKey:@"VotersVotesCount"];
                    reg.RecNewCount=[resultsItems objectForKey:@"NewVotersCount"];
                    reg.lastCount=[resultsItems objectForKey:@"PreviousVotersCount"];
                    reg.MAX_VOTER_SERIAL=[resultsItems objectForKey:@"MAX_VOTER_SERIAL"];
                    reg.TypeID=[[resultsItems objectForKey:@"CenterType"] intValue];
                    reg.isCanidiateCenter=  [NSNumber numberWithInt:[[resultsItems objectForKey:@"CanidiateCenter"] intValue]];
                    [self.centers addObject:reg];
                }
                [self addMarkersCenter];
            }else
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                [alertConErr show];
            }
        }];
    }
    @catch (NSException *exception) {
        FinishedAPI ++;
        [self checkActivityToStop];
    }
}
//
//
-(void)httpPostRequestDivision
{
    @try {
        NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
        NSError *error = nil;
        NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
        NSURL *url = [Helper getWebserviceURL:@"GetAllStatisticsByDivision"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data.length > 0 && connectionError == nil)
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                NSError * err =nil;
                NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:NSJSONReadingAllowFragments
                                                                                error:&err];
                NSString * results =[receivedData objectForKey:@"d"] ;
                if (results == nil || [results isEqualToString:@""]) {
                    UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                    [alertConErr show];
                    return ;
                }
                NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                             options:NSJSONReadingAllowFragments
                                                               error:&err];
                for (int i=0 ;i < ff.count; i++) {
                    NSDictionary * resultsItems =[ff objectAtIndex:i];
                    Region * reg=[[Region alloc]init];
                    reg.RegionName=[resultsItems objectForKey:@"Name"];
                    reg.Lat=[resultsItems objectForKey:@"lat"];
                    reg.Long=[resultsItems objectForKey:@"long"];
                    reg.VotersCount=[resultsItems objectForKey:@"VotersCount"];
                    reg.CandidatesCount=[resultsItems objectForKey:@"CandidatesCount"];
                    reg.DisregardedVotersCount=[resultsItems objectForKey:@"DisregardedVotersCount"];
                    reg.DisregardedCandidatesCount=[resultsItems objectForKey:@"DisregardedCandidatesCount"];
                    reg.VotersCentersCount=[resultsItems objectForKey:@"VotersCentersCount"];
                    reg.CandidatesCentersCount=[resultsItems objectForKey:@"CandidatesCentersCount"];
                    reg.VotersVotesCount=[resultsItems objectForKey:@"VotersVotesCount"];
                    reg.WinnersCount=[resultsItems objectForKey:@"WinnersCount"];
                    //
                    reg.CandFemale=[resultsItems objectForKey:@"FemaleCandidatesCount"];
                    reg.CandMale=[resultsItems objectForKey:@"MaleCandidatesCount"];
                    reg.RecNewFemaleCount=[resultsItems objectForKey:@"FemaleNewVotersCount"];
                    reg.RecNewMaleCount=[resultsItems objectForKey:@"MaleNewVotersCount"];
                    reg.RecNewCount=[resultsItems objectForKey:@"NewVotersCount"];
                    reg.lastCount=[resultsItems objectForKey:@"PreviousVotersCount"];
                    //
                    reg.TypeID=4;
                    [self.divisions addObject:reg];
                }
                [self addMarkersDivision];
            }else
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                [alertConErr show];
            }
        }];
    }
    @catch (NSException *exception) {
        FinishedAPI ++;
        [self checkActivityToStop];
    }
}
//
//
-(void)addMarkers
{
    annotationsRegion = [[NSMutableArray alloc]init];
    for (int i=0; i<self.regions.count; i++) {
        //  GMSMarker *marker = [[GMSMarker alloc] init];
        Region * reg =[self.regions objectAtIndex:i];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [[NSString stringWithFormat:@"%@",reg.Lat]doubleValue];
        coordinate.longitude = [[NSString stringWithFormat:@"%@",reg.Long]doubleValue];
        MapLocationView *annotation = [[MapLocationView alloc] initWithName:[NSString stringWithFormat:@"%d",i] address:reg.RegionName  coordinate:coordinate] ;
        annotation.typeID = reg.TypeID ;
        [_mapView addAnnotation:annotation];
        [annotationsRegion addObject:annotation];
    }
    //
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in [_mapView annotations]) {
        if (![annotation isKindOfClass:[MapLocationView class]]) {
            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
            if (MKMapRectIsNull(flyTo)) {
                flyTo = pointRect;
            } else {
                flyTo = MKMapRectUnion(flyTo, pointRect);
            }
        }
    }
    
    // Position the map so that all overlays and annotations are visible on screen.
    //_mapView.visibleMapRect = flyTo;
    
}
//
-(void)addMarkersCenter
{
    annotationsCenter = [[NSMutableArray alloc]init];
    for (int i=0; i<self.centers.count; i++) {
        Region * reg =[self.centers objectAtIndex:i];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [[NSString stringWithFormat:@"%@",reg.Lat]doubleValue];
        coordinate.longitude = [[NSString stringWithFormat:@"%@",reg.Long]doubleValue];
        MapLocationView *annotation = [[MapLocationView alloc] initWithName:[NSString stringWithFormat:@"%d",i] address:reg.RegionName  coordinate:coordinate] ;
        annotation.typeID = reg.TypeID ;
        annotation.isCanidiateCenter = reg.isCanidiateCenter;
        annotation.MAX_VOTER_SERIAL = reg.MAX_VOTER_SERIAL;
        [annotationsCenter addObject:annotation];
        
        
    }
    
    
}
//
-(void)addMarkersDivision
{
    annotationsDivision = [[NSMutableArray alloc]init];
    for (int i=0; i<self.divisions.count; i++) {
        Region * reg =[self.divisions objectAtIndex:i];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [[NSString stringWithFormat:@"%@",reg.Lat]doubleValue];
        coordinate.longitude = [[NSString stringWithFormat:@"%@",reg.Long]doubleValue];
        MapLocationView *annotation = [[MapLocationView alloc] initWithName:[NSString stringWithFormat:@"%d",i] address:reg.RegionName  coordinate:coordinate] ;
        annotation.typeID = reg.TypeID ;
        [annotationsDivision addObject:annotation];
        
    }
    
    
}

#pragma mark - lazy init

-(NSMutableArray *)regions
{
    if (!_regions) {
        _regions=[[NSMutableArray alloc]init];
    }
    return _regions;
}
//
-(NSMutableArray *)centers
{
    if (!_centers) {
        _centers=[[NSMutableArray alloc]init];
    }
    return _centers;
}
//
-(NSMutableArray *)divisions
{
    if (!_divisions) {
        _divisions=[[NSMutableArray alloc]init];
    }
    return _divisions;
}
//
- (IBAction)goToStatistics:(id)sender {
    AllStatisticsViewController *AllStatistics = [self.storyboard instantiateViewControllerWithIdentifier:@"AllStatistics"];
    [self presentViewController:AllStatistics animated:YES completion:nil];
}


- (IBAction)btnMapKeysTapped:(id)sender {
    self.viewMapKeys.hidden = NO;
}
- (IBAction)btnCloseKeysTapped:(id)sender {
    self.viewMapKeys.hidden = YES;
}

- (IBAction)btnReloadTapped:(id)sender {
    
    self.btnReload.hidden = YES;
    
    if (openedAnnotationView) {
        // Close the Callout
        DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
    }
    //
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
    self.mapView.delegate = self;
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = 20;
    zoomIn.span.latitudeDelta  = 20;
    [self.mapView setRegion:zoomIn animated:NO];
    //
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    
    [[self mapView] removeAnnotations:annotationsCenter];
    [[self mapView] removeAnnotations:annotationsDivision];
    [[self mapView] removeAnnotations:annotationsRegion];
    [self.regions removeAllObjects];
    [self.divisions removeAllObjects];
    [self.centers removeAllObjects];
    
    [self httpPostRequest];
    [self httpPostRequestCenter];
    [self httpPostRequestDivision];
}
- (IBAction)btnIntensityTapped:(id)sender {
    IntensityMapViewController *IntensityMap = [self.storyboard instantiateViewControllerWithIdentifier:@"IntensityMap"];
    [self presentViewController:IntensityMap animated:YES completion:nil];
}
@end
