//
//  MapViewController.m
//  MOMRA
//
//  Created by aya on 6/14/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "IntensityMapViewController.h"
#import "MapLocationView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "StatisticsCenter.h"
#import "StatisticsDivision.h"
#import "StatisticsMohafaza.h"

@interface IntensityMapViewController ()
@property (nonatomic ,strong ) NSMutableArray * regions;
@property (nonatomic ,strong ) NSMutableArray * centers;
@property (nonatomic ,strong ) NSMutableArray * divisions;
@property (nonatomic ,strong ) NSMutableArray * Governerates;
@end

@implementation IntensityMapViewController
static KMLParser *kmlParser;
static KMLParser *kmlParserCover;
static KMLParser *kmlParserAmanat;
static KMLParser *kmlParserMohafazat;
static KMLParser *kmlParserMohafazatPoint;
static MKAnnotationView *openedAnnotationView;
static bool isOverlayMohafazatAdded;
static bool isOverlayRegionAdded;
static NSArray *overlays ;
static NSArray *overlaysAmanat ;
static NSArray *overlaysMohafazat ;
static NSArray *overlaysMohafazatPoints ;
static NSArray *KMLAnnonations ;

static int FinishedAPI;
static UIColor * color1 ;
static UIColor * color2 ;
static UIColor * color3 ;
static UIColor * color4 ;
static UIColor * color5 ;
static float maxRegionVoters;
static float maxRegionFemaleVoters;
static float maxRegionYoungVoters;
static float maxMohaafazaVoters;
static float maxMohaafazaFemaleVoters;
static float maxMohaafazaYoungVoters;

- (void)viewDidLoad {
    [super viewDidLoad];
 
    //set the user type for all screens
    [staticVariables singelton].UserTypeID=@"0";
    color1 = [[UIColor alloc]initWithRed:218.0/255.0 green:133.0/255.0 blue:205.0/255.0 alpha:0.8];
    color2 = [[UIColor alloc]initWithRed:198.0/255.0 green:67.0/255.0 blue:248.0/255.0 alpha:0.8];
    color3 = [[UIColor alloc]initWithRed:165.0/255.0 green:10.0/255.0 blue:224.0/255.0 alpha:0.8];
    color4 = [[UIColor alloc]initWithRed:119.0/255.0 green:6.0/255.0 blue:162.0/255.0 alpha:0.8];
    color5 = [[UIColor alloc]initWithRed:72.0/255.0 green:4.0/255.0 blue:98.0/255.0 alpha:0.8];
    
    self.viewColor1.backgroundColor = color1;
    self.viewColor2.backgroundColor = color2;
    self.viewColor3.backgroundColor = color3;
    self.viewColor4.backgroundColor = color4;
    self.viewColor5.backgroundColor = color5;
    //
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
    self.mapView.delegate = self;
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = 20;
    zoomIn.span.latitudeDelta  = 20;
    [self.mapView setRegion:zoomIn animated:NO];
    //
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:singleTap];
    //
    NSURL *urlCover = [NSURL URLWithString:@"http://services.intekhab.gov.sa/GeoDashWebService/covers.kml"];
    kmlParserCover = [[KMLParser alloc] initWithURL:urlCover];
    [kmlParserCover parseKML];
    [self.mapView addOverlays:[kmlParserCover overlays]];
}

-(void)viewDidAppear:(BOOL)animated{
    //
    openedAnnotationView = nil;
    // Locate the path to the route.kml file in the application's bundle
    // and parse it with the KMLParser.
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"AllDevisions" ofType:@"kml"];
//    NSURL *url = [NSURL fileURLWithPath:path];
//    kmlParser = [[KMLParser alloc] initWithURL:url];
//    [kmlParser parseKML];
    //

    //
    NSString *pathAmanat = [[NSBundle mainBundle] pathForResource:@"amanat" ofType:@"kml"];
    NSURL *urlAmanat = [NSURL fileURLWithPath:pathAmanat];
    kmlParserAmanat = [[KMLParser alloc] initWithURL:urlAmanat];
    [kmlParserAmanat parseKML];
    overlaysAmanat =[kmlParserAmanat overlays];
    isOverlayRegionAdded = NO;
    
    //
    NSString *pathMohafazat = [[NSBundle mainBundle] pathForResource:@"mohafazatSplit" ofType:@"kml"];
    NSURL *urlMohafazat = [NSURL fileURLWithPath:pathMohafazat];
    kmlParserMohafazat = [[KMLParser alloc] initWithURL:urlMohafazat];
    [kmlParserMohafazat parseKML];
    overlaysMohafazat =[kmlParserMohafazat overlays];
    isOverlayMohafazatAdded = NO;
    //
    NSString *pathMohafazatPoints = [[NSBundle mainBundle] pathForResource:@"MohafazatPoints" ofType:@"kml"];
    NSURL *urlMohafazatPoints = [NSURL fileURLWithPath:pathMohafazatPoints];
    kmlParserMohafazatPoint = [[KMLParser alloc] initWithURL:urlMohafazatPoints];
    [kmlParserMohafazatPoint parseKML];
    overlaysMohafazatPoints =[kmlParserMohafazatPoint points];
    // Add all of the MKOverlay objects parsed from the KML file to the map.
    overlays = [kmlParser overlays];
    
    // Add all of the MKAnnotation objects parsed from the KML file to the map.
    KMLAnnonations = [kmlParser points];
    
    FinishedAPI = 0;
}
- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    if (!openedAnnotationView)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:openedAnnotationView];
    DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
    
    BOOL isCallout = (CGRectContainsPoint(DXview.calloutView.frame, touchPoint));
    BOOL isPin = (CGRectContainsPoint(DXview.pinView.frame, touchPoint));
    
    
    if (isCallout|| isPin) {
        return;
    }
    if ([openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        //
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];

        
    }
}
//
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
     [self showOverlayForMapView:mapView];
}
//
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKOverlayView * view = [[MKOverlayView alloc]init];
    if ([kmlParserCover viewForOverlay:overlay]) {
        view =[kmlParserCover viewForOverlay:overlay];
    }
    else if ([kmlParserAmanat viewForOverlay:overlay]) {
        view =[kmlParserAmanat viewForOverlay:overlay];
        MKOverlayPathView * pathView = (MKOverlayPathView *)view;
        // [[overlay title] intValue]
        pathView.fillColor = [self getRegionColorForRegionID: [[overlay title] intValue]];
        view = pathView;
    }
    else if ([kmlParserMohafazat viewForOverlay:overlay]) {
        view =[kmlParserMohafazat viewForOverlay:overlay];
        MKOverlayPathView * pathView = (MKOverlayPathView *)view;
       pathView.fillColor = [self getMohafazatColorForMohafazaID: [[overlay title] intValue]];
        pathView.lineWidth = 2.0;
        view = pathView;
    }
    else
    {
        view =[kmlParser viewForOverlay:overlay];
    }
    return view;
    
}
//
//0-20%  :color1
//21-40% :color2
//41-60% :color3
//61-80% :color4
//81-100 :color5
-(UIColor *)getRegionColorForRegionID:(int)regionID{
    
    
    int perc = 0.0;
    for (int i=0; i<self.regions.count; i++) {
        Region * reg =[self.regions objectAtIndex:i];
        if ([reg.ID isEqualToNumber: [NSNumber numberWithInt:regionID]]) {
            
            switch (self.segFilter.selectedSegmentIndex ) {
                case 0:
                {
                       perc = ([reg.VotersYoung intValue]/maxRegionYoungVoters)*100.0 ;
                }
                    break;
                case 1:
                {
                    perc = ([reg.CandFemale intValue]/maxRegionFemaleVoters)*100.0 ;
                }
                    break;

                case 2:
                {
                    perc = ([reg.VotersCount intValue]/maxRegionVoters)*100.0 ;
                }
                    break;

            }
            //
            break;
        }
    }
    UIColor * color = nil;
    if (perc >=0 && perc <=20) {
        color = color1;
    }else if (perc >20 && perc <=40) {
        color = color2;
    }else if (perc >40 && perc <=60) {
        color = color3;
    }else if (perc >60 && perc <=80) {
        color = color4;
    }else if (perc >80 && perc <=100) {
        color = color5;
    }



    return color;
}


-(UIColor *)getMohafazatColorForMohafazaID:(int)MohafazaID{
    float perc = 0.0;
    for (int i=0; i<self.Governerates.count; i++) {
        Region * reg =[self.Governerates objectAtIndex:i];
        if ([reg.ID isEqualToNumber: [NSNumber numberWithInt:MohafazaID]]) {
            switch (self.segFilter.selectedSegmentIndex ) {
                case 0:
                {
                    perc = ([reg.VotersYoung intValue]/maxMohaafazaYoungVoters)*100.0 ;
                }
                    break;
                case 1:
                {
                    perc = ([reg.CandFemale intValue]/maxMohaafazaFemaleVoters)*100.0 ;
                }
                    break;
                    
                case 2:
                {
                    perc = ([reg.VotersCount intValue]/maxMohaafazaVoters)*100.0 ;
                }
                    break;
                    
            }
            //
            break;
        }
    }
    
    UIColor * color = nil;
    if (perc >=0 && perc <=20) {
        color = color1;
    }else if (perc >20 && perc <=40) {
        color = color2;
    }else if (perc >40 && perc <=60) {
        color = color3;
    }else if (perc >60 && perc <=80) {
        color = color4;
    }else if (perc >80 && perc <=100) {
        color = color5;
    }
    else
    {
        color = [UIColor blackColor];
    }
    return color;
    
    
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
   
    
        if ( [kmlParser viewForAnnotation:annotation]) {
            return [kmlParser viewForAnnotationWithTitle:annotation forMapView:mapView];
        }
        else
        {
           
            
            DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"MapLocationView"];
            
            UIView *pinView = [[UIImageView alloc] init];
            //if the region have no valid id dont add the marker view
            if (![[annotation title] isEqualToString:@"-1"]) {
                 pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MohPin.png"]];
            }
            DXAnnotationSettings *newSettings = [DXAnnotationSettings defaultSettings];
            newSettings.animationType = DXCalloutAnimationNone ;
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:nil
                                                                 settings:newSettings];
            return annotationView;
        }

    
}
//
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    //    if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
    //        return;
    //    }
    
    DXAnnotationView * DXview = (DXAnnotationView *)view;
    if (openedAnnotationView &&openedAnnotationView == view ) {
        DXview.layer.zPosition = 1;
        [self.mapView selectAnnotation:[view annotation] animated:NO];
    }
    else
    {
        DXview.layer.zPosition = -2;
    }
}
//

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (openedAnnotationView && [openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        
        
        DXAnnotationView * newView = (DXAnnotationView *)openedAnnotationView;
        if (newView) {
            UIView *  viewStatistics =  newView.calloutView;
            id<MKAnnotation> annotation = [newView annotation];
            //
            MKMapRect r = [self.mapView visibleMapRect];
            MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
            CGRect BarChartFrame = viewStatistics.frame;
            r.origin.x = pt.x - r.size.width * 0.5;
            r.origin.y = pt.y ;
            BarChartFrame.origin.y =30;
            viewStatistics.frame =BarChartFrame;
            [self.mapView setVisibleMapRect:r animated:YES];
            
        }
    }
    
}
//
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    
    if (openedAnnotationView != nil&&openedAnnotationView != view) {
        view.layer.zPosition = -2;
        return;
    }else if(openedAnnotationView != nil&&openedAnnotationView == view)
    {
        return;
    }else
    {
        openedAnnotationView = view;
    }
    
    id annotation = [view annotation];
    UIView *infoWindow = [[UIView alloc]init];
    DXAnnotationView * DXview = (DXAnnotationView *)view;
   [DXview addNewCallOutView:infoWindow];
    view = (MKAnnotationView *)DXview;
    
    if ([view isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)view)showCalloutView];
        
        view.layer.zPosition = 0;
    }
      //find if annontation for region or Mohafaza
    int annType = 0;
    for (id<MKAnnotation> ann in overlaysAmanat) {
        if (ann == annotation) {
            annType = 1;
            break;
        }
    }
    
    if (annType == 0) {
        for (id<MKAnnotation> ann in overlaysMohafazatPoints) {
            if (ann == annotation) {
                annType = 2;
                break;
            }
        }
    }
    
if (annType == 0) {
    UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ" message:@"لا يوجد بيانات" delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
               [alertConErr show];
                return ;
}
    //
    Region * reg = nil;
    if (annType == 1) {
                for (int i=0; i<self.regions.count; i++) {
                    Region * regs =[self.regions objectAtIndex:i];
                    if ([regs.ID isEqualToNumber: [NSNumber numberWithInt:[[annotation title]intValue]]]) {
                        reg = regs ;
                        break;
                    }
                }

    }
    else if (annType == 2)
    {
        for (int i=0; i<self.Governerates.count; i++) {
            Region * regs =[self.Governerates objectAtIndex:i];
            if ([regs.ID isEqualToNumber: [NSNumber numberWithInt:[[annotation title]intValue]]]) {
                reg = regs ;
                break;
            }
        }
    }
    
    //
     NSString *title = reg.RegionName;
    NSString *desc ;
    
    if (annType == 1) {
    switch (self.segFilter.selectedSegmentIndex ) {
        case 0:
        {
         int  perc = ([reg.VotersYoung intValue]/maxRegionYoungVoters)*100.0 ;
        desc  = [NSString stringWithFormat:@"أجمالى الشباب : %@ (%d%%)",reg.VotersYoung,perc];
        }
            break;
        case 1:
        {
            int  perc = ([reg.CandFemale intValue]/maxRegionFemaleVoters)*100.0 ;
            desc  = [NSString stringWithFormat:@"أجمالى النساء : %@ (%d%%)",reg.CandFemale,perc];
        }
            break;
            
        case 2:
        {
            int  perc = ([reg.VotersCount intValue]/maxRegionVoters)*100.0 ;
            desc  = [NSString stringWithFormat:@"أجمالى الناخبين : %@ (%d%%)",reg.VotersCount,perc];
        }
            break;
            
    }
    }
    else if (annType ==2)
    {
        switch (self.segFilter.selectedSegmentIndex ) {
            case 0:
            {
                int  perc = ([reg.VotersYoung intValue]/maxMohaafazaYoungVoters)*100.0 ;
                desc  = [NSString stringWithFormat:@"أجمالى الشباب : %@ (%d%%)",reg.VotersYoung,perc];
            }
                break;
            case 1:
            {
                int  perc = ([reg.CandFemale intValue]/maxMohaafazaFemaleVoters)*100.0 ;
                desc  = [NSString stringWithFormat:@"أجمالى النساء : %@ (%d%%)",reg.CandFemale,perc];
            }
                break;
                
            case 2:
            {
                int  perc = ([reg.VotersCount intValue]/maxMohaafazaVoters)*100.0 ;
                desc  = [NSString stringWithFormat:@"أجمالى الناخبين : %@ (%d%%)",reg.VotersCount,perc];
            }
                break;
                
        }
    }
    
    NSLog(@"id: %@",reg.ID);
    if (reg.ID == nil) {
        UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ" message:@"لا يوجد بيانات لهذة المحافظة" delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
        [alertConErr show];
        return ;
    }
    
    [infoWindow setFrame:CGRectMake(0, 0, 280, 65)];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [infoWindow setFrame:CGRectMake(0, 0, 370, 100)];
    }
    [infoWindow setBackgroundColor:[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0]];
    //
    UILabel * titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(5, 0, 270, 44)];
    [titleLabel setTextColor:[UIColor colorWithRed:0/255.0 green:126/255.0 blue:89/255.0 alpha:1.0]];
    titleLabel.textAlignment=NSTextAlignmentRight;
   
    NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc] initWithString:title];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:17.0] range:NSMakeRange(0,title.length)];
        [titleLabel setFrame:CGRectMake(10, 0, 352, 55)];
    }
    else{
        [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,title.length)];
    }
    [attributedTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,title.length)];
    titleLabel.attributedText=attributedTitleString;
    titleLabel.numberOfLines=0;
    [infoWindow addSubview:titleLabel];
    //
    UILabel *SubTitleLabel =[[UILabel alloc]initWithFrame:CGRectMake(12, 45, 250, 15)];
    //[SubTitleLabel setBackgroundColor:[UIColor yellowColor]];
    [SubTitleLabel setTextColor:[UIColor colorWithRed:61/255.0 green:35/255.0 blue:9/255.0 alpha:1.0]];
    [SubTitleLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:13.0]];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [SubTitleLabel setFrame:CGRectMake(10, 60, 250, 19)];
        [SubTitleLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:17.0]];
    }
    SubTitleLabel.textAlignment=NSTextAlignmentRight;
    [SubTitleLabel setText:desc];
     [infoWindow addSubview:SubTitleLabel];
    //
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formatter setGroupingSeparator:groupingSeparator];
    [formatter setGroupingSize:3];
    [formatter setAlwaysShowsDecimalSeparator:NO];
    [formatter setUsesGroupingSeparator:YES];
    //                                                             80, 45, 85, 15
    UILabel *numberLabel =[[UILabel alloc]initWithFrame:CGRectMake(170, 45, 105, 15)];
    //[numberLabel setBackgroundColor:[UIColor cyanColor]];
    [numberLabel setTextColor:[UIColor colorWithRed:11/255.0 green:93/255.0 blue:60/255.0 alpha:1.0]];
    [numberLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:11.0]];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        [numberLabel setFrame:CGRectMake(50, 60, 169, 19)];
        [numberLabel setFont:[UIFont fontWithName:@"HacenSaudiArabia" size:15.0]];
    }
    numberLabel.textAlignment=NSTextAlignmentRight;
    [numberLabel setText:desc];
    //[infoWindow addSubview:numberLabel];
    //
    CGRect BarChartFrame = infoWindow.frame;
    BarChartFrame.origin.y =20;
    BarChartFrame.origin.x = -((BarChartFrame.size.width-30)/2);
    infoWindow.frame =BarChartFrame;
    [mapView setCenterCoordinate:[annotation coordinate] animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)checkActivityToStop{
    if (FinishedAPI >=2) {
        [self.activity stopAnimating];
        self.btnReload.hidden = NO;
    }
}

-(void)httpPostRequest
{
    @try {
        maxRegionFemaleVoters = 0;
        maxRegionVoters = 0;
        maxRegionYoungVoters = 0;
        //
        NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
        NSError *error = nil;
        NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
        NSURL *url = [Helper getWebserviceURL:@"GetDensityByRegion"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data.length > 0 && connectionError == nil)
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                
                NSError * err =nil;
                NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:NSJSONReadingAllowFragments
                                                                                error:&err];
                NSString * results =[receivedData objectForKey:@"d"] ;
                if (results == nil || [results isEqualToString:@""]) {
                    UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                    [alertConErr show];
                    return ;
                }
                NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                             options:NSJSONReadingAllowFragments
                                                               error:&err];
                for (int i=0 ;i < ff.count; i++) {
                    NSDictionary * resultsItems =[ff objectAtIndex:i];
                    Region * reg=[[Region alloc]init];
                    reg.RegionName=[resultsItems objectForKey:@"Name"];
                    reg.Lat=[resultsItems objectForKey:@"lat"];
                    reg.Long=[resultsItems objectForKey:@"long"];
                    reg.VotersCount=[resultsItems objectForKey:@"VotersCount"];
                    reg.CandFemale=[resultsItems objectForKey:@"FemaleVotersCount"];
                    reg.VotersYoung=[resultsItems objectForKey:@"Age1VotersCount"];
                    reg.ID=[resultsItems objectForKey:@"ID"];
                    //
                    reg.TypeID=3;

                    if ([reg.VotersCount intValue] > maxRegionVoters) {
                        maxRegionVoters = [reg.VotersCount intValue];
                    }
                    //
                    if ([reg.CandFemale intValue] > maxRegionFemaleVoters) {
                        maxRegionFemaleVoters = [reg.CandFemale intValue];
                    }
                    //
                    if ([reg.VotersYoung intValue] > maxRegionYoungVoters) {
                        maxRegionYoungVoters = [reg.VotersYoung intValue];
                    }
                    
                    [self.regions addObject:reg];
                    //
                }
                [self showOverlayForMapView:self.mapView];
            }else
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                [alertConErr show];
            }
        }];
    }
    @catch (NSException *exception) {
        FinishedAPI ++;
        [self checkActivityToStop];
        NSLog(@"%@",exception.description);
    }
}
//
-(void)showOverlayForMapView:(MKMapView *)mapView
{
    MapLocationView *selectedAnn = nil;
    if ([mapView selectedAnnotations ] && [mapView selectedAnnotations ].count >0 ) {
        selectedAnn =  [mapView selectedAnnotations ][0];
    }
    //
//    if (mapView.region.span.latitudeDelta <= [[staticVariables singelton].DivistionZoomLevel floatValue] && isOverlayAdded == NO) {
//        [mapView addOverlays:overlays];
//        [mapView addAnnotations:KMLAnnonations];
//        isOverlayAdded = YES;
//    } else  if (mapView.region.span.latitudeDelta > [[staticVariables singelton].DivistionZoomLevel floatValue] && isOverlayAdded == YES){
//        [mapView removeOverlays:overlays];
//        [mapView removeAnnotations:KMLAnnonations];
//        isOverlayAdded = NO;
//        
//    }
    //
    if (mapView.region.span.latitudeDelta > [[staticVariables singelton].CityZoomLevel floatValue] && isOverlayRegionAdded == NO  && self.regions && self.regions.count>0) {
        [mapView addOverlays:overlaysAmanat];
        [mapView addAnnotations:overlaysAmanat];
        isOverlayRegionAdded = YES;
    } else  if (mapView.region.span.latitudeDelta <= [[staticVariables singelton].CityZoomLevel floatValue] && isOverlayRegionAdded == YES){
        // Close the Callout
        //
        if (openedAnnotationView) {
            DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
            [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
            DXview.calloutView = nil;
            DXview.layer.zPosition = -1;
            openedAnnotationView = nil;
            [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
            
        }
        //

        [mapView removeOverlays:overlaysAmanat];
        isOverlayRegionAdded = NO;
        [mapView removeAnnotations:overlaysAmanat];
    }
    //
    if (mapView.region.span.latitudeDelta <= [[staticVariables singelton].CityZoomLevel floatValue] && mapView.region.span.latitudeDelta > [[staticVariables singelton].DivistionZoomLevel floatValue]  && isOverlayMohafazatAdded == NO && self.Governerates && self.Governerates.count>0) {
        [mapView addOverlays:overlaysMohafazat];
        [mapView addAnnotations:overlaysMohafazatPoints];
        
        isOverlayMohafazatAdded = YES;
    } else  if (mapView.region.span.latitudeDelta >= [[staticVariables singelton].CityZoomLevel floatValue] && isOverlayMohafazatAdded == YES){
        
        // Close the Callout
        //
        if (openedAnnotationView) {
            DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
            [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
            DXview.calloutView = nil;
            DXview.layer.zPosition = -1;
            openedAnnotationView = nil;
            [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
            
        }
        //
        [mapView removeOverlays:overlaysMohafazat];
        [mapView removeAnnotations:overlaysMohafazatPoints];
        
        isOverlayMohafazatAdded = NO;
    }
    //
}
//
-(void)httpPostRequestGovernerates
{
    @try {
        NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
        NSError *error = nil;
        NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
        NSURL *url = [Helper getWebserviceURL:@"GetDensityByGovernorates"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data.length > 0 && connectionError == nil)
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                
                NSError * err =nil;
                NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:NSJSONReadingAllowFragments
                                                                                error:&err];
                NSString * results =[receivedData objectForKey:@"d"] ;
                if (results == nil || [results isEqualToString:@""]) {
                    UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                    [alertConErr show];
                    return ;
                }
                NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                             options:NSJSONReadingAllowFragments
                                                               error:&err];
                for (int i=0 ;i < ff.count; i++) {
                    NSDictionary * resultsItems =[ff objectAtIndex:i];
                    Region * reg=[[Region alloc]init];
                    reg.RegionName=[resultsItems objectForKey:@"Name"];
                    reg.VotersCount=[resultsItems objectForKey:@"VotersCount"];
                    reg.CandFemale=[resultsItems objectForKey:@"FemaleVotersCount"];
                    reg.VotersYoung=[resultsItems objectForKey:@"Age1VotersCount"];
                    reg.ID =   [resultsItems objectForKey:@"ID"];
                    //
                    [self.Governerates addObject:reg];
                    //
                    if ([reg.VotersCount intValue] > maxMohaafazaVoters) {
                    maxMohaafazaVoters = [reg.VotersCount intValue];
                }
                //
                if ([reg.CandFemale intValue] > maxMohaafazaFemaleVoters) {
                    maxMohaafazaFemaleVoters = [reg.CandFemale intValue];
                }
                //
                if ([reg.VotersYoung intValue] > maxMohaafazaYoungVoters) {
                    maxMohaafazaYoungVoters = [reg.VotersYoung intValue];
                }
                
                [self.Governerates addObject:reg];
                //
            }
            [self showOverlayForMapView:self.mapView];
                
            }else
            {
                FinishedAPI ++;
                [self checkActivityToStop];
                UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                [alertConErr show];
            }
        }];
    }
    @catch (NSException *exception) {
        FinishedAPI ++;
        [self checkActivityToStop];
        NSLog(@"%@",exception.description);
    }
}
//

#pragma mark - lazy init

-(NSMutableArray *)regions
{
    if (!_regions) {
        _regions=[[NSMutableArray alloc]init];
    }
    return _regions;
}

-(NSMutableArray *)Governerates
{
    if (!_Governerates) {
        _Governerates=[[NSMutableArray alloc]init];
    }
    return _Governerates;
}

- (IBAction)btnMapKeysTapped:(id)sender {
    self.viewMapKeys.hidden = NO;
}
- (IBAction)btnCloseKeysTapped:(id)sender {
    self.viewMapKeys.hidden = YES;
}

- (IBAction)btnReloadTapped:(id)sender {
    
    self.btnReload.hidden = YES;
    
    if (openedAnnotationView) {
        // Close the Callout
        DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
    }
    //
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
    self.mapView.delegate = self;
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = 20;
    zoomIn.span.latitudeDelta  = 20;
    [self.mapView setRegion:zoomIn animated:NO];
    //
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    
    [[self mapView]removeOverlays:overlaysAmanat];
    [[self mapView]removeAnnotations:overlaysAmanat];
    [[self mapView]removeOverlays:overlaysMohafazat];
    [[self mapView] removeAnnotations:overlaysMohafazat];
    [self.regions removeAllObjects];
    [self.divisions removeAllObjects];
    [self.centers removeAllObjects];
    isOverlayMohafazatAdded = NO;
    isOverlayRegionAdded = NO;
    
    [self httpPostRequest];
    [self httpPostRequestGovernerates];
}
- (IBAction)btnShowIntensityTapped:(id)sender {
      self.viewMapKeys.hidden = YES;
    //
    self.btnReload.hidden = YES;
    
    if (openedAnnotationView) {
        // Close the Callout
        DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
    }
    //
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
    self.mapView.delegate = self;
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = 20;
    zoomIn.span.latitudeDelta  = 20;
    [self.mapView setRegion:zoomIn animated:NO];
    //
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    
    [[self mapView]removeOverlays:overlaysAmanat];
    [[self mapView]removeAnnotations:overlaysAmanat];
    [[self mapView]removeOverlays:overlaysMohafazat];
    [[self mapView] removeAnnotations:overlaysMohafazat];
    [self.regions removeAllObjects];
    [self.divisions removeAllObjects];
    [self.centers removeAllObjects];
    
    isOverlayMohafazatAdded = NO;
    isOverlayRegionAdded = NO;
    [self httpPostRequest];
    [self httpPostRequestGovernerates];
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
