//
//  AppealsMapReportResultViewController.h
//  MOMRA
//
//  Created by aya on 8/11/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "BarChartInfoWindow.h"
#import "WinningFullViewChartViewController.h"
#import <MapKit/MapKit.h>
#import "MapLocationView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "KMLParser.h"

@interface AppealsMapReportResultViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)close:(id)sender;
-(void)setSearchValuesWithRegionID:(NSString * )RegionID CityID:(NSString * )CityID  AppealStatus:(NSString * )AppealStatus AppellantType:(NSString * )AppellantType AppealStatusText:(NSString * )AppealStatusText AppellantTypeText:(NSString * )AppellantTypeText;
@end
