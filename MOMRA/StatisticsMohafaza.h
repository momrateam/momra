//
//  StatisticsCenter.h
//  MOMRA
//
//  Created by aya on 8/10/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatisticsMohafaza : UIView
@property (weak, nonatomic) IBOutlet UILabel *VotersCount;
@property (weak, nonatomic) IBOutlet UILabel *VotersWemen;
@property (weak, nonatomic) IBOutlet UILabel *RegionName;
@property (weak, nonatomic) IBOutlet UILabel *VotersYoung;

@end
