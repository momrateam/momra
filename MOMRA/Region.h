//
//  Region.h
//  MOMRA
//
//  Created by aya on 6/22/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Region : NSObject
@property (retain, nonatomic) NSNumber *ID;
@property (retain, nonatomic) NSNumber *VotersCount;
@property (retain, nonatomic) NSNumber *CandidatesCount;
@property (retain, nonatomic) NSNumber *DisregardedCandidatesCount;
@property (retain, nonatomic) NSNumber *DisregardedVotersCount;
@property (retain, nonatomic) NSNumber *VotersCentersCount;
@property (retain, nonatomic) NSNumber *CandidatesCentersCount;
@property (retain, nonatomic) NSNumber *AdminUsersCountCount;
@property (retain, nonatomic) NSNumber *VotersVotesCount;
@property (retain, nonatomic) NSNumber *WinnersCount;
@property (retain, nonatomic) NSNumber *Lat;
@property (retain, nonatomic) NSNumber *Long;
@property (retain, nonatomic) NSString *RegionName;
@property (nonatomic, assign) int TypeID;
@property (retain, nonatomic) NSNumber *CandFemale;
@property (retain, nonatomic) NSNumber *CandMale;
@property (retain, nonatomic) NSNumber *RecNewFemaleCount;
@property (retain, nonatomic) NSNumber *RecNewMaleCount;
@property (retain, nonatomic) NSNumber *RecNewCount;
@property (retain, nonatomic) NSNumber *lastCount;
@property (retain, nonatomic) NSNumber *isCanidiateCenter;
@property (retain, nonatomic) NSNumber *MAX_VOTER_SERIAL;
@property (retain, nonatomic) NSNumber *VotersYoung;

@end
