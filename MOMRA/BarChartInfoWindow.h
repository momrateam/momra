//
//  BarChartInfoWindow.h
//  MOMRA
//
//  Created by aya on 7/5/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOMRA-Swift.h"

@interface BarChartInfoWindow : UIView
@property (weak, nonatomic) IBOutlet BarChartView *barChart;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *criteria;

@end
