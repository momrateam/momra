//
//  staticVariables.h
//  MOMRA
//
//  Created by aya on 7/13/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface staticVariables : NSObject
@property(nonatomic,retain) NSString *UserTypeID;
@property(nonatomic,retain) NSDictionary * AllStaticisticDic ;
@property(nonatomic,retain) NSDictionary * dataDate;
+(staticVariables*) singelton;
@property(nonatomic,retain) NSNumber * DivistionZoomLevel;
@property(nonatomic,retain) NSNumber * CenterZoomLevel;
@property(nonatomic,retain) NSNumber * CityZoomLevel;
@property(nonatomic,retain) NSNumber * RegionZoomLevel;

@end
