//
//  Helper.m
//  callApi
//
//  Created by aya on 6/18/15.
//  Copyright (c) 2015 aya. All rights reserved.
//

#import "Helper.h"

@implementation Helper

+(NSURL *)getWebserviceURL: (NSString *)ServiceMethod
{
    // return  [NSURL URLWithString:[NSString stringWithFormat: @"http://197.44.127.186/MomraElectionsService2/MomraElectionsService.asmx/%@",ServiceMethod]];
   return  [NSURL URLWithString:[NSString stringWithFormat: @"http://services.intekhab.gov.sa/GeoDashWebService/MomraElectionsService.asmx/%@",ServiceMethod]];
    
}
+(NSString *)getTimeStamp
{
    NSDate *date = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYYMMdd"];
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
   dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    dateFormatter.locale= locale;
    NSString *dateString = [dateFormatter stringFromDate:date];
    return [NSString stringWithFormat:@"ITRootsMomraDBServiceToken%@",dateString];
}

+(void)configureBarChart: (BarChartView *)barChart inInfoWindow:(BOOL)inInfoWindow{
    barChart.descriptionText = @"";
    barChart.noDataTextDescription = @"You need to provide data for the chart.";
    barChart.pinchZoomEnabled = NO;
    barChart.drawBarShadowEnabled = NO;
    barChart.drawGridBackgroundEnabled = NO;
    //
    ChartLegend *legend = barChart.legend;
    legend.position = ChartLegendPositionBelowChartRight;
    legend.wordWrapEnabled=true;
    legend.direction=ChartLegendDirectionRightToLeft;
    legend.font = [UIFont fontWithName:@"HacenSaudiArabia" size:11.0f];
    //
    ChartXAxis *xAxis = barChart.xAxis;
    xAxis.labelFont = [UIFont fontWithName:@"HacenSaudiArabia" size:7.0f];
    xAxis.labelPosition = XAxisLabelPositionBottom;
    //
    ChartYAxis *leftAxis = barChart.leftAxis;
    leftAxis.labelFont = [UIFont fontWithName:@"HacenSaudiArabia" size:8.0f];
    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
    leftAxis.valueFormatter.maximumFractionDigits = 1;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.spaceTop = 0.25;
    //
    barChart.rightAxis.enabled = NO;
    barChart.valueFormatter = [[NSNumberFormatter alloc] init];
    barChart.valueFormatter.maximumFractionDigits = 1;
    if (!inInfoWindow) {
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            legend.font = [UIFont fontWithName:@"HacenSaudiArabia" size:15.0f];
            xAxis.labelFont = [UIFont fontWithName:@"HacenSaudiArabia" size:10.0f];
            leftAxis.labelFont = [UIFont fontWithName:@"HacenSaudiArabia" size:10.0f];
        }
    }
    else{
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            xAxis.labelFont = [UIFont fontWithName:@"HacenSaudiArabia" size:10.0f];
        }
    }
}

+(void)configurePieChart:(PieChartView *)pieChart{
    pieChart.usePercentValuesEnabled = YES;
    pieChart.holeTransparent = YES;
    pieChart.holeRadiusPercent = 0.58;
    pieChart.transparentCircleRadiusPercent = 0.61;
    pieChart.descriptionText = @"";
    pieChart.drawCenterTextEnabled = YES;
    pieChart.drawHoleEnabled = NO;
    pieChart.rotationAngle = 0.0;
    pieChart.rotationEnabled = YES;
    //
    //pieChart.legend.enabled=NO;
    pieChart.legend.position = ChartLegendPositionBelowChartCenter;
    pieChart.legend.wordWrapEnabled=true;
    pieChart.legend.direction=ChartLegendDirectionRightToLeft;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        pieChart.legend.font = [UIFont fontWithName:@"HacenSaudiArabia" size:15.0f];
    }else{
        pieChart.legend.font = [UIFont fontWithName:@"HacenSaudiArabia" size:11.0f];
    }
}
//
+(NSMutableDictionary *)convertStringDicValueToInt:(NSMutableDictionary *)Dic{
    NSMutableDictionary * ConvertedArray = [[NSMutableDictionary alloc]init];
    
    for ( int  i = 0;i< Dic.count ; i++ ) {
        NSString * objectVaue = [Dic allValues][i];
        NSNumber * objectKey = [NSNumber numberWithInt: [ [Dic allKeys][i]  intValue] ];
        [ConvertedArray setObject:objectVaue forKey:objectKey];
    }
    
    
    
    return ConvertedArray;
}
@end
