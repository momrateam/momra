//
//  staticVariables.m
//  MOMRA
//
//  Created by aya on 7/13/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "staticVariables.h"

@implementation staticVariables

+(staticVariables *)singelton{
    static dispatch_once_t pred;
    static staticVariables *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[staticVariables alloc] init];
        shared.CenterZoomLevel = @2;
        shared.DivistionZoomLevel = @3;
        shared.RegionZoomLevel = @20;
        shared.CityZoomLevel = @10;
    });
    return shared;
}

@end
