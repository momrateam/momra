//
//  Helper.h
//  callApi
//
//  Created by aya on 6/18/15.
//  Copyright (c) 2015 aya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOMRA-Swift.h"

@interface Helper : NSObject
+(NSString *)getTimeStamp;
+(void)configureBarChart: (BarChartView *)barChart inInfoWindow:(BOOL)inInfoWindow;
+(void)configurePieChart: (PieChartView *)pieChart;
+(NSURL *)getWebserviceURL: (NSString *)ServiceMethod;
+(NSMutableDictionary *)convertStringDicValueToInt:(NSMutableDictionary *)Dic;
@end
