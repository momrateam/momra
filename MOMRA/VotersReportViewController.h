//
//  VotersReportViewController.h
//  MOMRA
//
//  Created by aya on 6/23/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "ViewController.h"
#import "Helper.h"

@interface VotersReportViewController : ViewController<UIPickerViewDataSource, UIPickerViewDelegate>
- (IBAction)close:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *ckFirstYear;
@property (weak, nonatomic) IBOutlet UISwitch *ckSecYear;
@property (weak, nonatomic) IBOutlet UISwitch *ckCurrentYear;
- (IBAction)ckGenderChanged:(id)sender;
- (IBAction)ckDateChanged:(id)sender;
- (IBAction)ckCenterChanged:(id)sender;
- (IBAction)ckDivitionChanged:(id)sender;
- (IBAction)ckCityChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *ckCity;
@property (weak, nonatomic) IBOutlet UISwitch *ckGender;
@property (weak, nonatomic) IBOutlet UISwitch *ckDate;
@property (weak, nonatomic) IBOutlet UISwitch *ckCenter;
@property (weak, nonatomic) IBOutlet UISwitch *ckCircle;
- (IBAction)ShowInMap:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtCenter;
@property (weak, nonatomic) IBOutlet UITextField *txtage;
@property (weak, nonatomic) IBOutlet UITextField *txtWeek;
@property (weak, nonatomic) IBOutlet UITextField *txtDay;
@property (weak, nonatomic) IBOutlet UITextField *circle;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSeg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtRegion;

- (IBAction)ckAgeChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *ckAge;
@end
