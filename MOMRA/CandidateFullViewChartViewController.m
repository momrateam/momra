//
//  CandidateFullViewChartViewController.m
//  MOMRA
//
//  Created by aya on 7/7/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "CandidateFullViewChartViewController.h"

@interface CandidateFullViewChartViewController ()

@end

@implementation CandidateFullViewChartViewController
@synthesize regCurrent;
@synthesize regFirst;
@synthesize regSecond;
//
@synthesize parameters;
@synthesize chart;
@synthesize dictAges;
@synthesize reportTitle;
@synthesize mainContainerView;

static bool isFirstChart;
static bool isSecondChart;
static bool isThirdChart;
static BarChartView *secondChart;
static BarChartView *thirdChart;
static DetailsTable * table;

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
}

-(void)orientationChanged:(NSNotification *)notification
{
//    if (!UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.contentSize.height);
        [self adjustViewsForOrientation:[[UIApplication sharedApplication]statusBarOrientation]];
//    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    CGRect  frame=table.frame;
    CGRect  containerFrame=mainContainerView.frame;
    NSInteger wd = (containerFrame.size.width-frame.size.width)/2;
    frame.origin.x=wd;
    [table setFrame:frame];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    @try {
        secondChart=Nil;
        thirdChart=Nil;
        isFirstChart=NO;
        isSecondChart=NO;
        isThirdChart=NO;
        table=Nil;
        NSDictionary * reg;
        //
        CGFloat firstChartY=0;
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            firstChartY=100;
        }else{
            firstChartY=90;
        }
        //
        int numberOfCharts=0;
        if (regCurrent != Nil) {
            reg =regCurrent;
            [Helper configureBarChart:chart inInfoWindow:NO];
            isFirstChart=YES;
            //
            [self loadChart:reg withBarChartView:chart title:@"(الدورة الثالثة)"];
            numberOfCharts++;
            float sizeOfContent = 0;
            UIView *lLast = self.chart;
            NSInteger wd = lLast.frame.origin.y;
            NSInteger ht = lLast.frame.size.height;
            sizeOfContent = wd+ht;
            self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
        }
        if (regSecond != Nil){
            reg =regSecond;
            if (numberOfCharts == 0) {
                [Helper configureBarChart:chart inInfoWindow:NO];
                isFirstChart=YES;
                //
                [self loadChart:reg withBarChartView:chart title:@"(الدورة الثانية)"];
                numberOfCharts++;
                float sizeOfContent = 0;
                UIView *lLast = self.chart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
            }
            else{
                secondChart=[[BarChartView alloc] initWithFrame:CGRectMake(0, firstChartY+(numberOfCharts*chart.frame.size.height), chart.frame.size.width, chart.frame.size.height)];
                secondChart.autoresizingMask=UIViewAutoresizingFlexibleWidth;
                [mainContainerView addSubview:secondChart];
                float sizeOfContent = 0;
                UIView *lLast = secondChart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
                
                [Helper configureBarChart:secondChart inInfoWindow:NO];
                isSecondChart=YES;
                //
                [self loadChart:reg withBarChartView:secondChart title:@"(الدورة الثانية)"];
                numberOfCharts++;
            }
        }
        if(regFirst != Nil){
            reg =regFirst;
            if (numberOfCharts == 0) {
                [Helper configureBarChart:chart inInfoWindow:NO];
                isFirstChart=YES;
                //
                [self loadChart:reg withBarChartView:chart  title:@"(الدورة الأولى)"];
                numberOfCharts++;
                float sizeOfContent = 0;
                UIView *lLast = self.chart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
            }
            else{
                thirdChart=[[BarChartView alloc] initWithFrame:CGRectMake(0, firstChartY+(numberOfCharts*chart.frame.size.height), chart.frame.size.width, chart.frame.size.height)];
                thirdChart.autoresizingMask=UIViewAutoresizingFlexibleWidth;
                [mainContainerView addSubview:thirdChart];
                float sizeOfContent = 0;
                UIView *lLast = thirdChart;
                NSInteger wd = lLast.frame.origin.y;
                NSInteger ht = lLast.frame.size.height;
                sizeOfContent = wd+ht;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
                //
                [Helper configureBarChart:thirdChart inInfoWindow:NO];
                isThirdChart=YES;
                //
                [self loadChart:reg withBarChartView:thirdChart title:@"(الدورة الأولى)"];
                numberOfCharts++;
            }
        }
        //
        NSString *title = [NSString stringWithFormat:@"%@ %@ %@",@"إحصائيات",[reg objectForKey:@"Name"],@"حسب المعايير التالية"];
        NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc] initWithString:title];
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:14.0] range:NSMakeRange(0,title.length)];
        }else{
            [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:12.0] range:NSMakeRange(0,title.length)];
        }
        [attributedTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,title.length)];
        reportTitle.attributedText=attributedTitleString;
        self.header.text=title;
        //
        NSMutableString * criteria=[[NSMutableString alloc]init];
        //
        if (![[parameters valueForKey:@"Age"] isEqualToString:@"-1"]) {
            if ([[parameters valueForKey:@"Age"] isEqualToString:@"0"])
            {
                [criteria appendString:@"الفئة العمرية : الكل \n"];
            }
            else
            {
                [criteria appendString:[NSString stringWithFormat:@" الفئة العمرية : %@ \n",[dictAges objectForKey:[parameters valueForKey:@"Age"]]]];
            }
        }
        //
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"]) {
            
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
        {
            [criteria appendString:@"النوع : ذكور \n"];
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
        {
            [criteria appendString:@"النوع : إناث \n"];
        }
        else
        {
            [criteria appendString:@"النوع : الكل \n"];
        }
        //
        if (!([[parameters valueForKey:@"Day"] isEqualToString:@"-1"])) {
            //            [criteria appendString:[parameters objectForKey:@"Duration"]];
            [criteria appendString:[NSString stringWithFormat:@"اليوم : %@",[parameters objectForKey:@"Duration"]]];
        }
        else if (!([[parameters valueForKey:@"Week"] isEqualToString:@"-1"])) {
            //                [criteria appendString:[parameters objectForKey:@"Duration"]];
            [criteria appendString:[NSString stringWithFormat:@"الإسبوع : %@",[parameters objectForKey:@"Duration"]]];
        }
        //    if ([[parameters valueForKey:@"Day"] isEqualToString:@"-1"] && [[parameters valueForKey:@"Week"] isEqualToString:@"-1"]) {
        //        [criteria appendString:[parameters objectForKey:@"Duration"]];
        //    }
        self.criteria.text=criteria;
        //
        if (numberOfCharts >1) {
            NSMutableArray * values=[[NSMutableArray alloc]init];
            NSLog(@"barChart : %ld",(long)[chart yValueSum]);
//            if ([chart yValueSum] != 0) {
            if (isFirstChart) {
                KeyValue * first=[[KeyValue alloc]init];
                if ([[parameters objectForKey:@"CurrentYear"]isEqualToString:@"1"]) {
                    first.Key=@"الثالثة";
                }
                else if ([[parameters objectForKey:@"SecondYear"]isEqualToString:@"1"]) {
                    first.Key=@"الثانية";
                }
                else if ([[parameters objectForKey:@"FirstYear"]isEqualToString:@"1"]) {
                    first.Key=@"الأولى";
                }
                first.Value=[NSString stringWithFormat:@"%ld",(long)[chart yValueSum]];
                [values addObject:first];
            }
            NSLog(@"secondChart : %ld",(long)[secondChart yValueSum]);
//            if ([secondChart yValueSum] != 0) {
            if (isSecondChart) {
                KeyValue * first=[[KeyValue alloc]init];
                first.Key=@"الثانية";
                first.Value=[NSString stringWithFormat:@"%ld",(long)[secondChart yValueSum]];
                [values addObject:first];
            }
            NSLog(@"thirdChart : %ld",(long)[thirdChart yValueSum]);
//            if ([thirdChart yValueSum] != 0) {
            if (isThirdChart) {
                KeyValue * first=[[KeyValue alloc]init];
                first.Key=@"الأولى";
                first.Value=[NSString stringWithFormat:@"%ld",(long)[thirdChart yValueSum]];
                [values addObject:first];
            }
            DetailsTable *DetailsTable =  [[[NSBundle mainBundle] loadNibNamed:@"DetailsTable" owner:self options:nil] objectAtIndex:0];
            table=DetailsTable;
            [DetailsTable LoadDataWithArrayOfKeyValue:values];
            //
            CGRect frame=DetailsTable.frame;
            frame.origin.y=frame.origin.y+firstChartY+(numberOfCharts*chart.frame.size.height);
            DetailsTable.frame=frame;
            //
            [self adjustViewsForOrientation:[[UIApplication sharedApplication]statusBarOrientation]];
            //
            float sizeOfContent = 0;
            UIView *lLast = DetailsTable;
            NSInteger wd = lLast.frame.origin.y;
            NSInteger ht = lLast.frame.size.height;
            sizeOfContent = wd+ht;
            self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
            //
            [mainContainerView addSubview:DetailsTable];
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadChart :(NSDictionary *)reg withBarChartView:(BarChartView *)barChart title : (NSString *)title{
    @try {
        NSArray * arrofKeys = [[dictAges allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        //male and female
        if ([[parameters valueForKey:@"Sex"] isEqualToString:@"3"]) {
            if ([[parameters valueForKey:@"Age"] isEqualToString:@"0"]) {
                NSMutableArray *xVals = [[NSMutableArray alloc] init];
                for (int i = 0; i < arrofKeys.count; i++)
                {
                    [xVals addObject:[dictAges objectForKey:arrofKeys[i]]];
                }
                
                NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
                NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
                for (int i = 0; i < arrofKeys.count; i++)
                {
                    double val1 = [[reg objectForKey:[NSString stringWithFormat:@"MaleAge%dCandidatesCount",i+2]]doubleValue];
                    double val2 = [[reg objectForKey:[NSString stringWithFormat:@"FemaleAge%dCandidatesCount",i+2]]doubleValue];
                    [yVals1 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val1)] xIndex:i]];
                    [yVals2 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val2)] xIndex:i]];
                }
                BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@"ذكور"];
                BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:[NSString stringWithFormat:@"إناث  %@",title]];
                //
                set1.colors=@[[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
                set2.colors=@[[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
                //
                NSMutableArray *dataSets = [[NSMutableArray alloc] init];
                [dataSets addObject:set1];
                [dataSets addObject:set2];
                //
                BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
                data.groupSpace = 0.8;
                if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                    [data setValueFont:[UIFont systemFontOfSize:12.0f]];
                }
                barChart.data = data;
            } else {
                NSMutableArray *xVals = [[NSMutableArray alloc] init];
                if ([[parameters valueForKey:@"Age"] isEqualToString:@"-1"]) {
                    [xVals addObject:@"إجمالى"];
                }else{
                    [xVals addObject:[dictAges objectForKey:[parameters valueForKey:@"Age"]]];
                }
                //
                NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
                NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
                double val1 = [[reg objectForKey:@"MaleAgeCandidatesCount"]doubleValue];
                double val2 = [[reg objectForKey:@"FemaleAgeCandidatesCount"]doubleValue];
                [yVals1 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val1)] xIndex:0]];
                [yVals2 addObject:[[BarChartDataEntry alloc] initWithValues:@[@(val2)] xIndex:0]];
                //
                BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@"ذكور"];
                BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yVals2 label:[NSString stringWithFormat:@"إناث  %@",title]];
                set1.colors=@[[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
                set2.colors=@[[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
                //
                NSMutableArray *dataSets = [[NSMutableArray alloc] init];
                [dataSets addObject:set1];
                [dataSets addObject:set2];
                //
                BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
                data.groupSpace = 0.8;
                if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                    [data setValueFont:[UIFont systemFontOfSize:12.0f]];
                }
                barChart.data = data;
            }
            
        }
        else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"1"] || [[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
        {
            
            if ([[parameters valueForKey:@"Age"] isEqualToString:@"0"]) {
                NSMutableArray *xVals = [[NSMutableArray alloc] init];
                
                for (int i = 0; i < arrofKeys.count; i++)
                {
                    [xVals addObject:[dictAges objectForKey:arrofKeys[i]]];
                }
                
                NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
                for (int i = 0; i < arrofKeys.count; i++)
                {
                    double val1 = [[reg objectForKey:[NSString stringWithFormat:@"Age%dCandidatesCount",i+2]]doubleValue];
                    [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:i]];
                }
                BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:title];
                //
                if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"])
                {
                    [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:136/255.0 blue:91/255.0 alpha:1.0 ]];
                }
                else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
                {
                    [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
                }
                else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
                {
                    [set1 setColor:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
                }
                //
                NSMutableArray *dataSets = [[NSMutableArray alloc] init];
                [dataSets addObject:set1];
                
                BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
                //barChart.legend.enabled = NO;
                if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                    [data setValueFont:[UIFont systemFontOfSize:12.0f]];
                }
                barChart.data = data;
            }
            else
            {
                NSMutableArray *xVals = [[NSMutableArray alloc] init];
                if ([[parameters valueForKey:@"Age"] isEqualToString:@"-1"]) {
                    [xVals addObject:@"إجمالى"];
                }else{
                    [xVals addObject:[dictAges objectForKey:[parameters valueForKey:@"Age"]]];
                }
                
                NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
                double val1 = [[reg objectForKey:@"AgeCandidatesCount"]doubleValue];
                [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:0]];
                BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:title];
                //
                if ([[parameters valueForKey:@"Sex"] isEqualToString:@"-1"])
                {
                    [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:136/255.0 blue:91/255.0 alpha:1.0 ]];
                }
                else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"1"])
                {
                    [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:97/255.0 blue:43/255.0 alpha:1.0]];
                }
                else if ([[parameters valueForKey:@"Sex"] isEqualToString:@"2"])
                {
                    [set1 setColor:[[UIColor alloc]initWithRed:146/255.0 green:119/255.0 blue:60/255.0 alpha:1.0]];
                }
                //
                set1.barSpace=0.7;
                NSMutableArray *dataSets = [[NSMutableArray alloc] init];
                [dataSets addObject:set1];
                
                BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
                //barChart.legend.enabled = NO;
                if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                    [data setValueFont:[UIFont systemFontOfSize:12.0f]];
                }
                barChart.data = data;
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)close:(id)sender {
    @try {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}

@end
