//
//  DisCandidatesReportViewController.h
//  MOMRA
//
//  Created by aya on 6/25/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface DisCandidatesReportViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>


- (IBAction)ckExReasonChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *ckExReason;
@property (weak, nonatomic) IBOutlet UITextField *txtExReason;

- (IBAction)close:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSeg;






@property (weak, nonatomic) IBOutlet UISwitch *ckFirstYear;
@property (weak, nonatomic) IBOutlet UISwitch *ckSecYear;
@property (weak, nonatomic) IBOutlet UISwitch *ckCurrentYear;
- (IBAction)ckGenderChanged:(id)sender;
- (IBAction)ckDivitionChanged:(id)sender;
- (IBAction)ckCityChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *ckCity;
@property (weak, nonatomic) IBOutlet UISwitch *ckGender;
@property (weak, nonatomic) IBOutlet UISwitch *ckCircle;
- (IBAction)ShowInMap:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtage;

@property (weak, nonatomic) IBOutlet UITextField *circle;

@property (weak, nonatomic) IBOutlet UITextField *txtRegion;

- (IBAction)ckAgeChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *ckAge;
@end
