//
//  DetailsTable.h
//  MOMRA
//
//  Created by aya on 7/26/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsTable : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblTrdYearTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblScYearTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblFrstYearTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblScYrPrc;
@property (weak, nonatomic) IBOutlet UILabel *lblTrdYrPrc;
@property (weak, nonatomic) IBOutlet UILabel *lblFrstYrPrc;
@property (weak, nonatomic) IBOutlet UILabel *lblTrdYr;
@property (weak, nonatomic) IBOutlet UILabel *lblScYr;
@property (weak, nonatomic) IBOutlet UILabel *lblFrstYr;

-(void)LoadDataWithArrayOfKeyValue:(NSMutableArray *) arrOfYears;
@end
