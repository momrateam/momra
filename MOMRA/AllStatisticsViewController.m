//
//  AllStatisticsViewController.m
//  MOMRA
//
//  Created by aya on 6/21/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "AllStatisticsViewController.h"
#import "CandStatisticViewController.h"
#import "VoterStatisticViewController.h"
#import "Helper.h"
#import "VotersCentersReportViewController.h"
#import "CandidatesCentersReportViewController.h"
#import "GateEntryReportViewController.h"
#import "WinnersReportViewController.h"
#import "VotersVotesReportViewController.h"
#import "AppealsVotersViewController.h"
#import "staticVariables.h"

@interface AllStatisticsViewController ()
@end

@implementation AllStatisticsViewController
int ExVoter;
int Voter;

int ExCand;
int Cand;
static int FinishedAPI;

static NSTimer * timer;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    [self getLastUpdate];
    [self getAllStatistics];
    //
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    
    
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width, self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height);
    }
    
    self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) updateData:(NSTimer *)timer
{
    FinishedAPI = 0;
    [self.activity startAnimating] ;
    [self getAllStatistics];
    //
    [self getLastUpdate];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
    timer=nil;
}

-(void)checkActivityToStop{
    if (FinishedAPI >=2) {
        [self.activity stopAnimating];
        self.btnUpdate.enabled=YES;
        //((UIButton * )sender).enabled =YES;
        NSLog(@"stop");

    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    //
    if ((toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(320,  self.viewContent.frame.size.height);
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self   LoadCachedData];
    //
    // [self getAllStatistics];
    //
    //  [self getLastUpdate];
    //
    timer=[NSTimer scheduledTimerWithTimeInterval:900.0f
                                           target:self selector:@selector(updateData:) userInfo:nil repeats:YES];
    //
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if ((orientation == UIInterfaceOrientationLandscapeLeft) || (orientation == UIInterfaceOrientationLandscapeRight)) {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width, self.viewContent.frame.size.height+150);
    } else {
        
        self.scrollView.contentSize = CGSizeMake(self.viewContent.frame.size.width,  self.viewContent.frame.size.height);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
-(void)hideKeyboard
{
    [self.view endEditing:YES];
}
//
-(void)getLastUpdate{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetLastUpdateTime"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data.length > 0 && connectionError == nil)
        {
            FinishedAPI ++;
            [self checkActivityToStop];
            NSError * err =nil;
            NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                         NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
            NSDictionary * data=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:NSJSONReadingAllowFragments
                                                                  error:&err];
            if (data!= nil) {
                [staticVariables singelton].dataDate = data;
           
            if ([data objectForKey:@"1"] != Nil) {
                self.lbllastUpdate.text=[NSString stringWithFormat:@"أخر تحديث : %@",[data objectForKey:@"1"]];
            }
                 }
        }else
        {
            FinishedAPI ++;
            [self checkActivityToStop];
            UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
            [alertConErr show];
        }
    }];
}
//
-(void)getAllStatistics
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"GetAllStatistics"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data.length > 0 && connectionError == nil)
        {
            FinishedAPI ++;
            [self checkActivityToStop];
            NSError * err =nil;
            NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&err];
                         NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
            NSArray * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingAllowFragments
                                                           error:&err];
            NSDictionary *resultDic = [ff objectAtIndex:0];
            if (resultDic!= nil) {
                [staticVariables singelton].AllStaticisticDic = resultDic;
            
            _lblAttVote.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersVotesCount"]];
            _lblCandLoc.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"CandidatesCentersCount"]];
            _lblCand.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"CandidatesCount"]];
            _lblVoterLoc.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersCentersCount"]];
            _lblVoters.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersCount"]];
            _lblGate.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"AdminUsersCountCount"]];
            _lblWining.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"WinnersCount"]];
            _lblAppealCount.text =[resultDic objectForKey:@"AppealCount"]== nil?@"0":[NSString stringWithFormat:@"%@", [resultDic objectForKey:@"AppealCount"]];
            
            ExCand = [[resultDic objectForKey:@"DisregardedCandidatesCount"] intValue];
            ExVoter = [[resultDic objectForKey:@"DisregardedVotersCount"] intValue];
            Cand =  [[resultDic objectForKey:@"CandidatesCount"] intValue];
            Voter = [[resultDic objectForKey:@"VotersCount"] intValue];
                }
        }else
        {
            FinishedAPI ++;
            [self checkActivityToStop];
            UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
            [alertConErr show];
        }
    }];
}

-(void)LoadCachedData{
    NSDictionary *resultDic =  [staticVariables singelton].AllStaticisticDic;
     if (resultDic!= nil) {
    _lblAttVote.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersVotesCount"]];
    _lblCandLoc.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"CandidatesCentersCount"]];
    _lblCand.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"CandidatesCount"]];
    _lblVoterLoc.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersCentersCount"]];
    _lblVoters.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"VotersCount"]];
    _lblGate.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"AdminUsersCountCount"]];
    _lblWining.text = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"WinnersCount"]];
    _lblAppealCount.text =[resultDic objectForKey:@"AppealCount"]== nil?@"0":[NSString stringWithFormat:@"%@", [resultDic objectForKey:@"AppealCount"]];
    
    ExCand = [[resultDic objectForKey:@"DisregardedCandidatesCount"] intValue];
    ExVoter = [[resultDic objectForKey:@"DisregardedVotersCount"] intValue];
    Cand =  [[resultDic objectForKey:@"CandidatesCount"] intValue];
    Voter = [[resultDic objectForKey:@"VotersCount"] intValue];
     }
    //
    NSDictionary *data =  [staticVariables singelton].dataDate ;
       if (data!= nil) {
    if ([data objectForKey:@"1"] != Nil) {
        self.lbllastUpdate.text=[NSString stringWithFormat:@"أخر تحديث : %@",[data objectForKey:@"1"]];
    }
  }
}
#pragma mark - lazy init

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)goToVoters:(id)sender {
    VoterStatisticViewController *VoterReport = [self.storyboard instantiateViewControllerWithIdentifier:@"VoterStatistic"];
    int Acepted = Voter - ExVoter;
    [VoterReport LoadDataWithRec:[NSString stringWithFormat:@"%d",Voter] andAcepted:[NSString stringWithFormat:@"%d", Acepted] andExcl:[NSString stringWithFormat:@"%d", ExVoter]];
    
    [self presentViewController:VoterReport animated:YES completion:nil];
}

- (IBAction)update:(id)sender {
    //    NSLog(@"update");
    //    [self updateData:Nil];
     [self updateData:Nil];
     [self.activity startAnimating];
    self.btnUpdate.enabled=NO;
    ((UIButton * )sender).enabled =NO;
    NSLog(@"start");
}
- (IBAction)btnVotersVotesReport:(id)sender {
    
    WinnersReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VotersVotesReport"];
    
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (IBAction)btnWinnersReport:(id)sender {
    
    
    WinnersReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WinnersReport"];
    
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)btnGateEntryReport:(id)sender {
    
    GateEntryReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GateEntryReport"];
    
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)btnCandidatesCentersReport:(id)sender {
    CandidatesCentersReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CandidatesCentersReport"];
    
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (IBAction)btnVotersCentersReport:(id)sender {
    
    
    VotersCentersReportViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VotersCentersReport"];
    
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (IBAction)btnAppeals:(id)sender {
    AppealsVotersViewController *ViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AppealsVoters"];
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)goToCand:(id)sender {
    CandStatisticViewController *CandStatisticViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CandStatistic"];
    int Acepted = Cand - ExCand;
    
    [CandStatisticViewController LoadDataWithRec:[NSString stringWithFormat:@"%d",Cand] andAcepted:[NSString stringWithFormat:@"%d", Acepted] andExcl:[NSString stringWithFormat:@"%d", ExCand]];
    [self presentViewController:CandStatisticViewController animated:YES completion:nil];
    
}
@end
