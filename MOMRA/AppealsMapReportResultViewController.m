//
//  AppealsMapReportResultViewController.m
//  MOMRA
//
//  Created by aya on 8/11/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "AppealsMapReportResultViewController.h"
#import "MOMRA-Swift.h"
#import "staticVariables.h"

@interface AppealsMapReportResultViewController ()
@property (nonatomic ,strong ) NSMutableArray * regions;
@property (nonatomic ,strong ) NSMutableDictionary * parameters;
@end

@implementation AppealsMapReportResultViewController
@synthesize regions;
@synthesize parameters;

static NSMutableDictionary * dictCandidatesAges;
static MKAnnotationView *openedAnnotationView;
static KMLParser *kmlParser;
static KMLParser *kmlParserCover;
static bool isOverlayAdded;
static NSArray *overlays ;
static NSArray *KMLAnnonations ;
static int SearchTypeID ; //1:Region ,2: city , 3:Divition , 4: Center
- (void)viewDidLoad {
    [super viewDidLoad];
    //
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake(24.0000,45.0000);
    self.mapView.delegate = self;
  [self setZoomLevel];
    [self loadData];
    //
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:singleTap];
    openedAnnotationView = nil;
    //
    // Locate the path to the route.kml file in the application's bundle
    // and parse it with the KMLParser.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AllDevisions" ofType:@"kml"];
    NSURL *url = [NSURL fileURLWithPath:path];
    kmlParser = [[KMLParser alloc] initWithURL:url];
     [kmlParser parseKML];
    //
NSURL *urlCover = [NSURL URLWithString:@"http://services.intekhab.gov.sa/GeoDashWebService/covers.kml"];
    kmlParserCover = [[KMLParser alloc] initWithURL:urlCover];
    [kmlParserCover parseKML];
    [self.mapView addOverlays:[kmlParserCover overlays]];
    [self.mapView addAnnotations:[kmlParserCover points]];

    
    // Add all of the MKOverlay objects parsed from the KML file to the map.
    overlays = [kmlParser overlays];
    
    
    // Add all of the MKAnnotation objects parsed from the KML file to the map.
    KMLAnnonations = [kmlParser points];
    
}
- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    if (!openedAnnotationView)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:openedAnnotationView];
    DXAnnotationView * DXview = (DXAnnotationView *)openedAnnotationView;
    
    BOOL isCallout = (CGRectContainsPoint(DXview.calloutView.frame, touchPoint));
    BOOL isPin = (CGRectContainsPoint(DXview.pinView.frame, touchPoint));
    if (isCallout|| isPin) {
        return;
    }
    if ([openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)openedAnnotationView)hideCalloutView];
        DXview.calloutView = nil;
        DXview.layer.zPosition = -1;
        openedAnnotationView = nil;
        [self.mapView deselectAnnotation:[openedAnnotationView annotation] animated:NO];
        
    }
}
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)setSearchValuesWithRegionID:(NSString *)RegionID CityID:(NSString *)CityID AppealStatus:(NSString *)AppealStatus AppellantType:(NSString *)AppellantType AppealStatusText:(NSString *)AppealStatusText AppellantTypeText:(NSString *)AppellantTypeText{
    parameters=[[NSMutableDictionary alloc]init];
    [parameters setObject:RegionID forKey:@"RegionID"];
    [parameters setObject:CityID forKey:@"CityID"];
    [parameters setObject:AppealStatus forKey:@"AppealStatus"];
    [parameters setObject:AppellantType forKey:@"AppellantType"];
    [parameters setObject:AppealStatusText forKey:@"AppealStatusText"];
    [parameters setObject:AppellantTypeText forKey:@"AppellantTypeText"];
    //
   if (![CityID isEqualToString:@"-1"])
    {
        SearchTypeID = 2;
    }
    else
    {
        SearchTypeID = 1;
    }
}

-(void)loadData
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token",[parameters valueForKey:@"RegionID"],@"RegionID",[parameters valueForKey:@"CityID"],@"CityID",[parameters valueForKey:@"AppealStatus"],@"AppealStatus",[parameters valueForKey:@"AppellantType"],@"AppellantType", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"SearchAppeals"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    [self.activity startAnimating];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             [self.activity stopAnimating];
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
             NSString * results =[receivedData objectForKey:@"d"] ;
             NSString * results2=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:NSJSONReadingAllowFragments
                                                                  error:&err];
             regions=[NSJSONSerialization JSONObjectWithData:[results2 dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:NSJSONReadingAllowFragments
                                                       error:&err];
             [self addMarkers];
         }else
         {
             [self.activity stopAnimating];
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
    
}

-(void)setZoomLevel{
    float zoomLevel = 20.0 ;
    switch (SearchTypeID) {
        case 1:
            zoomLevel = [[staticVariables singelton].RegionZoomLevel floatValue];
            break;
        case 2:
            zoomLevel = [[staticVariables singelton].CityZoomLevel floatValue];
            break;
        case 3:
            zoomLevel = [[staticVariables singelton].DivistionZoomLevel floatValue];
            break;
        case 4:
            zoomLevel = [[staticVariables singelton].CenterZoomLevel floatValue];
            break;
    }
    
    MKCoordinateRegion zoomIn = self.mapView.region;
    zoomIn.span.longitudeDelta  = zoomLevel;
    zoomIn.span.latitudeDelta  = zoomLevel;
    [self.mapView setRegion:zoomIn animated:NO];
    
}
-(void)addMarkers
{
    for (int i=0; i<regions.count; i++) {
        NSDictionary * reg =[regions objectAtIndex:i];
        //[[NSString stringWithFormat:@"%@",reg.Lat]doubleValue]
        NSDictionary * results =[reg objectForKey:@"RegionData"] ;
        CLLocationCoordinate2D coordinate =  CLLocationCoordinate2DMake([[results objectForKey:@"Lat"]doubleValue],[[results objectForKey:@"Long"]doubleValue]);
        MapLocationView *annotation = [[MapLocationView alloc] initWithName:[NSString stringWithFormat:@"%d",i]  address:nil  coordinate:coordinate] ;
        [_mapView addAnnotation:annotation];
    }
    //
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in [_mapView annotations]) {
        if ([annotation isKindOfClass:[MapLocationView class]]) {
            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
            if (MKMapRectIsNull(flyTo)) {
                flyTo = pointRect;
            } else {
                flyTo = MKMapRectUnion(flyTo, pointRect);
            }
        }
    }
    
    //
    
    
    // Position the map so that all overlays and annotations are visible on screen.
    if (flyTo.origin.x != INFINITY && flyTo.origin.y != INFINITY) {
        _mapView.visibleMapRect = flyTo;
    }
    //
    if (flyTo.size.width ==0 || flyTo.size.height ==0) {
        [self  setZoomLevel];
    }
    
}

#pragma mark - map delegate
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    if (mapView.region.span.latitudeDelta <= 3.0f && isOverlayAdded == NO) {
        [mapView addOverlays:overlays];
        [mapView addAnnotations:KMLAnnonations];
        isOverlayAdded = YES;
    } else  if (mapView.region.span.latitudeDelta > 3.0f && isOverlayAdded == YES){
        [mapView removeOverlays:overlays];
        [mapView removeAnnotations:KMLAnnonations];
        isOverlayAdded = NO;
        
    }
}
//
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKOverlayView * view = [[MKOverlayView alloc]init];
    if ([kmlParserCover viewForOverlay:overlay]) {
        view =[kmlParserCover viewForOverlay:overlay];
    }
    else
    {
        view =[kmlParser viewForOverlay:overlay];
    }
    return view;
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MapLocationView class]])
    {
        UIView *pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GreenPin.png"]];
        
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
        if (!annotationView) {
            
            DXAnnotationSettings *newSettings = [DXAnnotationSettings defaultSettings];
            newSettings.animationType = DXCalloutAnimationNone ;
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:nil
                                                                 settings:newSettings];
        }
        return annotationView;
    }
    else
    {
        return [kmlParser viewForAnnotationWithTitle:annotation forMapView:mapView];
    }
    
}
//
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
        return;
    }
    
    DXAnnotationView * DXview = (DXAnnotationView *)view;
    if (openedAnnotationView &&openedAnnotationView == view ) {
        DXview.layer.zPosition = 1;
        [self.mapView selectAnnotation:[view annotation] animated:NO];
    }
    else
    {
        DXview.layer.zPosition = -2;
    }
}
//
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (openedAnnotationView && [openedAnnotationView isKindOfClass:[DXAnnotationView class]]) {
        
        
        DXAnnotationView * newView = (DXAnnotationView *)openedAnnotationView;
        if (newView) {
            BarChartInfoWindow *  BarChartInfoView =   (BarChartInfoWindow *)newView.calloutView;
            id<MKAnnotation> annotation = [newView annotation];
            MKMapRect r = [self.mapView visibleMapRect];
            MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
            CGRect BarChartFrame = BarChartInfoView.frame;
            r.origin.x = pt.x - r.size.width * 0.5;
            r.origin.y = pt.y ;
            BarChartFrame.origin.y =20;
            BarChartInfoView.frame =BarChartFrame;
            [self.mapView setVisibleMapRect:r animated:YES];
        }
    }
    
}

static BarChartView * candidatesChart;

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (![[view annotation] isKindOfClass:[MapLocationView class]]) {
        return;
    }
    
    if (openedAnnotationView != nil&&openedAnnotationView != view) {
        view.layer.zPosition = -2;
        return;
    }else if(openedAnnotationView != nil&&openedAnnotationView == view)
    {
        return;
    }else
    {
        openedAnnotationView = view;
    }
    
    id annotation = [view annotation];
    UIView *calloutView ;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        calloutView = [[[NSBundle mainBundle] loadNibNamed:@"BarChartInfoWindow_ipad" owner:self options:nil] firstObject];
    }
    else{
        calloutView = [[[NSBundle mainBundle] loadNibNamed:@"BarChartInfoWindow" owner:self options:nil] firstObject];
    }
    
    DXAnnotationView * DXview = (DXAnnotationView *)view;
    [DXview addNewCallOutView:calloutView];
    view = (MKAnnotationView *)DXview;
    
    if ([view isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)view)showCalloutView];
        
        view.layer.zPosition = 0;
    }
    //
    DXAnnotationView * newView = (DXAnnotationView * )view;
    BarChartInfoWindow *  BarChartInfoView =   (BarChartInfoWindow *)newView.calloutView;
    
    NSInteger index=[[annotation name] integerValue];
    NSDictionary * reg =[self.regions objectAtIndex:index];
    //
    //
    UIButton *btnFullScreen = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        btnFullScreen.frame = CGRectMake(230, 320, 270 , 30);
    }else{
        btnFullScreen.frame = CGRectMake(40, 282, 270 , 30);
    }
    //
    NSString *btnFullScreenTitle = @"عرض الإحصائيات على الشاشة الكاملة";
    NSMutableAttributedString *btnFullScreenTitleString = [[NSMutableAttributedString alloc] initWithString:btnFullScreenTitle];
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        [btnFullScreenTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:16.0] range:NSMakeRange(0,btnFullScreenTitle.length)];
    }else{
        [btnFullScreenTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:13.0] range:NSMakeRange(0,btnFullScreenTitle.length)];
    }
    [btnFullScreenTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,btnFullScreenTitle.length)];
    //
    [btnFullScreen setAttributedTitle:btnFullScreenTitleString forState:(UIControlState)UIControlStateNormal];
    //
    [btnFullScreen addTarget:self
                      action:@selector(fullScreenPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    btnFullScreen.tag = index;
    
    [BarChartInfoView addSubview:btnFullScreen];
    //
    
    
    candidatesChart=BarChartInfoView.barChart;
    //
     NSDictionary * results =[reg objectForKey:@"RegionData"] ;
    //
    NSString *title = [NSString stringWithFormat:@"%@ %@ %@",@"إحصائيات",[results objectForKey:@"Name"],@"حسب المعايير التالية"];
    NSMutableAttributedString *attributedTitleString = [[NSMutableAttributedString alloc] initWithString:title];
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:15.0] range:NSMakeRange(0,title.length)];
    }else{
        [attributedTitleString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HacenSaudiArabia" size:12.0] range:NSMakeRange(0,title.length)];
    }
    
    [attributedTitleString addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0,title.length)];
    BarChartInfoView.title.attributedText=attributedTitleString;
    //
    NSMutableString * criteria=[[NSMutableString alloc]init];
    //
    if ([[parameters valueForKey:@"AppealStatus"] isEqualToString:@"0"])
    {
        [criteria appendString:@"حالة الطعن : الكل \n"];
    }
    else
    {
        [criteria appendString:[NSString stringWithFormat:@" حالة الطعن : %@ \n",[parameters valueForKey:@"AppealStatusText"]]];
    }
    //
    if ([[parameters valueForKey:@"AppellantType"] isEqualToString:@"0"])
    {
        [criteria appendString:@"نوع مقدم الطعن : الكل \n"];
    }
    else
    {
        [criteria appendString:[NSString stringWithFormat:@" نوع مقدم الطعن : %@ \n",[parameters valueForKey:@"AppellantTypeText"]]];
    }
    //
    BarChartInfoView.criteria.text=criteria;
    //
    [Helper configureBarChart:candidatesChart inInfoWindow:YES];
    //
    [self loadChart:reg];
    //
    [candidatesChart animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseInCirc];
    //[hhh addSubview:chart];
    //
    [mapView setCenterCoordinate:[annotation coordinate] animated:YES];
        MKMapRect r = [mapView visibleMapRect];
    MKMapPoint pt = MKMapPointForCoordinate([annotation coordinate]);
    CGRect BarChartFrame = BarChartInfoView.frame;
    r.origin.x = pt.x - r.size.width * 0.5;
    r.origin.y = pt.y ;
    BarChartFrame.origin.y =20;
    BarChartInfoView.frame =BarChartFrame;
    [mapView setVisibleMapRect:r animated:YES];
    
}

-(void)fullScreenPressed:(id)sender{
    
    WinningFullViewChartViewController *fullChart = [self.storyboard instantiateViewControllerWithIdentifier:@"WinningFullViewChart"];
    fullChart.parameters=parameters;
    NSInteger index=[[NSString stringWithFormat:@"%ld",(long)((UIButton *) sender).tag] integerValue];
    NSDictionary * reg =[self.regions objectAtIndex:index];
    fullChart.reg=reg;
    fullChart.dictAges=dictCandidatesAges;
    fullChart.viewName=@"Appeals";
    [self presentViewController:fullChart animated:YES completion:nil];
}
-(void)loadChart :(NSDictionary *)reg
{
    NSArray  * results =[reg objectForKey:@"AppealData"] ;
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    for (int i=0; i < results.count; i++) {
        NSDictionary * regData=[results objectAtIndex:i];
        [xVals addObject:[regData objectForKey:@"StatusName"]];
        double val1 = [[regData objectForKey:@"VOTERS_MAIN_REGION_COUNT"]doubleValue];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:val1 xIndex:i]];
    }
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yVals1 label:@""];
    [set1 setColor:[[UIColor alloc]initWithRed:0/255.0 green:136/255.0 blue:91/255.0 alpha:1.0 ]];
    if (results.count == 1) {
        set1.barSpace=0.7;
    }
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    
    BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
    candidatesChart.legend.enabled = NO;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        [data setValueFont:[UIFont systemFontOfSize:12.0f]];
    }
    candidatesChart.data = data;
}
#pragma mark - lazy init
-(NSMutableArray *)regions
{
    if (!regions) {
        regions=[[NSMutableArray alloc]init];
    }
    return regions;
}
-(NSMutableDictionary *)parameters
{
    if (!parameters) {
        parameters=[[NSMutableDictionary alloc]init];
    }
    return parameters;
}


@end
