//
//  AllStatisticsViewController.h
//  MOMRA
//
//  Created by aya on 6/21/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "VotersReportViewController.h"

@interface AllStatisticsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblAppealCount;
@property (weak, nonatomic) IBOutlet UIView *lastView;
- (IBAction)btnVotersVotesReport:(id)sender;
- (IBAction)btnWinnersReport:(id)sender;
- (IBAction)btnGateEntryReport:(id)sender;
- (IBAction)btnCandidatesCentersReport:(id)sender;
- (IBAction)btnVotersCentersReport:(id)sender;
- (IBAction)btnAppeals:(id)sender;
- (IBAction)goToCand:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblWining;
@property (weak, nonatomic) IBOutlet UILabel *lblGate;
@property (weak, nonatomic) IBOutlet UILabel *lblAttVote;
@property (weak, nonatomic) IBOutlet UILabel *lblCandLoc;
@property (weak, nonatomic) IBOutlet UILabel *lblVoterLoc;
@property (weak, nonatomic) IBOutlet UILabel *lblExCand;
@property (weak, nonatomic) IBOutlet UILabel *lblExVoters;
@property (weak, nonatomic) IBOutlet UILabel *lblCand;
@property (weak, nonatomic) IBOutlet UILabel *lblVoters;
@property (weak, nonatomic) IBOutlet UILabel *lbllastUpdate;
- (IBAction)close:(id)sender;
- (IBAction)goToVoters:(id)sender;
- (IBAction)update:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;

@end
