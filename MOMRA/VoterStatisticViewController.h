//
//  VoterStatisticViewController.h
//  MOMRA
//
//  Created by aya on 7/2/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoterStatisticViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
- (IBAction)btnAcceptedVotersReport:(id)sender;
- (IBAction)btnExVoter:(id)sender;
- (IBAction)btnGoToReport:(id)sender;
- (IBAction)close:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNewVotersCount;
@property (weak, nonatomic) IBOutlet UILabel *lblPreviousVotersCount;
@property (weak, nonatomic) IBOutlet UILabel *lblMaleNewVotersCount;
@property (weak, nonatomic) IBOutlet UILabel *lblFemaleNewVotersCount;

@property (weak, nonatomic) IBOutlet UILabel *lblAcepted;
@property (weak, nonatomic) IBOutlet UILabel *lblRec;
@property (weak, nonatomic) IBOutlet UILabel *lblExcl;
@property (weak, nonatomic) IBOutlet UILabel *lbllastUpdate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

-(void)LoadDataWithRec:(NSString *)rec andAcepted:(NSString *)Acepted andExcl:(NSString *)Excl;

@end
