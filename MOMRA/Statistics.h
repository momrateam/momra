//
//  Statistics.h
//  MOMRA
//
//  Created by aya on 6/21/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Statistics : UIView
@property (weak, nonatomic) IBOutlet UILabel *votersYoung;
@property (weak, nonatomic) IBOutlet UILabel *CandFemale;
@property (weak, nonatomic) IBOutlet UILabel *CandMale;
@property (weak, nonatomic) IBOutlet UILabel *RecNewFemaleCount;
@property (weak, nonatomic) IBOutlet UILabel *RecNewMaleCount;
@property (weak, nonatomic) IBOutlet UILabel *RecNewCount;
@property (weak, nonatomic) IBOutlet UILabel *lastCount;
@property (weak, nonatomic) IBOutlet UILabel *VotersCount;
@property (weak, nonatomic) IBOutlet UILabel *AcceptedVotersCount;
@property (weak, nonatomic) IBOutlet UILabel *CandidatesCount;
@property (weak, nonatomic) IBOutlet UILabel *AcceptedCandidatesCount;
@property (weak, nonatomic) IBOutlet UILabel *DisregardedCandidatesCount;
@property (weak, nonatomic) IBOutlet UILabel *DisregardedVotersCount;
@property (weak, nonatomic) IBOutlet UILabel *VotersCentersCount;
@property (weak, nonatomic) IBOutlet UILabel *CandidatesCentersCount;
@property (weak, nonatomic) IBOutlet UILabel *AdminUsersCountCount;
@property (weak, nonatomic) IBOutlet UILabel *VotersVotesCount;
@property (weak, nonatomic) IBOutlet UILabel *WinnersCount;
@property (weak, nonatomic) IBOutlet UILabel *RegionName;

@end
