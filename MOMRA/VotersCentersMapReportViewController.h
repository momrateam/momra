//
//  VotersCentersMapReportViewController.h
//  MOMRA
//
//  Created by aya on 7/8/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "PieChartInfoWindow.h"
#import "votersCentersFullViewChartViewController.h"
#import "KeyValue.h"
#import "DetailsTable.h"
#import <MapKit/MapKit.h>
#import "MapLocationView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "KMLParser.h"

@interface VotersCentersMapReportViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)close:(id)sender;
-(void)setSearchValuesWithRegionID:(NSString * )RegionID CityID:(NSString * )CityID DivisionID:(NSString * )DivisionID  Sex:(NSString * )Sex CurrentYear:(NSString *)CurrentYear SecondYear:(NSString * )SecondYear FirstYear:(NSString *)FirstYear isVoters:(NSString * )isVoters;
@end
