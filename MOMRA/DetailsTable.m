//
//  DetailsTable.m
//  MOMRA
//
//  Created by aya on 7/26/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "DetailsTable.h"
#import "KeyValue.h"

@implementation DetailsTable

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)LoadDataWithArrayOfKeyValue:(NSMutableArray *) arrOfYears
{
    if (arrOfYears && arrOfYears.count >0) {
        
        KeyValue * frstYr = arrOfYears[0];
        int sum = 0;
        if(arrOfYears.count>1){
            sum=[frstYr.Value intValue]+[((KeyValue*)arrOfYears[1]).Value intValue];
        }
        if (arrOfYears.count>2) {
            sum=[frstYr.Value intValue]+[((KeyValue*)arrOfYears[1]).Value intValue]+[((KeyValue*)arrOfYears[2]).Value intValue];
        }
        self.lblFrstYr.text =frstYr.Key ;
        float frtTotal=0.0;
        if (sum != 0) {
            frtTotal = ([frstYr.Value floatValue]/sum)*100;
        }
        self.lblFrstYrPrc.text = [NSString stringWithFormat:@"%%%.3f",frtTotal ];
        
        //float frstYrTotal = [frstYr.Value floatValue];
        //self.lblFrstYearTotal.text =[self abbreviateNumber:[frstYr.Value intValue]] ;
        self.lblFrstYearTotal.text =frstYr.Value ;
        //
        if (arrOfYears.count>1) {
            KeyValue * scYr = arrOfYears[1];
            self.lblScYr.text =scYr.Key ;
            
            float ScTotal = 0.0;
            if (sum != 0) {
                ScTotal = ([scYr.Value floatValue]/sum)*100;
            }
            self.lblScYrPrc.text = [NSString stringWithFormat:@"%%%.3f",ScTotal ];
            //self.lblScYearTotal.text =[self abbreviateNumber:[scYr.Value intValue]] ;
            self.lblScYearTotal.text =scYr.Value ;
        }
        //
        if (arrOfYears.count>2) {
            KeyValue * trdYr = arrOfYears[2];
            self.lblTrdYr.text =trdYr.Key ;
            
             float trdTotal = 0.0;
            if (sum != 0) {
                trdTotal = ([trdYr.Value floatValue]/sum)*100;
            }
            self.lblTrdYrPrc.text = [NSString stringWithFormat:@"%%%.3f",trdTotal ];
            //self.lblTrdYearTotal.text =[self abbreviateNumber:[trdYr.Value intValue]] ;
            self.lblTrdYearTotal.text =trdYr.Value ;
        }
    }
}


-(NSString *)abbreviateNumber:(int)num {
    
    NSString *abbrevNum;
    float number = (float)num;
    
    //Prevent numbers smaller than 1000 to return NULL
    if (num >= 1000) {
        NSArray *abbrev = @[@"K", @"M", @"B"];
        
        for (int i = abbrev.count - 1; i >= 0; i--) {
            
            // Convert array index to "1000", "1000000", etc
            int size = pow(10,(i+1)*3);
            
            if(size <= number) {
                // Removed the round and dec to make sure small numbers are included like: 1.1K instead of 1K
                number = number/size;
                NSString *numberString = [self floatToString:number];
                
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
            
        }
    } else {
        
        // Numbers like: 999 returns 999 instead of NULL
        abbrevNum = [NSString stringWithFormat:@"%d", (int)number];
    }
    
    return abbrevNum;
}

- (NSString *) floatToString:(float) val {
    NSString *ret = [NSString stringWithFormat:@"%.1f", val];
    unichar c = [ret characterAtIndex:[ret length] - 1];
    
    while (c == 48) { // 0
        ret = [ret substringToIndex:[ret length] - 1];
        c = [ret characterAtIndex:[ret length] - 1];
        
        //After finding the "." we know that everything left is the decimal number, so get a substring excluding the "."
        if(c == 46) { // .
            ret = [ret substringToIndex:[ret length] - 1];
        }
    }
    
    return ret;
}

@end
