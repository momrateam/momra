//
//  StatisticsCenter.h
//  MOMRA
//
//  Created by aya on 8/10/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatisticsCenter : UIView
@property (weak, nonatomic) IBOutlet UILabel *VotersCount;
@property (weak, nonatomic) IBOutlet UILabel *DisregardedVotersCount;
@property (weak, nonatomic) IBOutlet UILabel *VotersVotesCount;
@property (weak, nonatomic) IBOutlet UILabel *RegionName;
@property (weak, nonatomic) IBOutlet UILabel *AcceptedVotersCount;
@property (weak, nonatomic) IBOutlet UILabel *RecNewCount;
@property (weak, nonatomic) IBOutlet UILabel *lastCount;
@end
