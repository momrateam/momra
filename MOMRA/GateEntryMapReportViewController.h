//
//  GateEntryMapReportViewController.h
//  MOMRA
//
//  Created by aya on 7/9/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import <MapKit/MapKit.h>
#import "MapLocationView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "KMLParser.h"

@interface GateEntryMapReportViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)close:(id)sender;
-(void)setSearchValuesWithRegionID:(NSString * )RegionID ;
@end
