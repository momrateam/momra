//
//  RejectedVotersMapReportResultViewController.h
//  MOMRA
//
//  Created by aya on 7/12/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "BarChartInfoWindow.h"
#import "RejectedVotersFullViewChartViewController.h"
#import "KeyValue.h"
#import "DetailsTable.h"
#import <MapKit/MapKit.h>
#import "MapLocationView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "KMLParser.h"

@interface RejectedVotersMapReportResultViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
- (IBAction)close:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

-(void)setSearchValuesWithRegionID:(NSString *)RegionID CityID:(NSString *)CityID DivisionID:(NSString *)DivisionID CenterID:(NSString *)CenterID Age:(NSString *)Age Sex:(NSString *)Sex CurrentYear:(NSString *)CurrentYear SecondYear:(NSString * )SecondYear FirstYear:(NSString *)FirstYear AgesDiction:(NSDictionary *)AgesDiction Reason:(NSString *)reason disReason:(NSString *)disReason isVoters:(NSString * )isVoters;

@end
