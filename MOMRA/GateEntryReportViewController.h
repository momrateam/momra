//
//  GateEntryReportViewController.h
//  MOMRA
//
//  Created by aya on 6/25/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface GateEntryReportViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


- (IBAction)close:(id)sender;


@property (weak, nonatomic) IBOutlet UISwitch *ckFirstYear;
@property (weak, nonatomic) IBOutlet UISwitch *ckSecYear;
@property (weak, nonatomic) IBOutlet UISwitch *ckCurrentYear;

- (IBAction)ShowInMap:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtRegion;


@end
