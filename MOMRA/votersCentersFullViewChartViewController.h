//
//  votersCentersFullViewChartViewController.h
//  MOMRA
//
//  Created by aya on 7/8/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOMRA-Swift.h"
#import "Helper.h"
#import "KeyValue.h"
#import "DetailsTable.h"

@interface votersCentersFullViewChartViewController : UIViewController

@property (weak, nonatomic) IBOutlet PieChartView *chart;
- (IBAction)close:(id)sender;

@property (weak, nonatomic) NSDictionary * regCurrent;
@property (weak, nonatomic) NSDictionary * regFirst;
@property (weak, nonatomic) NSDictionary * regSecond;
//
@property (weak, nonatomic) NSDictionary *parameters;
@property (weak, nonatomic) IBOutlet UILabel *reportTitle;
@property (weak, nonatomic) NSMutableDictionary * dictAges;
@property (weak, nonatomic) IBOutlet UILabel *criteria;
@property (weak, nonatomic) IBOutlet UILabel *header;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *mainContainerView;
@end
