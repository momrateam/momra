//
//  GateEntryReportViewController.m
//  MOMRA
//
//  Created by aya on 6/25/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import "GateEntryReportViewController.h"
#import "Helper.h"
#import "GateEntryMapReportViewController.h"
#import "staticVariables.h"

@interface GateEntryReportViewController ()

@end

@implementation GateEntryReportViewController

//
static NSDictionary * dicRegions;

//
static UIPickerView*  pickerRegion;
//
static NSString * SelectedRegionID;


- (void)viewDidLoad {
    [super viewDidLoad];
    SelectedRegionID = nil;

     // Do any additional setup after loading the view.
    [self loadPickers];
    //
    float sizeOfContent = 0;
    UIView *lLast = [self.scrollView.subviews lastObject];
    NSInteger wd = lLast.frame.origin.y;
    NSInteger ht = lLast.frame.size.height;
    
    sizeOfContent = wd+ht;
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, sizeOfContent);
    //self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [self.scrollView addGestureRecognizer:tapGesture];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    ///
       //
    [self initPickers];
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if ((orientation == UIInterfaceOrientationPortraitUpsideDown) || (orientation == UIInterfaceOrientationPortrait)) {
        self.scrollView.contentSize = CGSizeMake(306,  350);
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if ((orientation == UIInterfaceOrientationPortraitUpsideDown) || (orientation == UIInterfaceOrientationPortrait)) {
        self.scrollView.contentSize = CGSizeMake(306,  458);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
static  bool keyboardIsShown;
- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    //    viewFrame.size.height += (keyboardSize.height - kTabBarHeight);
    
    viewFrame.size.height += (keyboardSize.height - 140.0);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}
- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    //    viewFrame.size.height -= (keyboardSize.height - kTabBarHeight);
    viewFrame.size.height -= (keyboardSize.height - 140.0);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}
//
-(void)initPickers{
    
    
    pickerRegion = [[UIPickerView alloc]init];
    pickerRegion.tag=2;
    [pickerRegion setDataSource:self];
    [pickerRegion setDelegate:self];
    [pickerRegion setShowsSelectionIndicator:YES];
    self.txtRegion.inputView = pickerRegion;
    
    
 }

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}



-(void)updateTextField:(id)sender
{
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
-(void)hideKeyboard
{
    [self.view endEditing:YES];
}
-(void)loadPickers
{
    NSDictionary *inputData = [NSDictionary dictionaryWithObjectsAndKeys:[Helper getTimeStamp], @"Token", nil];
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    NSURL *url = [Helper getWebserviceURL:@"AdminUsersSearchCriteria"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError * err =nil;
             NSDictionary * receivedData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingAllowFragments
                                                                             error:&err];
             
                          NSString * results =[receivedData objectForKey:@"d"] ;
                          if (results == nil || [results isEqualToString:@""]) {
                 UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى استرجاع البيانات" message:@" من فضلك اعد المحاولة مرة اخرى و اذا استمرت المشكله قم بالاتصال بمدير النظام " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
                 [alertConErr show];
                 return ;
             }
             NSDictionary * ff=[NSJSONSerialization JSONObjectWithData:[results dataUsingEncoding:NSUTF8StringEncoding]
                                                               options:NSJSONReadingAllowFragments
                                                                 error:&err];
             
             //
             dicRegions =[NSJSONSerialization JSONObjectWithData:[[ff objectForKey:@"Regions"] dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingAllowFragments
                                                           error:&err];
             if (![[staticVariables singelton].UserTypeID isEqualToString:@"0"]) {
                 self.txtRegion.text = [dicRegions objectForKey:[staticVariables singelton].UserTypeID];
                 self.txtRegion.enabled = NO;
                 
                 SelectedRegionID = [staticVariables singelton].UserTypeID;

             }

             
         }else
         {
             UIAlertView * alertConErr = [[UIAlertView alloc]initWithTitle:@"خطأ فى الاتصال بالخادم" message:@"تعثر الاتصال بالخادم \n من فضلك تأكد من الاتصال بالانترنت " delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
             [alertConErr show];
         }
     }];
    
}
//



#pragma mark - Deleget event
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
  
            return (int)dicRegions.count;
 
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
   if (pickerView.tag == 2) {
        NSArray * arrofKeys = [[dicRegions allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * arrofValues = [dicRegions objectsForKeys: arrofKeys notFoundMarker: [NSNull null]];
        
        self.txtRegion.text = arrofValues[row];
        [self.txtRegion resignFirstResponder];
        NSString * ID = (NSString *)arrofKeys[row];

        SelectedRegionID = ID;
        
    }
}
//
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* tView = (UILabel*)view;
    
    if (!tView){
        tView = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        tView.font = [UIFont fontWithName:@"Arial Black" size:11.0];
        tView.textAlignment = NSTextAlignmentCenter;
        tView.textColor = [UIColor colorWithRed:0.0392 green:0.4470 blue:0.6156 alpha:1.0];
    }
        if (pickerView.tag == 2) {
        NSArray * sortedKeys = [[dicRegions allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
        NSArray * objects = [dicRegions objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
        tView.text= objects[row];
        return tView;
    }
    else
    {
        return NULL;
    }
}
- (IBAction)ShowInMap:(id)sender {
    
    NSString * ValidMessage =  [self Validation];
    
    if (![ValidMessage isEqualToString:@""]) {
        UIAlertView * alertValid = [[UIAlertView alloc]initWithTitle:@"تنبية" message:ValidMessage delegate:self cancelButtonTitle:@"إغلاق" otherButtonTitles:nil, nil];
        [alertValid show];
        return;
    }
    NSString * regionID;

    //
    
     //
    regionID = SelectedRegionID;
    
    GateEntryMapReportViewController *MapResult = [self.storyboard instantiateViewControllerWithIdentifier:@"GateEntryStatisticsResult"];
    
    NSString *currentYear ;
    NSString *secondYear ;
    NSString *firstYear ;
    
    if (self.ckCurrentYear.on) {
        currentYear = @"1";
    }
    else
    {
        currentYear = @"0";
    }
    
    if (self.ckSecYear.on) {
        secondYear = @"1";
    }
    else
    {
        secondYear = @"0";
    }
    
    if (self.ckFirstYear.on) {
        firstYear = @"1";
    }
    else
    {
        firstYear = @"0";
    }
    
    
    [MapResult setSearchValuesWithRegionID:regionID ];
    [self presentViewController:MapResult animated:YES completion:nil];
    
}
//
-(NSString *)Validation
{
    NSString * ValidationMessage =@"";
    
    if (SelectedRegionID == nil) {
        ValidationMessage = [NSString stringWithFormat:@"%@-%@",ValidationMessage,@"من فضلك أختر لجنة انتخابية"];
        
    }
       
    if (!self.ckCurrentYear.on && !self.ckSecYear.on && !self.ckFirstYear.on) {
        //
          ValidationMessage = [NSString stringWithFormat:@"%@\n-%@",ValidationMessage,@"من فضلك أختر مرحلة انتخابية واحدة على الاقل"];
    }
    return ValidationMessage;
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
