//
//  MapReportResultViewController.h
//  MOMRA
//
//  Created by aya on 6/30/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Helper.h"
#import "BarChartInfoWindow.h"
#import "FullViewChartViewController.h"
#import "KeyValue.h"
#import "DetailsTable.h"
#import <MapKit/MapKit.h>
#import "MapLocationView.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "KMLParser.h"

@interface MapReportResultViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)close:(id)sender;
-(void)setSearchValuesWithRegionID:(NSString *)RegionID CityID:(NSString *)CityID DivisionID:(NSString *)DivisionID CenterID:(NSString *)CenterID Age:(NSString *)Age Day:(NSString *)Day Week:(NSString *)Week Sex:(NSString *)Sex CurrentYear:(NSString *)CurrentYear SecondYear:(NSString * )SecondYear FirstYear:(NSString *)FirstYear AgesDiction:(NSDictionary *)AgesDiction Duration:(NSString *)Duration votersView:(NSString * )votersView;
@end
