//
//  CandStatisticViewController.h
//  MOMRA
//
//  Created by aya on 7/2/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"

@interface CandStatisticViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
- (IBAction)btnAcceptedCandReport:(id)sender;
- (IBAction)btnDisCandidatesReport:(id)sender;
- (IBAction)btnRecCand:(id)sender;
- (IBAction)close:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblAcepted;
@property (weak, nonatomic) IBOutlet UILabel *lblRec;
@property (weak, nonatomic) IBOutlet UILabel *lblExcl;
@property (weak, nonatomic) IBOutlet UILabel *lbllastUpdate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet UILabel *lblCandMaleCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCandFemaleCount;

-(void)LoadDataWithRec:(NSString *)rec andAcepted:(NSString *)Acepted andExcl:(NSString *)Excl;
@end
