//
//  PieChartInfoWindow.h
//  MOMRA
//
//  Created by aya on 7/8/15.
//  Copyright (c) 2015 ITRoots. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOMRA-Swift.h"

@interface PieChartInfoWindow : UIView
@property (weak, nonatomic) IBOutlet PieChartView *PieChart;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *criteria;
@end
